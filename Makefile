export RAIDDEX_OP_ENV=TESTING
GITLAB_IMAGE_REPO=registry.gitlab.com/riley_dixon/raiddex
ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
RAIDDEX_DIR := ${ROOT_DIR}src/raiddex/
SOURCE_PG_CONF_FILE=${RAIDDEX_DIR}db/config/postgresql.conf
DOCKER_PG_CONF_FILE=/etc/postgresql/postgresql.conf
SOURCE_PG_CONN_CONFIG_DIR=${RAIDDEX_DIR}db/connection_config/local
DOCKER_PG_CONN_CONFIG_DIR=/run/secrets
unittest:
	python3 -m unittest discover tests.unit

dbtest:
	docker run \
		--name raiddex_postgres-test \
		--detach \
		--rm \
		-p 5435:5433 \
		-e POSTGRES_USER_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-name \
		-e POSTGRES_PASSWORD_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-password \
		-e POSTGRES_DB_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-default-db \
		-v ${SOURCE_PG_CONF_FILE}:${DOCKER_PG_CONF_FILE} \
		-v ${SOURCE_PG_CONN_CONFIG_DIR}:${DOCKER_PG_CONN_CONFIG_DIR} \
		postgres:15.5 \
		-c "config_file=${DOCKER_PG_CONF_FILE}"
	python3 -m unittest discover tests.db || true
	docker stop raiddex_postgres-test --time 5

sanitytest:
	docker run \
		--name raiddex_postgres-test \
		--detach \
		--rm \
		-p 5435:5433 \
		-e POSTGRES_USER_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-name \
		-e POSTGRES_PASSWORD_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-password \
		-e POSTGRES_DB_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-default-db \
		-v ${SOURCE_PG_CONF_FILE}:${DOCKER_PG_CONF_FILE} \
		-v ${SOURCE_PG_CONN_CONFIG_DIR}:${DOCKER_PG_CONN_CONFIG_DIR} \
		postgres:15.5 \
		-c "config_file=${DOCKER_PG_CONF_FILE}"
	python3 -m unittest discover tests.sanity || true
	docker stop raiddex_postgres-test --time 5

bottest:
	docker run \
		--name raiddex_postgres-test \
		--detach \
		--rm \
		-p 5435:5433 \
		-e POSTGRES_USER_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-name \
		-e POSTGRES_PASSWORD_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-password \
		-e POSTGRES_DB_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-default-db \
		-v ${SOURCE_PG_CONF_FILE}:${DOCKER_PG_CONF_FILE} \
		-v ${SOURCE_PG_CONN_CONFIG_DIR}:${DOCKER_PG_CONN_CONFIG_DIR} \
		postgres:15.5 \
		-c "config_file=${DOCKER_PG_CONF_FILE}"
	python3 -m unittest discover tests.bot || true
	docker stop raiddex_postgres-test --time 5

upgradetest:
#docker run -it --rm -v ~/pogo_bot/PokemonGOInfoBot:/tmp/raiddex --add-host=host.docker.internal:host-gateway --cap-add NET_ADMIN --sysctl net.ipv4.conf.all.route_localnet=1 -e RAIDDEX_OP_ENV=TESTING alpine /tmp/raiddex/docker/run_upgrade_db_test.sh
	docker run \
		--name raiddex_postgres-test \
		--detach \
		--rm \
		-p 5435:5433 \
		-e POSTGRES_USER_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-name \
		-e POSTGRES_PASSWORD_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-su-password \
		-e POSTGRES_DB_FILE=${DOCKER_PG_CONN_CONFIG_DIR}/pg-default-db \
		-v ${SOURCE_PG_CONF_FILE}:${DOCKER_PG_CONF_FILE} \
		-v ${SOURCE_PG_CONN_CONFIG_DIR}:${DOCKER_PG_CONN_CONFIG_DIR} \
		postgres:15.5 \
		-c "config_file=${DOCKER_PG_CONF_FILE}"
	docker run \
		--name raiddex_db_upgrade_driver \
		--rm \
		--add-host=host.docker.internal:host-gateway \
		--cap-add NET_ADMIN \
		--sysctl net.ipv4.conf.all.route_localnet=1 \
		-e RAIDDEX_OP_ENV=${RAIDDEX_OP_ENV} \
		-e RAIDDEX_STDERR_LOG_LEVEL_OVERRIDE="INFO" \
		-v ${ROOT_DIR}:/tmp/raiddex \
		alpine \
		/tmp/raiddex/docker/run_upgrade_db_test.sh || true

	docker stop raiddex_postgres-test --time 5

build_ci_image:
	docker buildx build \
		-f docker/DOCKERFILE.ci_image \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--cache-from=type=registry,ref=${GITLAB_IMAGE_REPO}/raiddex_ci_env:latest \
		-t ${GITLAB_IMAGE_REPO}/raiddex_ci_env:latest .
	echo "docker tag ${GITLAB_IMAGE_REPO}/raiddex_ci_env:latest raiddex_ci_env:latest"

push_ci_image:
	docker push registry.gitlab.com/riley_dixon/raiddex/raiddex_ci_env:latest

start_dev_db:
	docker run \
		--name raiddex_postgres-dev \
		--detach \
		--rm \
		-p 5434:5433 \
		-e POSTGRES_USER=rd_admin \
		-e POSTGRES_PASSWORD_FILE=/run/secrets/pg-su-password \
		-e POSTGRES_DB=postgres \
		-v ${ROOT_DIR}src/db/connection_config/local:/run/secrets \
		-v ${ROOT_DIR}src/db/config/postgresql.conf:/etc/postgresql/postgresql.conf postgres:15.5 -c "config_file=/etc/postgresql/postgresql.conf"
