import os

from enum import Flag, auto


RAIDDEX_OP_ENV_VAR_NAME = "RAIDDEX_OP_ENV"


class OperatingEnvironment(Flag):
    """
    Defines a set of supported environments for RaidDex to execute in.

    This allows RaidDex, in whole or in part, to have different behaviour depending
    on where RaidDex is operating. This is useful when the RaidDex library wants to
    automatically determine and configure certain subsystems, such as logging or
    Database connection information.

    While I acknowledge that this limits flexibility, the convenience of having the library
    automatically determine this information is incredibly beneficial to me.
    """

    DEVELOPMENT = auto()
    TESTING = auto()
    GITLAB_CI = auto()
    PRODUCTION = auto()


def get_operating_environment():
    """
    Determine and store the current operating environment.

    If the operating environment environment variable is missing or an
    otherwise unknown value, the running environment will be set to
    DEVELOPMENT.

    An assumption is made that logging has not yet been configured yet,
    so a message will be sent directly to STDOUT.
    """
    op_env_str = os.environ.get(RAIDDEX_OP_ENV_VAR_NAME)
    if op_env_str is None:
        op_env_str = "DEVELOPMENT"
        print(
            f"ATTENTION: Operating environment EnvVar is not set. "
            f"Please set EnvVar `{RAIDDEX_OP_ENV_VAR_NAME}`. "
            f"Defaulting operating environment to `{op_env_str}`."
        )
    op_env_str.upper()

    try:
        running_op_env = OperatingEnvironment[op_env_str]
    except KeyError:
        running_op_env = OperatingEnvironment.DEVELOPMENT
        print(
            f"ATTENTION: Unknown operating environment `{op_env_str}`. "
            f"Please correct EnvVar `{RAIDDEX_OP_ENV_VAR_NAME}`. "
            f"Defaulting operating environment to `{running_op_env.name}`."
        )
    
    return running_op_env
