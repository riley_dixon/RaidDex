import logging
import os
import pathlib
import sys

from logging.handlers import TimedRotatingFileHandler

import discord

import raiddex
from raiddex.utils.colour_formatter import ColourFormatter
from raiddex.utils.op_env import OperatingEnvironment as OpEnv, get_operating_environment

# An override option in case we want more verbose logging on STDERR
STDERR_LOG_LEVEL_OVERRIDE_ENV_VAR = "RAIDDEX_STDERR_LOG_LEVEL_OVERRIDE"
LOG_FILE_DIR = pathlib.Path("/var/log/raiddex").resolve()
LOG_FILE_NAME = "raiddex_messages.log"

def swap_datetime_order(filename: str) -> str:
    """
    Change the location of the DateTime stamp in the filename.

    By default, `logging` will append the DateTime stamp to after the file extension
    of the log file.
    This will swap the order to maintain the file extension being last.
    """
    parts = filename.split(".")
    # Expect 3 parts (in this order): filename, file extension, DateTime stamp.
    return f"{parts[0]}.{parts[2]}.{parts[1]}"


def compress_logs():
    """
    An idea to compress old logs to save hard drive space.

    Set this function to Handler.rotator.
    """
    pass


def _setup_raiddex_logger(operating_env: OpEnv):
    base_logger = logging.getLogger(raiddex.__name__)
    if operating_env in (OpEnv.DEVELOPMENT | OpEnv.TESTING | OpEnv.GITLAB_CI):
        base_logger.setLevel(logging.DEBUG)
    elif operating_env == OpEnv.PRODUCTION:
        base_logger.setLevel(logging.INFO)
    else:
        # We should not get to this point, but just in case notify the developer.
        raise Exception("Unknown Operating Environment")

    return base_logger


def _setup_stderr_handler(operating_env: OpEnv):
    stderr_handler = logging.StreamHandler(stream=sys.stderr)
    if operating_env == OpEnv.DEVELOPMENT:
        stderr_handler.setLevel(logging.DEBUG)
    elif operating_env in OpEnv.TESTING | OpEnv.GITLAB_CI:
        # For Testing envs, log ERROR - To preserve nice output from Test Driver.
        stderr_handler.setLevel(logging.ERROR)
    elif operating_env == OpEnv.PRODUCTION:
        stderr_handler.setLevel(logging.INFO)
    else:
        # We should not get to this point, but just in case notify the developer.
        raise Exception("Unknown Operating Environment")

    colour_formatter = ColourFormatter()
    stderr_handler.setFormatter(colour_formatter)

    return stderr_handler


def _setup_file_handler(operating_env: OpEnv):
    # NOTE: Will require package manager to setup logging dirs.
    log_abs_path = (LOG_FILE_DIR / LOG_FILE_NAME).resolve()
    file_handler = TimedRotatingFileHandler(
        filename=log_abs_path,
        when="MIDNIGHT",
        backupCount=7, # 7 Days of logs kept.
        delay=True,
        utc=True
    )
    file_handler.namer = swap_datetime_order
    if operating_env in OpEnv.DEVELOPMENT | OpEnv.TESTING | OpEnv.GITLAB_CI:
        file_handler.setLevel(logging.DEBUG)
    elif operating_env == OpEnv.PRODUCTION:
        file_handler.setLevel(logging.INFO)
    else:
        # We should not get to this point, but just in case notify the developer.
        raise Exception("Unknown Operating Environment")
    # consider rotator to also compress files
    # would need to add compressed file extension to namer

    basic_formatter = logging.Formatter(
        fmt="%(asctime)s %(levelname)-8s %(name)s %(message)s"
    )
    file_handler.setFormatter(basic_formatter)

    return file_handler


def _process_overrides(
    root_logger: logging.Logger,
    discord_logger: logging.Logger,
    stderr_handler: logging.Handler,
    file_handler: TimedRotatingFileHandler
):
    override_log_level = os.environ.get(STDERR_LOG_LEVEL_OVERRIDE_ENV_VAR)
    if override_log_level is not None:
        stderr_handler.setLevel(override_log_level)
        root_logger.critical(f"Overriding STDERR Log Handler to level '{override_log_level}'.")


def setup_logging():
    """
    Setup Loggers and Handlers based on operating environment.

    Dev Environments - Active development, directly running code or tests.
    Testing Environments - Running tests through say a CI/CD system.
    Production - Should be self-explanatory.
    """
    operating_env = get_operating_environment()

    raiddex_logger = _setup_raiddex_logger(operating_env)
    root_logger = logging.getLogger(None)

    # Handler for Stderr messages
    stderr_handler = _setup_stderr_handler(operating_env)

    # Handler for Log file
    file_handler = _setup_file_handler(operating_env)

    root_logger.addHandler(stderr_handler)
    root_logger.addHandler(file_handler)

    # Manage discord.py's logger
    discord_logger = logging.getLogger(discord.__name__)
    discord_logger.setLevel(logging.INFO) # Not interested in DEBUG messages from discord.py

    root_logger.critical(f"RaidDex Operating Environment Set To: {operating_env}")

    _process_overrides(root_logger, discord_logger, stderr_handler, file_handler)
