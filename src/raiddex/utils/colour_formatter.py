import logging
import time

ANSI_RESET = "\x1b[0m"
LEVEL_COLOURS = {
    logging.DEBUG: '\x1b[40;1m',
    logging.INFO: '\x1b[34;1m',
    logging.WARNING: '\x1b[33;1m',
    logging.ERROR: '\x1b[31m',
    logging.CRITICAL: '\x1b[41m',
}
LOG_TIME_FMT = f"\x1b[30;1m%(asctime)s{ANSI_RESET}"
LOG_LEVEL_FMT = f"[ {{colour}}%(levelname)-8s{ANSI_RESET} ]" # Defer {{colour}} choice in ColourFormatter.FORMATS
LOG_NAME_FMT = f"\x1b[35m%(name)s{ANSI_RESET}"
LOG_MESSAGE_FMT = f"%(message)s{ANSI_RESET}"


class ColourFormatter(logging.Formatter):
    """
    Logging Formatter to add colour into the logs to improve readability.
    """

    FORMATS = {
        level: logging.Formatter(
            fmt=f"{LOG_TIME_FMT} {LOG_LEVEL_FMT.format(colour=colour)} {LOG_NAME_FMT} {LOG_MESSAGE_FMT}",
            datefmt=None
        )
        for level, colour in LEVEL_COLOURS.items()
    }

    converter = time.gmtime
    
    def format(self, record):
        formatter = self.FORMATS.get(record.levelno)
        if formatter is None:
            formatter = self.FORMATS[logging.DEBUG]

        # Override the traceback to always print in red
        if record.exc_info:
            text = formatter.formatException(record.exc_info)
            record.exc_text = f'\x1b[31m{text}\x1b[0m'

        output = formatter.format(record)

        # Remove the cache layer
        record.exc_text = None
        return output
