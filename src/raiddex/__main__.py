"""
Entry point for the discord bot 'RaidDex'.
"""
from raiddex.bot.raiddex_bot import RaidDexDiscord
from raiddex.core.parser.parser import Parser
from raiddex.db.db_manager import DBManager
from raiddex.db.db_pool import DBPool


raider_dex_bot = RaidDexDiscord()
# TODO: Honestly hacking this together at this point...
# 1) Can we just automatically parse the data? That would be nice to not have to declare it here.
#       e.g. Just call it within initialize_db. Need some form of dependency injection to support
#            mocking out in tests.
# 2) DBPools creation, do we need to manually trigger this step?
#       Honestly it may help clean up some of the DB tests.
DBPool.create_pools()
data_parser = Parser()
data_parser.parse_all_data()
db_manager = DBManager()

try:
    db_manager.initialize_db(data_parser)
    raider_dex_bot.run()
finally:
    DBPool.raiddex_pool.close()
    DBPool.telemetry_pool.close()
