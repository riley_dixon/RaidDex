#Start of something cool?
import pathlib

from raiddex.utils.setup_logging import setup_logging

# Setup Logging for RaidDex.
# Configure logging for Dev/Test/Prod envs here.
setup_logging()
