import re
import logging
logger = logging.getLogger(__name__)

from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.models.pokemon.pokemons import Pokemons

class PokemonFactory:

    def __init__(self) -> None:
        pass
    
    def _inject_smeargle_moves(self, smeargle_data: dict, smeargle_moves: dict) -> None:
        """
        Smeargle's fast & charged moveset is stored seperately from it's main data model.

        This breaks shit. Lets keep it in the same place as everywhere else.
        Front End may need a special handling case explaining how Smeargle moves are learned.
        Perhaps a list of moves that cannot be learned is better than what can be learned.
        """
        moves = smeargle_moves["data"]["smeargleMovesSettings"]
        fast_moves = moves["quickMoves"]
        charged_moves = moves["cinematicMoves"]

        smeargle_internal_data = smeargle_data["data"]["pokemonSettings"]
        smeargle_internal_data["quickMoves"] = fast_moves
        smeargle_internal_data["cinematicMoves"] = charged_moves
    
    def _is_pokemon_type_reversion(self, pokemon_name: str) -> bool:
        """
        Test if the pokemon node is for special Pokemon HOME rules.

        True if the template_id contains the identifier "REVERSION" or "FORM_REVERSION".
        False otherwise.

        I honestly have no idea what purpose they currently serve, but we don't want to
        include these nodes in our data.
        """
        reversion_found = re.search(r"HOME(_FORM)?_REVERSION", pokemon_name) is not None
        return reversion_found
    
    def parse_all_pokemon(self, all_pokemon_data: list[dict], smeargle_moves: dict) -> dict[str, Pokemon]:
        """
        Parse all of the Pokemon data in PokeMiner's data.

        Returned dict key is the Pokemon **TEMPLATE_ID**. Value is a parsed `Pokemon` object.
        """
        pokemons = {}

        for pokemon_data in all_pokemon_data:
            # Note: This is PokeMiner's name which includes ID# and Form.
            #logger.debug(f"Currently parsing Pokemon node: {pokemon_data['templateId']}")

            pokemon_name = Pokemon._extract_pokemon_name(pokemon_data["templateId"])
            if "SMEARGLE" == pokemon_name:
                self._inject_smeargle_moves(pokemon_data, smeargle_moves)
            if self._is_pokemon_type_reversion(pokemon_name):
                logger.debug(f"Skipping REVERSION listing: {pokemon_data['templateId']}")
                continue

            pokemon = Pokemon.parse_from_pokeminers(pokemon_data)

            if "NORMAL" in pokemon_name:
                # Overwrite the default non-normal Pokemon. This data supersedes any previous entry.
                # Don't ask me why. But my best observation is that the "NORAML" form tends to carry
                # extra/correct information. (See Dugtrio; non-normal entry has wrong stats)
                pokemon_name = pokemon_name[:-7]
            pokemons[pokemon_name] = pokemon
        
        return pokemons
