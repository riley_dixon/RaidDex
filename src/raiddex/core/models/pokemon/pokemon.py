# Main file that contains data about each Pokemon in the game.
import re
from typing import Optional, Union

from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier
from raiddex.core.models.types.types import PokemonType


# Pokemon with at least one space in the name.
MULTI_WORD_POKEMON = [
    "MR_MIME",
    "HO_OH",
    "MIME_JR",
    "PORYGON_Z",
    "JANGMO_O",
    "HAKAMO_O",
    "KOMMO_O",
    "TAPU_KOKO",
    "TAPU_LELE",
    "TAPU_BULU",
    "TAPU_FINI",
    "MR_RIME"
]
TEMPLATE_ID_FILTER = r"V\d{4}_POKEMON_[A-Z_]+"
STAT_PRODUCT_FACTOR = 1000

class Pokemon:
    """
    Example of Schema in PokeMiner APK Dump

    {
        'data': {
            'pokemonSettings': {
                'animationTime': [<class 'float'>],
                'buddyGroupNumber': <class 'int'>,
                'buddyOffsetFemale': [<class 'float'>],
                'buddyOffsetMale': [<class 'float'>],
                'buddyPortraitOffset': [<class 'float'>],
                'buddyScale': <class 'float'>,
                'buddySize': <class 'str'>,
                'buddyWalkedMegaEnergyAward': <class 'int'>,
                'camera': {
                    'cylinderGroundM': <class 'float'>,
                    'cylinderHeightM': <class 'float'>,
                    'cylinderRadiusM': <class 'float'>,
                    'diskRadiusM': <class 'float'>,
                    'shoulderModeScale': <class 'float'>
                },
                'candyToEvolve': <class 'int'>,
                'cinematicMoves': [<class 'str'>],
                'combatDefaultCameraAngle': [<class 'float'>],
                'combatOpponentFocusCameraAngle': [<class 'float'>],
                'combatPlayerFocusCameraAngle': [<class 'float'>],
                'combatPlayerPokemonPositionOffset': [<class 'float'>],
                'combatShoulderCameraAngle': [<class 'float'>],
                'disableTransferToPokemonHome': <class 'bool'>,
                'eliteCinematicMove': [<class 'str'>],
                'eliteQuickMove': [<class 'str'>],
                'encounter': {
                    'attackProbability': <class 'float'>,
                    'attackTimerS': <class 'float'>,
                    'baseCaptureRate': <class 'float'>,
                    'baseFleeRate': <class 'float'>,
                    'bonusCandyCaptureReward': <class 'int'>,
                    'bonusStardustCaptureReward': <class 'int'>,
                    'bonusXlCandyCaptureReward': <class 'int'>,
                    'cameraDistance': <class 'float'>,
                    'collisionHeadRadiusM': <class 'float'>,
                    'collisionHeightM': <class 'float'>,
                    'collisionRadiusM': <class 'float'>,
                    'dodgeDistance': <class 'float'>,
                    'dodgeDurationS': <class 'float'>,
                    'dodgeProbability': <class 'float'>,
                    'jumpTimeS': <class 'float'>,
                    'maxPokemonActionFrequencyS': <class 'float'>,
                    'minPokemonActionFrequencyS': <class 'float'>,
                    'movementTimerS': <class 'float'>,
                    'movementType': <class 'str'>,
                    'obShadowFormAttackProbability': <class 'float'>,
                    'obShadowFormBaseCaptureRate': <class 'float'>,
                    'obShadowFormDodgeProbability': <class 'float'>
                },
                'evolutionBranch': [
                    {
                        'candyCost': <class 'int'>,
                        'evolution': <class 'str'>,
                        'evolutionItemRequirement': <class 'str'>,
                        'form': <class 'str'>,
                        'genderRequirement': <class 'str'>,
                        'kmBuddyDistanceRequirement': <class 'float'>,
                        'lureItemRequirement': <class 'str'>,
                        'mustBeBuddy': <class 'bool'>,
                        'noCandyCostViaTrade': <class 'bool'>,
                        'obPurificationEvolutionCandyCost': <class 'int'>,
                        'onlyDaytime': <class 'bool'>,
                        'onlyNighttime': <class 'bool'>,
                        'onlyUpsideDown': <class 'bool'>,
                        'priority': <class 'int'>,
                        'questDisplay': [{'questRequirementTemplateId': <class 'str'>}],
                        'temporaryEvolution': <class 'str'>,
                        'temporaryEvolutionEnergyCost': <class 'int'>,
                        'temporaryEvolutionEnergyCostSubsequent': <class 'int'>
                    }
                ],
                'evolutionIds': [<class 'str'>],
                'evolutionPips': <class 'int'>,
                'familyId': <class 'str'>,
                'form': <class 'str'>,
                'formChange': [
                    {
                        'availableForm': [<class 'str'>],
                        'candyCost': <class 'int'>,
                        'stardustCost': <class 'int'>
                    }
                ],
                'heightStdDev': <class 'float'>,
                'isDeployable': <class 'bool'>,
                'isTradable': <class 'bool'>,
                'isTransferable': <class 'bool'>,
                'kmBuddyDistance': <class 'float'>,
                'modelHeight': <class 'float'>,
                'modelScale': <class 'float'>,
                'modelScaleV2': <class 'float'>,
                'obPreviewPokemonSetting': {
                    'obPreviewFloat1': <class 'float'>,
                    'obPreviewFloat10': <class 'float'>,
                    'obPreviewFloat2': <class 'float'>,
                    'obPreviewFloat3': <class 'float'>,
                    'obPreviewFloat4': <class 'float'>,
                    'obPreviewFloat5': <class 'float'>,
                    'obPreviewFloat6': <class 'float'>,
                    'obPreviewFloat7': <class 'float'>,
                    'obPreviewFloat8': <class 'float'>,
                    'obPreviewFloat9': <class 'float'>
                },
                'parentPokemonId': <class 'str'>,
                'pokedexHeightM': <class 'float'>,
                'pokedexWeightKg': <class 'float'>,
                'pokemonId': <class 'str'>,
                'quickMoves': [<class 'str'>],
                'raidBossDistanceOffset': <class 'float'>,
                'rarity': <class 'str'>,
                'shadow': {
                    'purificationCandyNeeded': <class 'int'>,
                    'purificationStardustNeeded': <class 'int'>,
                    'purifiedChargeMove': <class 'str'>,
                    'shadowChargeMove': <class 'str'>
                },
                'stats': {
                    'baseAttack': <class 'int'>,
                    'baseDefense': <class 'int'>,
                    'baseStamina': <class 'int'>
                },
                'tempEvoOverrides': [
                    {
                        'averageHeightM': <class 'float'>,
                        'averageWeightKg': <class 'float'>,
                        'buddyOffsetFemale': [<class 'float'>],
                        'buddyOffsetMale': [<class 'float'>],
                        'buddyPortraitOffset': [<class 'float'>],
                        'camera': {
                            'cylinderGroundM': <class 'float'>,
                            'cylinderHeightM': <class 'float'>,
                            'cylinderRadiusM': <class 'float'>
                        },
                        'modelHeight': <class 'float'>,
                        'modelScaleV2': <class 'float'>,
                        'stats': {
                            'baseAttack': <class 'int'>,
                            'baseDefense': <class 'int'>,
                            'baseStamina': <class 'int'>
                        },
                        'tempEvoId': <class 'str'>,
                        'typeOverride1': <class 'str'>,
                        'typeOverride2': <class 'str'>
                    }
                ],
                'thirdMove': {
                    'candyToUnlock': <class 'int'>,
                    'stardustToUnlock': <class 'int'>
                },
                'type': <class 'str'>,
                'type2': <class 'str'>,
                'weightStdDev': <class 'float'>
            },
            'templateId': <class 'str'>
        },
        'templateId': <class 'str'>
    }
    """

    @staticmethod
    def _extract_pokemon_number(template_id: str) -> int:
        """
        Extract the Pokemon PokeDex number out of the PokeMiner ID.
        """
        pattern = r"^V(?P<id>\d+)_"
        match = re.search(pattern, template_id)
        if match is None:
            raise ValueError(
                f"Cannot find pokemon number in {template_id}."
            )
        return int(match["id"])
    
    @staticmethod
    def _extract_pokemon_name(template_id: str) -> str:
        """
        Name includes ALOLA or GALARIAN or NORMAL where pointed to.
        """
        pattern = r"V\d{4}_POKEMON_(?P<name>[A-Z0-9_]+)"
        match = re.match(pattern, template_id)
        if match is None:
            # TODO: Make specific exception
            raise ValueError(
                f"Unable to find Pokemon Name in: {template_id}"
            )
        return match["name"]
    
    @staticmethod
    def _extract_pokemon_form(template_id: str) -> str:
        """
        Extract the Pokemon Form name out of the PokeMiner ID.

        If no form is found (i.e. just the pokemon name), then form is set to "NORMAL".

        This is not straightforward. PokeMiner's uses the "FORM" inclusively for
        species forms, regional forms, and costumes. We will need an additional field to
        track for costumes.
        """
        pattern = r"V\d{4}_POKEMON_(?P<name>[A-Z0-9_]+)"
        match = re.match(pattern, template_id)
        if match is None:
            # TODO: Make specific exception
            raise ValueError(
                f"Unable to find Pokemon Name in: {template_id}"
            )
        pokemon_name = match["name"]
        
        form = None
        for multi_word_name in MULTI_WORD_POKEMON:
            if multi_word_name in pokemon_name:
                form = pokemon_name[len(multi_word_name)+1:]
                if form == "":
                    # If form name not included in templateId.
                    form = "NORMAL"

        if form is None:  # Single word pokemon name
            if "_" not in pokemon_name:
                form = "NORMAL"
            else:
                form = pokemon_name.split("_", maxsplit=1)[1]

        return form

    @staticmethod
    def _extract_pokemon_types(type1: str, type2: Optional[str] = None) -> list[PokemonType]:
        """
        Convert type data stored in PokeMiners to `PokemonType`.

        Returns a 1 or 2 member list of the Pokemon's types.
        """
        types = []
        for _type in [t for t in [type1, type2] if t is not None]:
            types.append(PokemonType.from_str(_type))
        #__types = [PokemonType.from_str(_type) for t in [type1, type2] if t is not None]
        return types
    
    @classmethod
    def parse_from_pokeminers(cls, pokemon_data):
        """
        Create a Pokemon object from PokeMiner's data.

        Note: This may include data patched in from another source.
        """
        pokeminer_id = pokemon_data["templateId"]
        pokemon_data = pokemon_data["data"]["pokemonSettings"]
        _id = cls._extract_pokemon_number(pokeminer_id)
        form = cls._extract_pokemon_form(pokeminer_id)
        types = cls._extract_pokemon_types(pokemon_data["type"], pokemon_data.get("type2"))
        
        parsed_data = {}
        parsed_data["name"] = pokemon_data["pokemonId"]
        parsed_data["name_alias"] = pokemon_data.get("name_alias", None)
        parsed_data["form"] = form
        parsed_data["form_alias"] = pokemon_data.get("form_alias", None)
        parsed_data["id"] = _id
        parsed_data["types"] = types
        parsed_data["base_attack"] = pokemon_data["stats"]["baseAttack"]
        parsed_data["base_defense"] = pokemon_data["stats"]["baseDefense"]
        parsed_data["base_stamina"] = pokemon_data["stats"]["baseStamina"]

        return cls(**parsed_data)

    @classmethod
    def parse_from_postgres(cls, values):
        """
        Create a Pokemon object from data stored in PostgreSQL.
        """
        types = [values.pop("type1"), values.pop("type2")]
        
        values["types"] = [PokemonType[t] for t in types if t is not None]

        return cls(**values)
    
    def __init__(
        self,
        name: str=None,
        name_alias: Optional[str]=None,
        form: str=None,
        form_alias: Optional[str]=None,
        id: int=0,
        types: PokemonType=None,
        base_attack: int=0,
        base_defense: int=0,
        base_stamina: int=0
    ):
        """
        Create a new Pokemon object.

        All data is purely optional in the default constructor, but it may cause unexpected
        behaviour when used if left None. It is recommended that one of the classmethods be
        used when creating new Pokemon.

        Note: Form is a tricky parameter. PokeMiner's uses it to encompass species forms,
              regional forms, and costumes all in one parameter. While it is not complicated
              to extract, it does mean we have to be careful with how we treat forms. For example,
              it is likely not helpful to use costume forms in simulations.
        """
        self.name = name
        self.name_alias = name_alias
        self.form = form
        self.form_alias = form_alias
        self.id = id
        self._types = []  # Property
        self.base_attack = base_attack
        self.base_defense = base_defense
        self.base_stamina = base_stamina

        if types is not None:
            self.types = types
    
    def __eq__(self, other_pokemon):
        """
        Return True if all attributes are the same, otherwise return False.
        """
        if not isinstance(other_pokemon, self.__class__):
            raise TypeError(f"Cannot compare type '{self.__class__}' to type '{type(other_pokemon)}'")
        return vars(self) == vars(other_pokemon)

    @property
    def types(self) -> list[PokemonType]:
        """
        Return the type/types of the Pokemon.
        """
        return self._types

    @types.setter
    def types(self, new_types: Union[PokemonType, list[PokemonType]]) -> None:
        """
        Set Pokemon.types. Enforces type correctness.

        Only accepts List[PokemonType] or PokemonType. All others wrong.
        If PokemonType - Only one type allowed.
        If List[PokemonType] - 1 or 2 elements are allowed.
        All others will raise an TypeError or ValueError.
        """
        _types = []
        if isinstance(new_types, PokemonType):
            _types = [new_types]
        elif isinstance(new_types, list):
            list_size = len(new_types)
            if list_size not in [1, 2]:
                raise ValueError(
                    f"List new_types has an unexpected number of elements. "
                    f"Number of Elements: {list_size} != [1,2]. "
                    f"List: {new_types}"
                )
            for new_type in new_types:
                if not isinstance(new_type, PokemonType):
                    raise TypeError(
                        "Unexpected element type found in `new_types` list: "
                        f"'{new_type}' of type '{type(new_type)}'."
                    )
                _types.append(new_type)
        else:
            raise TypeError(
                f"Pokemon.types expecting a PokemonTypeIndex or List[PokemonTypeIndex]. "
                f"Received a '{type(new_types)}' type of value '{new_types}'."
            )
        self._types = _types

    @property
    def type1(self):
        """
        Helper property for nicer DB code.

        Note: DB currently stores a string of the type. Ideally I would
        like to replace this with an Enum within the database.
        """
        if len(self._types) < 1:
            return None
        return self._types[0].name

    @property
    def type2(self):
        """
        Helper property for nicer DB code.

        Note: DB currently stores a string of the type. Ideally I would
        like to replace this with an Enum within the database.
        """
        if len(self._types) < 2:
            return None
        return self._types[1].name

    #     self.fast_moves = data["quickMoves"]
    #     self.charged_moves = data["cinematicMoves"]

    #     self.evolution = data.get("evolutionIds") # Default if no special requirements
    #     self.evolves_from = data.get("parentPokemonId")
    #     self.evolution_candy_cost = data.get("candyToEvolve") # Default if no special requirements

    #     self.average_height = data["pokedexHeightM"]
    #     self.height_std_dev = data["heightStdDev"]
    #     self.average_weight = data["pokedexWeightKg"]
    #     self.weight_std_dev = data["weightStdDev"]

    #     self.family = data["familyId"]

    def get_live_pokemon(
        self,
        attack_iv: int = 0,
        defense_iv: int = 0,
        stamina_iv: int = 0,
        level: int | float = 1
    ):
        """
        Create a `LivePokemon` object from this Pokemon base data.

        Author's note:
        While we use inheritance here to be able to easily access `Pokemon` attributes
        from a `LivePokemon`, it may make more sense through composition and modifying
        the __getattr__ method to fallback on the `Pokemon` type. The drawback however
        is this kind of attribute reading is less visible to static tools.

        It would be worthwhile to create some kind of reflective method so we do not
        need to maintain this parameter list as we expand `Pokemon`'s attributes.
        """
        live_pokemon = LivePokemon(
            name=self.name,
            name_alias=self.name_alias,
            form=self.form,
            form_alias = self.form_alias,
            id=self.id,
            types=self.types,
            base_attack=self.base_attack,
            base_defense=self.base_defense,
            base_stamina=self.base_stamina,
            attack_iv=attack_iv,
            defense_iv=defense_iv,
            stamina_iv=stamina_iv,
            level=level
        )
        return live_pokemon


class LivePokemon(Pokemon):
    """
    This is supposed to represent a Pokemon that could possibly exist in the game.

    Specifically, unlike PokemonTemplate which is more like a recipe, this one
    can store IVs, what Moves are registered, and other pertinent information.
    """
    def __init__(
        self,
        attack_iv: int = 0,
        defense_iv: int = 0,
        stamina_iv: int = 0,
        fast_move = None,
        charged_moves = None,
        level: int | float = 1,
        **kwargs
    ) -> None:
        super().__init__(**kwargs)
        self._attack_iv = None
        self._defense_iv = None
        self._stamina_iv = None
        self.attack_iv = attack_iv
        self.defense_iv = defense_iv
        self.stamina_iv = stamina_iv

        self._attack = None
        self._defense = None
        self._stamina = None

        self._cpm = None  # Leave above self.level.
        self._level = None
        self.level = level
        
        self._cp = None

        self.fast_move = fast_move
        self.charged_moves = charged_moves        

    def _iv_check(self, iv: int):
        if not isinstance(iv, int):
            raise TypeError(f"IV must be of type int, not {type(iv)}.")
        if iv < 0 or iv > 15:
            raise ValueError(f"IV must be within the inclusive range of 0-15, not {iv}.")

    @property
    def attack_iv(self):
        return self._attack_iv

    @attack_iv.setter
    def attack_iv(self, attack_iv: int):
        self._iv_check(attack_iv)
        
        self._cp = None
        self._attack = None
        self._attack_iv = attack_iv

    @property
    def defense_iv(self):
        return self._defense_iv

    @defense_iv.setter
    def defense_iv(self, defense_iv: int):
        self._iv_check(defense_iv)
        
        self._cp = None
        self._defense = None
        self._defense_iv = defense_iv

    @property
    def stamina_iv(self):
        return self._stamina_iv

    @stamina_iv.setter
    def stamina_iv(self, stamina_iv: int):
        self._iv_check(stamina_iv)
        
        self._cp = None
        self._stamina = None
        self._stamina_iv = stamina_iv
    
    @property
    def attack(self):
        if self._attack is None:
            self._attack = self.base_attack + self._attack_iv
        return self._attack

    @property
    def scaled_attack(self):
        """
        Attack scaled to level.
        """
        return self.attack * self._cpm

    @property
    def defense(self):
        if self._defense is None:
            self._defense = self.base_defense + self._defense_iv
        return self._defense

    @property
    def scaled_defense(self):
        """
        Defense scaled to level.
        """
        return self.defense * self._cpm

    @property
    def stamina(self):
        if self._stamina is None:
            self._stamina = self.base_stamina + self._stamina_iv
        return self._stamina
    
    @property
    def scaled_stamina(self):
        """
        Stamina scaled to level.
        """
        return self.stamina * self._cpm

    @property
    def level(self):
        return self._level
    
    @level.setter
    def level(self, new_level):
        # Leave correctness checks to CPMultiplier
        self._cpm = CPMultiplier[new_level]
        self._level = new_level
        self._cp = None

    @property
    def cp(self):
        if self._cp is None:
            _cp = int(self.attack * self.defense**0.5 * self.stamina**0.5 * self._cpm**2 / 10)
            self._cp = max(10, _cp)
        return self._cp
    
    @property
    def stat_product(self):
        stat_prod = (
            self.attack * self.defense * self._cpm**2 * int(self.stamina * self._cpm) / STAT_PRODUCT_FACTOR
        )
        return stat_prod