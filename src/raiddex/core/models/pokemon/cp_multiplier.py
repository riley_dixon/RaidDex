"""
Poke Miners CP_Multilper (found under templateId "PLAYER_LEVEL_SETTINGS")
only contains data for each full level.

To skip needlessly complicating the formula, and then adding in a cache,
lets just hardcode in the values for now.

Consider this more dynamic approach a stretch goal ;)
"""
import logging
logger = logging.getLogger("CPMultiplier")
logger.setLevel("INFO")

from typing import Union

MAX_LEVEL = 51

class MetaCPMultiplier(type):
    # Because I want a the CPMultiplier class to be subscriptable....
    # Why do I need this hack? To preserve the interface if I want
    # to make the CPMultiplier calculations dynamic vs hardcoded.
    def __getitem__(cls, level):
        return cls.__getitem__(level)


class CPMultiplierException(Exception):
    pass

class CPMultiplier(metaclass=MetaCPMultiplier):
    _CP_MULTIPLIERS = []

    @classmethod
    def __getitem__(cls, level: Union[int, float]) -> float:
        """
        Level should be 1 indexed. CPMultiplier will deal with finding the right index.
        """
        if level < 1 or 51 < level:
            raise IndexError(
                "Invalid Level provided. Needs to be within the range of 1-51 "
                f"inclusively. Received: {level}."
            )
        if level % 0.5 != 0:
            raise ValueError(
                f"Invalid Level provided. It must be divisable by 0.5. Received: {level}."
            )
        cpm = cls._CP_MULTIPLIERS[int((level - 1) * 2)]

        return cpm

    @classmethod
    def _calculate_half_level_cpm(cls, cpm_level, cpm_level_plus_one) -> float:
        """
        Calculate the CPM for half levels.

        Store this so we don't have to keep reecalculating it.
        """
        half_cpm = (cpm_level**2/2 + cpm_level_plus_one**2/2)**0.5
        return half_cpm

    @classmethod
    def parse_from_data(cls, cpm_data: list) -> None:
        """
        Calculate and store the CPM data from the PokeMiner dump.

        CPM data is stored within the class itself.

        CPM data should be considered immutable (see below note). If this function
        is called multiple times throughout the lifespan of the class (e.g running
        multiple test suites each parsing CPM data), this function will act as a no-op
        on all but the first.

        NOTE: These values are assumed to be steady state and should have no reason to change.
        """
        if len(cls._CP_MULTIPLIERS) != 0:
            return

        logger.debug(f"Calculating CPM upto Level {MAX_LEVEL}")
        for level in range(0, MAX_LEVEL):
            cpm_level = cpm_data[level]
            cls._CP_MULTIPLIERS.append(cpm_level)
            logger.debug(f"CPM for level {level + 1} is: {cpm_level}")
            cpm_half_level = cls._calculate_half_level_cpm(cpm_data[level], cpm_data[level+1])
            cls._CP_MULTIPLIERS.append(cpm_half_level)
            logger.debug(f"CPM for level {level + 1}.5 is: {cpm_half_level}")
        
        cls._CP_MULTIPLIERS.pop() # MAX_LEVEL and a half does not exist.

        if len(cls._CP_MULTIPLIERS) != MAX_LEVEL * 2 - 1:
            raise CPMultiplierException(
                f"Expected to have {MAX_LEVEL * 2} entried in CPM. "
                f"Instead have {len(cls._CP_MULTIPLIERS)}.")

    @classmethod
    def get_closest_level(cls, given_cpm: float):
        """
        Given a CPM, reverse calculate what the highest possible level could be.

        The returned level will have a corresponding known CPM <= the given CPM.
        """
        highest_possible_cpm = 0
        for known_cpm in cls._CP_MULTIPLIERS:
            if known_cpm > given_cpm:
                break
            highest_possible_cpm = known_cpm
        closest_level = (cls._CP_MULTIPLIERS.index(highest_possible_cpm) / 2) + 1

        return closest_level
