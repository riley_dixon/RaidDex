from copy import deepcopy

from raiddex.core.models.pokemon.pokemon import Pokemon
"""
Note: Possibly deprecated code as we have now transitioned to Postgres
    for data storage. The only thing this may help with is a fuzzy algo
    for querying pokemon names without an exact match.
"""
class Pokemons():
    
    def __init__(self):
        self.pokemons = {}
    
    def __getitem__(self, pokemon_name):
        """
        In case we want to have any kind of secondary search algorithm.

        Returns a copy of the move to avoid propagating accidental changes.

        Primary - Name (duh)
        Secondary - Id

        Could skip Primary altogether if the "pokemon_name" provided is an int.
        One issue is that ID number cannot uniquely identify some pokemon.
        A sophisticated search could maybe parse a "A", "G", "H", or "P" qualifier
        from a number to understand what is actually being looked for.

        Possibly better to use a "search" method rather than __getitem__?
        Although at the end of the day it is just syntax sugar.
        """
        pokemon = self.pokemons.__getitem__(pokemon_name)
        return deepcopy(pokemon)

    def __setitem__(self, pokemon_name, pokemon_obj):
        if not isinstance(pokemon_obj, Pokemon):
            raise TypeError(f"{Pokemons} only can contain items of type {Pokemon}, not {type(pokemon_obj)}.")
        self.pokemons[pokemon_name] = pokemon_obj