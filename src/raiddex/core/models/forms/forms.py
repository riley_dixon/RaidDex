# Parser File for each PvE Move

import re

TEMPLATE_ID_FILTER = r"FORMS_V\d{4}_POKEMON_[A-Z_]+"

class Form:

    def __init__(self, form_name, is_costume):
        self.form_name = form_name
        self.is_costume = is_costume

class Forms:
    """
    {
        'data': {
            'formSettings': {
                'forms': 
                    [{
                        'assetBundleSuffix': <class 'str'>,
                        'assetBundleValue': <class 'int'>,
                        'form': <class 'str'>,
                        'isCostume': <class 'bool'>
                    }],
                'pokemon': <class 'str'>
            },
            'templateId': <class 'str'>
        },
        'templateId': <class 'str'>
    }
    """

    @classmethod
    def parse_from_data(cls, pm_data):
        return cls(data=pm_data["data"])

    def _parse_forms(self, forms):
        """Form form form, form form, form Form form."""
        self.forms = []
        for form in forms:
            try:
                form_name = form["form"]
            except KeyError:
                # Log Debug that Pokemon X had an empty form entry
                continue
            else:
                form = Form(form_name, form.get("isCostume", False))
                self.forms.append(form)
    
    def __init__(self, data=None):
        data = data["formSettings"]

        # Some pokemon do not have form entries. The data is not consistent.
        # For those missing a "Form", point to the default "Pokemon" data.
        # For those that do have a single form (usually denoted as NORMAL),
        # we take the NORMAL form over the default form.
        self.pokemon_name = data["pokemon"]
        try:
            forms = data["forms"]
        except KeyError:
            #self.forms = [Form(self.pokemon_name, False)]
            self.forms = [] # No forms
        else:
            self.forms = self._parse_forms(forms)
