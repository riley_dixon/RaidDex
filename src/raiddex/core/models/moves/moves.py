from copy import deepcopy

from raiddex.core.models.moves.move import Move

class Moves():
    
    def __init__(self):
        self.moves = {}
    
    def __getitem__(self, move_name):
        """
        In case we want to have any kind of secondary search algorithm.

        Returns a copy of the move to avoid propagating accidental changes.
        """
        move = self.moves.__getitem__(move_name)

        return deepcopy(move)
    
    def __setitem__(self, move_name, move_obj):
        if not isinstance(move_obj, Move):
            raise TypeError(f"{Moves} only can contain items of type {Move}, not {type(move_obj)}.")
        self.moves[move_name] = move_obj
