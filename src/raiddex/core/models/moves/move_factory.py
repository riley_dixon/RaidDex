import re
import logging
logger = logging.getLogger(__name__)

from raiddex.core.models.moves.moves import Moves
from raiddex.core.models.moves.move import Move
from raiddex.core.models.moves.pve_move import PvEMove
from raiddex.core.models.moves.pvp_move import PvPMove

class MoveFactory:

    def __init__(self) -> None:
        pass

    def create_move(self, move_name, pve_data, pvp_data) -> Move:
        # logger.debug(f"Found matching PvE and PvP moves for '{move_name}'.")
        pve_move = PvEMove.parse_from_data(pve_data)
        pvp_move = PvPMove.parse_from_data(pvp_data)
        move = Move(move_name, pve_data, pvp_data)

        return move
    
    def _get_move_name_from_pvp(self, pvp_data):
        return pvp_data["data"]["combatMove"]["uniqueId"]
    
    def _find_matching_pve_move(self, move_name, pve_moves_data):
        for pve_move in pve_moves_data:
            if move_name in pve_move["templateId"]:
                return pve_move
        logger.error(f"No matching PvE move for {move_name}.")

        return None
    
    def parse_all_moves(self, pve_moves_data: list[dict], pvp_moves_data: list[dict]):
        """
        Parse all the moves from the PokeMiners list.
        """
        move_list = Moves()
        for pvp_move_data in pvp_moves_data:
            try:
                move_name = self._get_move_name_from_pvp(pvp_move_data)
                if "BLASTOISE" in move_name:
                    # I not Gamepress know why this is in PokeMiners datadump.
                    logger.warning(f"Skipping Unknown/Unused Blastoise move: {move_name}")
                    continue

                pve_move_data = self._find_matching_pve_move(move_name, pve_moves_data)
                if pve_move_data is None:
                    continue

                move = self.create_move(move_name, pve_move_data, pvp_move_data)
                move_list[move_name] = move
            except Exception:
                logger.error(f"Failed to parse move '{move_name}'.")
                logger.error(f"Move data: {pvp_move_data}")
                raise

        return move_list
