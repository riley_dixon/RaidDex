# Parser File for each PvE Move

import re

from raiddex.core.models.types.types import PokemonType

TEMPLATE_ID_FILTER = r"V\d{4}_MOVE_[A-Z_]+"

class PvEMove:
    """
    {
        'data': {
            'moveSettings': {
                'accuracyChance': <class 'float'>,
                'animationId': <class 'int'>,
                'criticalChance': <class 'float'>,
                'damageWindowEndMs': <class 'int'>,
                'damageWindowStartMs': <class 'int'>,
                'durationMs': <class 'int'>,
                'energyDelta': <class 'int'>,
                'healScalar': <class 'float'>,
                'isLocked': <class 'bool'>,
                'movementId': <class 'str'>,
                'pokemonType': <class 'str'>,
                'power': <class 'float'>,
                'staminaLossScalar': <class 'float'>,
                'trainerLevelMax': <class 'int'>,
                'trainerLevelMin': <class 'int'>,
                'vfxName': <class 'str'>
            },
            'templateId': <class 'str'>
        },
        'templateId': <class 'str'>
    }
    """

    @classmethod
    def parse_from_data(cls, pm_data):
        return cls(data=pm_data["data"])

    def __init__(self, data=None):
        data = data["moveSettings"]
        self.name = data["movementId"]
        self.type = PokemonType.from_str(data["pokemonType"])
        self.power = data.get("power", 0)  # ...come on splash...
        self.energy = data.get("energyDelta", 0)  # ...come on struggle...
        self.move_duration = data["durationMs"]
