# Parser File for each PvP Move

import re

from raiddex.core.models.types.types import PokemonType

TEMPLATE_ID_FILTER = r"COMBAT_V\d{4}_MOVE_[A-Z_]+"

class PvPMove:
    """
    {
        'data': {
            'combatMove': {
                'buffs': {
                    'attackerAttackStatStageChange': <class 'int'>,
                    'attackerDefenseStatStageChange': <class 'int'>,
                    'buffActivationChance': <class 'float'>,
                    'targetAttackStatStageChange': <class 'int'>,
                    'targetDefenseStatStageChange': <class 'int'>
                },
                'durationTurns': <class 'int'>,
                'energyDelta': <class 'int'>,
                'power': <class 'float'>,
                'type': <class 'str'>,
                'uniqueId': <class 'str'>,
                'vfxName': <class 'str'>
            },
            'templateId': <class 'str'>
        },
        'templateId': <class 'str'>
    }
    """

    @classmethod
    def parse_from_data(cls, pm_data):
        return cls(data=pm_data["data"])
    
    def __init__(self, data=None):
        data = data["combatMove"]
        self.name = data["uniqueId"]
        self.type = PokemonType.from_str(data["type"])
        self.power = data.get("power", 0) # lol splash/transform
        # Fast (+) / Charged (-)
        self.energy = data.get("energyDelta", 0) # Transform (fast move) has no energydelta
        self.buffs = data.get("buffs", None)
