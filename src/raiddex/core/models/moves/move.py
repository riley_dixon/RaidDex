# More of a dataclass to containerize PvP and PvE move data.

class Move:
    
    def __init__(self, move_name, pvp_stats, pve_stats):
        self.move_name = move_name
        self.pvp_stats = pvp_stats
        self.pve_stats = pve_stats
    
    @property
    def pvp(self):
        return self.pvp_stats
    
    @property
    def pve(self):
        return self.pve_stats
    
    def is_fast(self):
        pass

    def is_charged(self):
        pass
