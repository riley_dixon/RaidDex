# Parser File for each Type Matchups
from enum import IntEnum

import re

TEMPLATE_ID_FILTER = r"POKEMON_TYPE_[A-Z]+"

class PokemonTypeNotInitialized(Exception):
    """Raised if using this class before all types have been added."""
    pass

class PokemonType(IntEnum):
    """
    Enum value corresponds to the index of the type modifiers stored by PokeMiners.

    Type Modifier data is stored as an unlabeled array of int's. The order is rather
    arbitrary, so we use this as a map to identify which array member corresponds to
    which type.
    """
    NORMAL = 0
    FIGHTING = 1
    FLYING = 2
    POISON = 3
    GROUND = 4
    ROCK = 5
    BUG = 6
    GHOST = 7
    STEEL = 8
    FIRE = 9
    WATER = 10
    GRASS = 11
    ELECTRIC = 12
    PSYCHIC = 13
    ICE = 14
    DRAGON = 15
    DARK = 16
    FAIRY = 17

    @classmethod
    def from_str(cls, attack_type: str) -> str:
        """
        Convert the string PokeMiner's uses to store type data to PokemonType.
        """
        pattern = r"^POKEMON_TYPE_(?P<type_name>[A-Z]+)"
        match = re.match(pattern, attack_type)
        if match is None:
            raise ValueError(
                f"Unable to find pokemon type name in: '{attack_type}'."
            )
        return cls[match["type_name"]]

class TypeMatchups:
    """
    Since the schema is not aware of the importance of each float,
    an example of the schema with annotations is recorded instead.

    templateId is the type of the attack.
    attackScalar is the multiplier against a particular type.
    0.390625 - "No Effect"
    0.625 - "Not Very Effective"
    1.0 - "Effective"
    1.6 - "Super Effective"

    {
        "templateId": "POKEMON_TYPE_X",
        "data": {
            "templateId": "POKEMON_TYPE_X",
            "typeEffective": {
                "attackScalar": [
                    1.0,     # Normal Pokemon
                    1.0,     # Fighting Pokemon
                    1.0,     # Flying Pokemon
                    1.0,     # Poison Pokemon
                    1.0,     # Ground Pokemon
                    1.0,     # Rock Pokemon
                    1.0,     # Bug Pokemon
                    1.0,     # Ghost Pokemon
                    0.625,   # Steel Pokemon
                    1.0,     # Fire Pokemon
                    1.0,     # Water Pokemon
                    1.0,     # Grass Pokemon
                    1.0,     # Electric Pokemon
                    1.0,     # Psychic Pokemon 
                    1.0,     # Ice Pokemon
                    1.6,     # Dragon Pokemon
                    1.0,     # Dark Pokemon
                    0.390625 # Fairy Pokemon
                ],
                "attackType": "POKEMON_TYPE_DRAGON"
            }
        }
    }
    """
    _INITIALIZED = False
    _TYPE_MAP = {}

    @classmethod
    def _check_if_initialized(cls) -> bool:
        if cls._INITIALIZED:
            return True

        for pokemon_type in PokemonType:
            if pokemon_type not in cls._TYPE_MAP:
                return False
        
        cls._INITIALIZED = True
        return True

    @classmethod
    def register_type_from_data(cls, pm_data) -> None:
        """
        Store the type modifier data from PokeMiners in this class.

        These modifiers are unlikely to change, however they do differ
        from main series Pokemon games. So we rely on PokeMiner's data
        for what these values currently are.
        """
        type_data = pm_data["data"]["typeEffective"]
        if len(type_data["attackScalar"]) != len(PokemonType):
            raise ValueError(
                f"{pm_data['templateId']} has an incorrect number of types."
            )
        _type = PokemonType.from_str(type_data["attackType"])
        cls._TYPE_MAP[_type] = type_data["attackScalar"]
        cls._check_if_initialized()

    @classmethod
    def get_type_multiplier(cls, attack_type: PokemonType, defender_types: list[PokemonType]) -> float:
        """
        Calculate the type damage modifier given an attacking type, and the defender's types.
        """
        if not cls._INITIALIZED:
            raise PokemonTypeNotInitialized
        if isinstance(attack_type, PokemonType):
            attack_vector = cls._TYPE_MAP[attack_type]
        else:
            raise TypeError(
                "attack_type must be of type PokemonType. Is: %s"
                % type(attack_type)
            )

        if len(defender_types) != 1 and len(defender_types) != 2:
            raise ValueError(
                "defender_type must be a list of 1 or 2 elements. Got: %s"
                % defender_types
            )
        defender_vector = []
        # TODO: Re-evaluate this loop as `Pokemon` now enforces PokemonType.
        for defender_type in defender_types:
            if isinstance(defender_type, PokemonType):
                defender_vector.append(defender_type)
            else:
                raise TypeError(
                    "Elements of defender_type must be of type PokemonType"
                    ". Is: %s" % type(defender_type)
                )

        multiplier = 1
        # TODO: Can we simplify this and do this in the previous for loop?
        for defender_type in defender_vector:
            multiplier *= attack_vector[defender_type.value]
        return multiplier

def get_type_obj(type_name: str) -> PokemonType:
    """
    Convert the name of a pokemon type into a `PokemonType` object.

    Raises KeyError if `type_name` does not match a `PokemonType`.
    """
    return PokemonType[type_name]
