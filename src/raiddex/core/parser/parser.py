import json
import logging
import re
from importlib.resources import files
from typing import Any

from raiddex.core.models.types.types import TypeMatchups
from raiddex.core.models.moves.move_factory import MoveFactory
from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier
from raiddex.core.models.pokemon.pokemon_factory import PokemonFactory

PM_GAME_DATA_PATH = files("raiddex.assets").joinpath("pokeminer_game_master.json")
CUSTOM_DATA_PATH = files("raiddex.assets").joinpath("custom_game_master.json")

parser_log = logging.getLogger(__name__)

class Parser:

    def __init__(self):
        self.raw_data: None | list[dict[str, Any]] = None
        self.raw_custom_data: None | dict[str, Any] = None
        self.custom_default_forms: None | dict[str, str] = None
        self.raw_forms = None
        self.raw_pvp_moves = []
        self.raw_pve_moves = []
        self.raw_pokemon = []
        self.raw_types = []

        self.pokemons = None
        self.moves = None

    def _merge_data(self, poke_miner_data: dict, custom_data: dict) -> None:
        """
        Iterative approach to deep merging "custom_data" into "poke_miner_data".

        This performs a deep merge operation on poke_miner_data. This will allow us to insert
        custom data into the PokeMiner data. Custom Data will be formatted similarly to how 
        PokeMiner's is currently structured.

        This deep merge operates on the following instructions:
        1. Get an element from custom_data, keeping record of it's location.
        2. Compare the current element between custom_data and poke_miner_data:
            a. If the element does not exist in poke_miner_data, insert it into poke_miner_data as-is and goto Step 4.
            b. If the element does exist in poke_miner_data and is of the same type, goto Step 3.
            c. If the element does exist in poke_miner_data and is a different type, raise a TypeError.
        3. Overwrite the existing element in poke_miner_data
            a. If the current element in custom_data is a map, recurse into the map and process those elements.
            b. If the current element in custom_data is a primitive, overwrite that field in poke_miner_data.
            c. If the current element in custom_data is a sequence, extend the sequence in poke_miner_data.
        4. Go to the next element until all elements in custom_data have been merged in.
        """
        dicts_to_work_on = [(poke_miner_data, custom_data)]
        while dicts_to_work_on:
            pm_dict, custom_dict = dicts_to_work_on.pop()
            for custom_key, custom_value in custom_dict.items():
                pm_dict_value = pm_dict.get(custom_key, None)
                if pm_dict_value is None:
                    pm_dict[custom_key] = custom_value
                elif not isinstance(custom_value, type(pm_dict_value)):
                    raise TypeError(f"({custom_value} - {type(custom_value)}) != ({pm_dict_value} - {type(pm_dict_value)}) for '{custom_key}'.")
                elif isinstance(custom_value, dict):
                    dicts_to_work_on.append((pm_dict_value, custom_value))
                elif isinstance(custom_value, list):
                    pm_dict_value.extend(custom_value)
                else:
                    pm_dict[custom_key] = custom_value
    
    def parse_all_data(self) -> None:
        """
        Parse all PokeMiner data.

        TODO: Include the possibility of merging in secondary data to add to/replace
            PokeMiner data. This could be for anything.

        Ultimately, this should get passed into a Parser for a particular
        source of data. But for the time being lets create it here.
        """
        with PM_GAME_DATA_PATH.open("r") as game_data_json:
            self.raw_data: list[dict] = json.load(game_data_json)
        with CUSTOM_DATA_PATH.open("r") as custom_data_json:
            self.raw_custom_data: dict[str, Any] = json.load(custom_data_json)
            self.custom_default_forms = self.raw_custom_data["default_forms"]

        for node in self.raw_data:
            node_key = node["templateId"]

            if node_key in self.raw_custom_data["pokemon_data"]:
                # Since the root of PokeMiner's data is a sequence, we unfortunately need to check
                # every element to see if there is any custom data that needs to be added.
                self._merge_data(node, self.raw_custom_data["pokemon_data"][node_key])

            if node.get("raiddex_disable", False):
                # Do not parse this node, Do not insert it into the DB.
                continue

            if re.match(r"V\d{4}_POKEMON_[A-Z0-9_]+", node_key): # Parse a Pokemon                    
                self.raw_pokemon.append(node)
            elif re.match(r"POKEMON_TYPE_[A-Z]+", node_key):  # Parse a Type
                self.raw_types.append(node)
            elif re.match(r"V\d{4}_MOVE_[A-Z_]+", node_key):  # Parse a PVE Move Stats
                self.raw_pve_moves.append(node)
            elif re.match(r"COMBAT_V\d{4}_MOVE_[A-Z_]+", node_key):  # Parse a PVP Move Stats
                self.raw_pvp_moves.append(node)
            elif re.match(r"SMEARGLE_MOVES_SETTINGS", node_key):
                raw_smeargle_moves = node
            elif re.match(r"PLAYER_LEVEL_SETTINGS", node_key):
                raw_cpm_data = node["data"]["playerLevel"]["cpMultiplier"]
                CPMultiplier.parse_from_data(raw_cpm_data)

        # Pokemon have a dependency on Types and Moves.
        # We need to process those first before we can instantiate the Pokemon data.
        parser_log.info("2nd Stage Types")
        for _type in self.raw_types:
            # Types are self contained within the Type class.
            # No need to store a instance ref in RaiderDex
            TypeMatchups.register_type_from_data(_type)
        parser_log.info("2nd Stage Moves")
        move_factory = MoveFactory()
        moves = move_factory.parse_all_moves(self.raw_pve_moves, self.raw_pvp_moves)
        self.moves = moves
        parser_log.info("2nd Stage Pokemon")
        pokemon_factory = PokemonFactory()
        pokemons = pokemon_factory.parse_all_pokemon(self.raw_pokemon, raw_smeargle_moves)
        self.pokemons = pokemons
