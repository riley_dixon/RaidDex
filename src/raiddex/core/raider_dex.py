import logging

from raiddex.core.parser.parser import Parser

logging.basicConfig()
logging.root.setLevel("DEBUG")
rd_logger = logging.getLogger("RaiderDexLog")

class RaiderDex:

    @classmethod
    def init_from_raw_data(cls):
        raider_dex = cls()
        data_parser = Parser()
        data_parser.parse_all_data()

        return raider_dex
    
    def __init__(self) -> None:
        pass

    def populate_pokemon_data(self):
        pass
    
if __name__ == "__main__":
    raider_dex = RaiderDex.init_from_raw_data()