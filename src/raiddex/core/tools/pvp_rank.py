"""
Tool to calculate PvP Ranks of a given pokemon, for a given league.
"""
from enum import IntEnum

from raiddex.core.models.pokemon.pokemon import Pokemon, LivePokemon
from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier, MAX_LEVEL

STAT_PRODUCT_FACTOR = 1000


def get_iv_hex(attack_iv: int, defense_iv: int, stamina_iv: int) -> int:
    """
    Encode the IVs into a single integer.

    This will produces a 3-byte int that contains the IVs.
    The produced encoding is: 0xADS
    Where:
        A - Attack IV
        D - Defense IV
        S - Stamina IV
    """
    for iv in [attack_iv, defense_iv, stamina_iv]:
        if iv not in range(0, 16):
            raise ValueError("IVs must be within ")

    x = (attack_iv << 8) | (defense_iv << 4) | (stamina_iv)
    return x


class PvPLeague(IntEnum):
    """
    Enum of different Leagues/Tiers for PvP Battles in Pokemon GO.

    Value of each `PvPLeague` is the maximum CP a `LivePokemon` can have
    in the league. `LivePokemon` that exceed this threshold are not permitted.

    A value of -1 indicates that there is no maximum CP allowed.
    """
    LITTLE = 500
    GREAT = 1500
    ULTRA = 2500
    MASTER = -1

class PvPIVStat:
    def __init__(
        self, pokemon: Pokemon, live_pokemon: LivePokemon
    ):
        """
        Score is the output of whatever formula is used to compare IV combinations.

        For example, Stat Product is currently preferred as the score.
        """
        self.pokemon = pokemon
        self.attack_iv = live_pokemon.attack_iv
        self.defense_iv = live_pokemon.defense_iv
        self.stamina_iv = live_pokemon.stamina_iv
        self.optimal_level = live_pokemon.level
        self.optimal_cp = live_pokemon.cp
        self.score = live_pokemon.stat_product
        self.score_percent = None
        self.rank = None
    
    def __str__(self) -> str:
        """
        Return a string representation of the PvPIVStats.

        This includes the `Pokemon` name of the stats, the IVs, the scoring function
        used, and the score.
        """
        pvp_str = (
            f"{self.pokemon.name} - ({self.attack_iv}/{self.defense_iv}/{self.stamina_iv}) - StatProduct: {self.score}"
        )
        return pvp_str

    # Primary Comparator: Score
    # Secondary Comparator: CP
    def __lt__(self, other) -> bool:
        if self.score == other.score:
            return self.optimal_cp < other.optimal_cp
        return self.score < other.score

    def __le__(self, other) -> bool:
        if self.score == other.score:
            return self.optimal_cp <= other.optimal_cp
        return self.score <= other.score

    def __eq__(self, other) -> bool:
        return self.score == other.score and self.optimal_cp == other.optimal_cp

    def __ne__(self, other) -> bool:
        return self.score != other.score or self.optimal_cp != other.optimal_cp

    def __gt__(self, other) -> bool:
        if self.score == other.score:
            return self.optimal_cp > other.optimal_cp
        return self.score > other.score

    def __ge__(self, other) -> bool:
        if self.score == other.score:
            return self.optimal_cp >= other.optimal_cp
        return self.score >= other.score

    def iv_to_str(self) -> str:
        return f"({self.attack_iv}/{self.defense_iv}/{self.stamina_iv})"
    
    def get_live_pokemon(self) -> LivePokemon:
        """
        Create a LivePokemon out of these PvPIVStat's.

        This is to allow for smart memory management where each stat stores
        a single reference to the Pokemon base information. If we instead
        stored the LivePokemon object directly, we would instead be duplicating
        the Pokemon data across all 4096 permutations.

        The primitive information needed to construct a LivePokemon is stored
        within this object, allowing us to recreate a LivePokemon on demand.
        """
        live_pokemon = self.pokemon.get_live_pokemon(
            attack_iv=self.attack_iv,
            defense_iv=self.defense_iv,
            stamina_iv=self.stamina_iv,
            level=self.optimal_level
        )
        return live_pokemon

class PvPIVStats:
    """
    A wrapper of collections for groupings of PvPIVStats.

    Implements two collection attributes:
        - A list for being able to sort by rank.
        - A dict for quick read access based on a set of IVs.
    """
    
    def __init__(self):
        self.ranks: list[PvPIVStat] = []
        self.mapped_ranks: dict[int, PvPIVStat] = {}
    
    def __len__(self) -> int:
        return len(self.ranks)

    def sort_ranks(self) -> None:
        """
        Sort the ranks from highest score to lowest score.
        """
        self.ranks.sort(reverse=True)

    def append(self, iv_stat: PvPIVStat) -> None:
        """
        Add another `PvPIVStat` to the calculated ranks.

        Additionally add to the mapped_ranks.
        """
        self.ranks.append(iv_stat)
        iv_hex = get_iv_hex(iv_stat.attack_iv, iv_stat.defense_iv, iv_stat.stamina_iv)
        self.mapped_ranks[iv_hex] = iv_stat

    def get_top_rank(self) -> PvPIVStat:
        """
        Return the top scoring `PvPIVStat`.
        """
        return self.ranks[0]

    def get_ivs(self, attack_iv: int, defense_iv: int, stamina_iv: int) -> PvPIVStat:
        """
        Return a `PvPIVStat` that matches the provides IVs.
        """
        x = get_iv_hex(attack_iv, defense_iv, stamina_iv)
        return self.mapped_ranks[x]


class PvPRankTool:

    def __init__(self) -> None:
        pass

    def _calculate_max_level(
        self, live_pokemon: LivePokemon, cp_ceiling: int, max_level: float
    ) -> float:
        """
        Calculate the optimal level for the pokemon iv_set and cp_ceiling.
        
        More specifically, find the level for the (attack_iv, defense_iv, stamina_iv) set
        that keeps the Pokemon CP under the cp_ceiling.
        """
        if cp_ceiling == -1:
            return max_level
        # CP calculation but solve for theoretical max CPM.
        cp_stat_combination = (
            (live_pokemon.attack) * (live_pokemon.defense)**0.5
            * (live_pokemon.stamina)**0.5
        )
        theoretical_cpm = (10*(cp_ceiling+0.9999)/cp_stat_combination)**0.5
        calculated_max_level = CPMultiplier.get_closest_level(theoretical_cpm)

        return calculated_max_level
    
    def calculate_ranks(self, pokemon: Pokemon, cp_ceiling: int, max_level: float = MAX_LEVEL) -> PvPIVStats:
        """
        Calculate the PvP rank for a given `Pokemon` in a given PvP League.

        If `cp_ceiling` == -1, there is no max. See `PvPLeague` for examples of cp_ceiling.

        `max_level` dictates what the highest level a `Pokemon` can be for this calculation.
        This was quite relevant when Level 50 was initially introduced and some leagues had
        Level 40 (+1 Best Buddy) caps.

        TODO: Employ multiprocessing to speed up calculation of values.
        TODO: Calculate using a different formula (e.g. StatProd, TDO, DPS).
        """
        calculated_ranks = PvPIVStats()
        live_pokemon = pokemon.get_live_pokemon()

        for attack_iv in range(0, 16):
            for defense_iv in range(0, 16):
                for stamina_iv in range(0, 16):
                    live_pokemon.attack_iv = attack_iv
                    live_pokemon.defense_iv = defense_iv
                    live_pokemon.stamina_iv = stamina_iv

                    optimal_level = self._calculate_max_level(
                        live_pokemon, cp_ceiling, max_level
                    )
                    live_pokemon.level = optimal_level

                    pvp_set = PvPIVStat(
                        pokemon, live_pokemon
                    )
                    calculated_ranks.append(pvp_set)

        calculated_ranks.sort_ranks()
        max_score = calculated_ranks.ranks[0].score
        for i in range(0, len(calculated_ranks)):
            iv_stat = calculated_ranks.ranks[i]
            iv_stat.rank = i + 1
            iv_stat.score_percent = (iv_stat.score / max_score) * 100

        return calculated_ranks
