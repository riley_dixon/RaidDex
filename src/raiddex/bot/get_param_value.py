from discord.interactions import Interaction


def get_param_value(interaction: Interaction, name: str, default=None) -> int | str | None:
    """
    Get the app_command argument `name` from a given interaction.

    If the argument `name` is not found, `default` is returned instead.

    Rationale for this:
        discord.py does not have a nice mechanism to read other parameter's
        currently written out by the user. However, it does store the raw data
        received by Discord. As such, we can parse through the raw data to inspect
        this value. This can be helpful if we want to further filter autocomplete
        values by another data field.
    """
    options = interaction.data["options"]
    for option in options:
        if option["name"] == name:
            return option["value"]
    
    return default