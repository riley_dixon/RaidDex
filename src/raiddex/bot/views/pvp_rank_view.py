from typing import Optional
from discord.ui import View

from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.tools.pvp_rank import PvPIVStat, PvPLeague

class PvPRankView(View):
    """
    Provide buttons/modal's to further interact with a PvPRank command.
    """
    def __init__(
        self,
        pokemon: Pokemon,
        league: PvPLeague,
        user_stats: PvPIVStat,
        top_stats: PvPIVStat,
        *,
        timeout: float | None = 180
    ):
        super().__init__(timeout=timeout)
