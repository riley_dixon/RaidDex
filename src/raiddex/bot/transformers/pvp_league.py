from typing import Any, Coroutine, List
from discord import Client
from discord.app_commands import Transformer
from discord.app_commands.models import Choice
from discord.interactions import Interaction

from raiddex.bot.transformers.exceptions import UserError
from raiddex.core.tools.pvp_rank import PvPLeague

class PvPLeagueTransformer(Transformer):
    
    async def transform(
        self,
        interaction: Interaction[Client],
        value: str
    ) -> Coroutine[Any, Any, Any]:
        """
        On command execution, find a matching `PvPLeague`.

        Raises a `UserError` if no matching PvPLeague is found.
        """
        try:
            league = PvPLeague[value.upper()]
        except KeyError as e:
            await interaction.response.send_message(
                f"'{value}' is not a valid PvPLeague",
                ephemeral=True,
                delete_after=10.0
            )
            raise UserError(value, self, interaction)

        return league
    
    async def autocomplete(
        self,
        interaction: Interaction[Client],
        current_value: str
    ) -> List[Choice[str]]:
        """
        Provide a list of Choices for PvPLeague.
        If blank, return all leagues. Else, match what the user currently has typed.
        """
        if current_value == "":
            suggestions = [
                Choice(name=league.name.capitalize(), value=league.name) for league in PvPLeague
            ]
        else:
            suggestions = [
                Choice(name=league.name.capitalize(), value=league.name) for league in PvPLeague
                if league.name.startswith(current_value.upper())
            ]

        return suggestions
