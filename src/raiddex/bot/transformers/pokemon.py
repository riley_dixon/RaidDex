from typing import Any, Coroutine, List
from discord import Client
from discord.app_commands import Transformer
from discord.app_commands.models import Choice
from discord.interactions import Interaction
from raiddex.bot.transformers.exceptions import UserError
from raiddex.bot.transformers import MAX_CHOICES
from raiddex.bot.get_param_value import get_param_value
from raiddex.db.raiddex_db import RaidDexDB

class PokemonTransformer(Transformer):
    """
    Transformer to select valid Pokemon species.

    Primarily handles Pokemon Names, but can also select specific forms.

    Note: This is typically used in conjunction with a `PokemonFormTransformer`.
        Without it, only the base Pokemon form can be retrieved.
    """
    
    async def autocomplete(
        self,
        interaction: Interaction[Client],
        current_value: str
    ) -> Coroutine[Any, Any, List[Choice[str]]]:
        """
        Try to help the user out by suggesting a Pokemon based on what they have currently typed.

        Requires 3 characters to start suggestions.
        """
        raiddex_db = RaidDexDB()
        pokemons_table = raiddex_db.get_pokemon_table()

        if len(current_value) < 3:
            # Don't start autocomplete until we have at least 3 characters.
            return []

        names = pokemons_table.search_pokemon_name(current_value, num_results=MAX_CHOICES)

        suggestions = [
            Choice(
                name=name.alias if name.alias is not None else name.name.capitalize(),
                value=name.name) for name in names
        ]

        return suggestions

    async def transform(
        self,
        interaction: Interaction[Client],
        pokemon_name: str
    ) -> Coroutine[Any, Any, Any]:
        """
        On command execution, find and return a matching `Pokemon`.

        Most commands involving a Pokemon should also include a `form` field.

        Raises a `UserError` if no Pokemon can be found.
        """
        raiddex_db = RaidDexDB()
        pokemons_table = raiddex_db.get_pokemon_table()
        form_name = get_param_value(interaction, "form", "normal").upper()
        pokemon_obj = pokemons_table.get_pokemon(
            pokemon_name=pokemon_name.upper(), pokemon_form=form_name
        )

        if pokemon_obj is None:
            if form_name == "NORMAL":
                requested = f"'{pokemon_name.capitalize()}'"
            else:
                requested = f"'{pokemon_name.capitalize()}' (Form: '{form_name.capitalize()}')"

            await interaction.response.send_message(
                f"Pokemon {requested} does not exist.",
                ephemeral=True,
                delete_after=10.0
            )
            raise UserError(
                f"{pokemon_name} ({form_name})", self, interaction
            )

        return pokemon_obj
