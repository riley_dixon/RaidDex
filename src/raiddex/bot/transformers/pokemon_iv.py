from typing import Any, Coroutine
from discord import Client
from discord.app_commands import Transformer
from discord.enums import AppCommandOptionType
from discord.interactions import Interaction


class PokemonIVTransformer(Transformer):
    """
    Transform IV data into an int. Use Discord to enforce valid IV ranges.
    """
    
    @property
    def type(self) -> AppCommandOptionType:
        return AppCommandOptionType.integer
    
    @property
    def max_value(self) -> int:
        return 15
    
    @property
    def min_value(self) -> int:
        return 0

    def transform(self, interaction: Interaction[Client], value: int) -> Coroutine[Any, Any, Any]:
        return value

