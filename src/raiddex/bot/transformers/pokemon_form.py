import string
from typing import Any, Coroutine, List
from discord import Client
from discord.app_commands import Transformer
from discord.app_commands.models import Choice
from discord.interactions import Interaction

from raiddex.bot.get_param_value import get_param_value
from raiddex.db.raiddex_db import RaidDexDB

class PokemonFormTransformer(Transformer):
    """
    Transformer to select valid Pokemon Forms.

    Note: This is typically used in conjunction with a `PokemonTransformer`.
    """

    def transform(
        self,
        interaction: Interaction[Client],
        value: str
    ) -> Coroutine[Any, Any, Any]:
        return value.upper()
    
    async def autocomplete(
        self,
        interaction: Interaction[Client],
        current_value: str
    ) -> Coroutine[Any, Any, List[Choice[str]]]:
        """
        Provide a list of suggestions based on what the current value of the `pokemon` field.

        Similar to the PokemonTransformer, `pokemon` needs to have a minimum of 3 characters
        typed prior to autocomplete starting.
        """
        raiddex_db = RaidDexDB()
        pokemons_table = raiddex_db.get_pokemon_table()
        pokemon_name = get_param_value(interaction, "pokemon", "")

        if len(pokemon_name) < 3:
            # To save us from hammering the DB, don't search for ALL forms in existence.
            return [
                Choice(name="<Give_Pokemon_Name_First>", value="")
            ]

        forms = pokemons_table.search_pokemon_form(
            current_value.upper(), pokemon_name=pokemon_name, num_results=25
        )
        suggestions = [
            Choice(
                name=form.alias if form.alias is not None else string.capwords(form.form.replace("_", " ")),
                value=form.form
            )
            for form in forms
        ]

        return suggestions
