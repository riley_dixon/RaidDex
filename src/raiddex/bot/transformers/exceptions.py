from discord.interactions import Interaction
from discord.app_commands import Transformer


class UserError(Exception):
    """
    Exception to be raised on invalid user input.

    WARNING: The logging mechanism of `discord.Client` for RaidDex will not emit a
        log message for exceptions of this type (unless the log level is set to DEBUG).
        
        This is by design as in most cases this is not our problem and the user probably
        typed in an invalid value.
    """
    def __init__(self, value: str, transformer: Transformer, interaction: Interaction) -> None:
        self.msg = (
            f"Failed to convert '{value}' to {transformer._error_display_name} during execution of "
            f"command {interaction.command.name}."
        )
        super().__init__(self.msg)