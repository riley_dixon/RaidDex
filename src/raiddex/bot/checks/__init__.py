# Module that provides helper methods to check user's roles.
# These roles can then be used to hide/enable features depending
# on permissions. This can be used for fine tuning Commands further
# by enabling or disabling certain Command features rather than
# the entire command.