from importlib.resources import files
import json
import logging

from discord import Client, Intents, Interaction
from discord.app_commands import Command, CommandTree

from raiddex.bot.commands import register_all_commands
from raiddex.bot.transformers.exceptions import UserError
from raiddex.db.telemetry.telemetry_db import TelemetryDB
from raiddex.utils.op_env import get_operating_environment, OperatingEnvironment as OpEnv


logger = logging.getLogger(__name__)


class RaidDexDiscord(Client):
    """
    The main class for operating the Discord service.

    Currently, this class orchestrates starting up and initializing
    the RaidDex service, including populating & updating the database.

    Ideally, we could decouple this and make the database independent
    from the bot, however for putting together an initial service
    and automating most of the work, this has been nice to have.
    """

    def __init__(self) -> None:
        intents = Intents.default()
        intents.message_content = True
        intents.members = True
        super().__init__(
            intents=intents
        )

        self.tree = CommandTree(self)

        self._discord_secret_key = self.get_discord_secret_key()

        # Until we setup comprehensive logging let's utilize the default logger
        # but allow for extending it.
        self._original_on_error = self.tree.on_error
        self.tree.error(self.on_app_command_error)

        # Defer initialization until after DB is known to be online.
        self.telemetry_db: TelemetryDB = None
    
    def get_discord_secret_key(self) -> str:
        """
        Determine which Discord Key to use in the bot.

        If running mode is PRODUCTION, use the prod bot key.
        Otherwise, use the test bot key.
        """
        secrets_path = files("raiddex").joinpath(".secrets.json")
        with secrets_path.open("r") as secrets:
            self._secrets = json.load(secrets)["bot"]

        op_env = get_operating_environment()
        if op_env == OpEnv.PRODUCTION:
            secret_key = self._secrets["DISCORD_SECRET_KEY"]
        else:
            secret_key = self._secrets["DISCORD_TEST_SECRET_KEY"]

        return secret_key

    async def setup_hook(self) -> None:
        await register_all_commands(self)

    async def on_app_command_completion(self, interaction: Interaction, command: Command) -> None:
        """
        Called when an AppCommand completes without an uncaught exception.
        """
        logger.info(f"Interaction {interaction.id} for Command {command.name} completed successfully.")
        await self.telemetry_db.app_command_complete(interaction)

    async def on_app_command_error(self, interaction, error):
        """
        Called when an AppCommand raises an exception.
        """
        if isinstance(error, UserError) or isinstance(error.__cause__, UserError):
            logger.debug("Ignoring user input error", exc_info=error)
            await self.telemetry_db.app_command_user_error(interaction)
            return
        else:
            await self._original_on_error(interaction, error)
            await self.telemetry_db.app_command_error(interaction)

    def run(self):
        """
        Entry point to start bot operations.
        """
        #Hack for now to ensure DBPool is initialized prior to use.
        self.telemetry_db = TelemetryDB()

        super().run(
            self._discord_secret_key,
            log_handler=None, # We will manage the log Handlers for the Discord library.
            root_logger=False
        )
