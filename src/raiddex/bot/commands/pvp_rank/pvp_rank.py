# Returns the PVP Rank of a given Pokemon based on given IVs
import logging

from discord import Interaction
from discord.app_commands import command, Transform

from raiddex.bot.embeds.pvp_rank_embed import PvPRankEmbed
from raiddex.bot.transformers.pokemon import PokemonTransformer
from raiddex.bot.transformers.pokemon_form import PokemonFormTransformer
from raiddex.bot.transformers.pokemon_iv import PokemonIVTransformer
from raiddex.bot.transformers.pvp_league import PvPLeagueTransformer

from raiddex.core.tools.pvp_rank import PvPRankTool, PvPLeague, Pokemon


logger = logging.getLogger(__name__)


@command()
async def pvprank(
    interaction: Interaction,
    league: Transform[PvPLeague, PvPLeagueTransformer],
    pokemon: Transform[Pokemon, PokemonTransformer],
    attack_iv: Transform[int, PokemonIVTransformer],
    defense_iv: Transform[int, PokemonIVTransformer],
    stamina_iv: Transform[int, PokemonIVTransformer],
    form: Transform[str, PokemonFormTransformer] = ""
):
    """
    Calculate the rank of a specific `Pokemon` species for a PvP League.
    
    Enhancements:
    league - Premium: "ALL" which will print a table
    level_cap
    best_friend option
    """
    # OPTIONAL: Provide current CP, calculate level and determine level up cost etc.
    pvp_tool = PvPRankTool()
    
    try:
        ranks = pvp_tool.calculate_ranks(pokemon, cp_ceiling=league.value)
    except Exception:
        # TODO: Send a Generic Error embed instead. Looks nicer.
        await interaction.response.send_message(f"An unknown error occurred.")
        raise
    top_stats = ranks.get_top_rank()
    user_stats = ranks.get_ivs(attack_iv, defense_iv, stamina_iv)

    pvp_rank_embed = PvPRankEmbed(interaction, league, user_stats, top_stats)

    response = interaction.response
    await response.send_message(content=None, embed=pvp_rank_embed)
