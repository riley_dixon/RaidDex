# Simple ping module to test basic bot responsiveness.
import logging

from discord import Interaction
from discord.app_commands import command

logger = logging.getLogger(__name__)


@command()
async def ping(interaction: Interaction) -> None:
    """
    Test RaidDex's availability.
    """
    # Example of deferring action:
    # 1) Sent interaction Deferral - await interaction.response.defer()
    # 2) Do processing
    # 3) Using followup webhook, send a new message - responded_msg = await interaction.followup.send("msg")
    # 4) Return value from follow up is a Message that can be further edited.

    ping_sent = interaction.created_at
    await interaction.response.send_message("Pong!")

    responded_msg = await interaction.original_response()
    pong_sent = responded_msg.created_at
    delta_ms = round((pong_sent - ping_sent).total_seconds(), 3)
    pong_msg = f"Pong! `{delta_ms}`ms."
    await responded_msg.edit(content=pong_msg)
    logger.debug(pong_msg)
