import logging
import os
from typing import List

from discord import Client
from discord.app_commands import Command, CommandTree

from raiddex.bot.commands.ping.ping import ping
from raiddex.bot.commands.pvp_rank.pvp_rank import pvprank
from raiddex.utils.op_env import OperatingEnvironment as OpEnv, get_operating_environment

logger = logging.getLogger("raiddex.discord.commands")


ALL_CMDS: List[Command] = [
    ping, pvprank
]

async def register_all_commands(discord_client: Client):
    cmd_tree: CommandTree = discord_client.tree
    for cmd in ALL_CMDS:
        if isinstance(cmd, Command):
            cmd_tree.add_command(cmd)
        else:
            raise TypeError(
                f"Unknown Command: {cmd} or type {type(cmd)}."
            )
    
    op_env = get_operating_environment()
    if op_env == OpEnv.PRODUCTION:
        list = await discord_client.tree.sync()
    else:
        # ID is RaidDex Home Operations Server
        test_server = discord_client.get_guild(1177789936276090880)
        list = await cmd_tree.sync(guild=test_server)

    logger.info(
        f"Registered Commands: {[c.name for c in ALL_CMDS]}"
    )