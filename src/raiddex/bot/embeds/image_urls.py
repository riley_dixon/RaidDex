from raiddex.core.models.pokemon.pokemon import Pokemon


class ImageURLs:
    """
    Helper class to build URLs pointing towards different image files.

    Regardless of whether we upload images directly to Discord, or
    "host" them on an external service, we need mechanisms to fetch
    a URL pointing to the image we want.

    This class will act as an interface to return a URL to the image we want
    to include/retrieve.
    """

    # Keeping these in source in case we need to revert back to abusing GitHub's CDN.
    # POKEMINER_BASE_URL = "https://raw.githubusercontent.com/PokeMiners/pogo_assets/master/Images"
    # POKEMINER_POKEMON_IMAGES_BASE = POKEMINER_BASE_URL + "/Pokemon/Addressable%20Assets"

    BASE_IMAGE_CONTAINER_URL = "https://raiddex.blob.core.windows.net/images"
    POKEMON_IMAGES_URL = BASE_IMAGE_CONTAINER_URL + "/pokemon"

    @classmethod
    def get_pokemon(cls, pokemon: Pokemon) -> str:
        """
        Build a URL that points to an image hosted in RaidDex's Azure blob bucket.

        The filenames are the same as PokeMiner's filename schema.

        PokeMiner filename schema:
        pm<Pokemon #>.[form_data][is_shiny].icon.png
        Where:
            form_data - f<FORM_NAME>. or c<COSTUME_NAME>.
            is_shiny - s.        
        """
        url = cls.POKEMON_IMAGES_URL + f"/pm{pokemon.id}."
        if pokemon.form != "NORMAL":
            # TODO: What if costume instead?
            url += f"f{pokemon.form}."
        # TODO: What if we want shiny?
        url += "icon.png"

        return url
