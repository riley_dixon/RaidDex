import string

from datetime import datetime, timezone
from enum import Enum
from typing import Any, Optional, Union
from discord import Embed, Interaction
from discord.colour import Colour
from discord.types.embed import EmbedType

from raiddex.bot.embeds.image_urls import ImageURLs
from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.tools.pvp_rank import PvPIVStat, PvPLeague


class PvPLeagueColour(Enum):
    LITTLE = Colour.from_rgb(r=246, g=136, b=123)
    GREAT = Colour.from_rgb(r=50, g=81, b=203)
    ULTRA = Colour.from_rgb(r=236, g=213, b=51)
    MASTER = Colour.from_rgb(r=129, g=41, b=158)


class PvPRankEmbed(Embed):

    def __init__(
        self,
        interaction: Interaction,
        league: PvPLeague,
        user_stats: PvPIVStat,
        top_stats: PvPIVStat,
    ):
        """
        Create a Discord `Embed` to nicely present the calculated data to the user.

        TODO: Let `description` serve as a warning label in the event experimental
            data is used. (e.g. Pokemon not released yet)
        """
        pokemon = user_stats.pokemon
        colour = PvPLeagueColour[league.name].value
        # Add League Name into title for colour blindness accessibility.
        pokemon_name = pokemon.name_alias if pokemon.name_alias is not None else pokemon.name.capitalize()
        if pokemon.form == "NORMAL":
            pokemon_form = ""
        elif pokemon.form_alias is not None:
            pokemon_form = f"{pokemon.form_alias} "
        else:
            pokemon_form = f"{string.capwords(pokemon.form.replace('_', ' '))} "
            
        title = f" {league.name.capitalize()} PvP Rank - {pokemon_form}{pokemon_name}"
        top_output = self.info_to_str(top_stats)
        user_output = self.info_to_str(user_stats)
        
        super().__init__(
            colour=colour,
            title=title,
            description=None,
            timestamp=datetime.now(timezone.utc)
        )

        self.set_thumbnail(
            url=ImageURLs.get_pokemon(pokemon)
        )
        self.add_field(
            name="Top IVs", value=top_output, inline=True
        )
        self.add_field(
            name="Your IVs", value=user_output, inline=True
        )
        self.set_footer(text=interaction.id)
    
    def info_to_str(self, stats: PvPIVStat) -> str:
        """
        Try to structure the data nicely. Note that with inline=True,
        this will be shown as a narrow column for each set of IVs.
        """
        live_pokemon = stats.get_live_pokemon()
        output = (
            f"**Rank:** {stats.rank} - {stats.iv_to_str()}\n"
            f"**CP:** {stats.optimal_cp} (Level: {stats.optimal_level})\n"
            f"**Attack:** {round(live_pokemon.scaled_attack, 2)}\n"
            f"**Defense:** {round(live_pokemon.scaled_defense, 2)}\n"
            f"**Stamina:** {int(live_pokemon.scaled_stamina)}\n"
            f"**Score:** {round(stats.score, 2)}\n"
            f"**Score %:** {round(stats.score_percent, 2)}\n"
        )
        return output