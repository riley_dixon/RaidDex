from discord import Interaction
from discord.app_commands import Choice

from raiddex.db.raiddex_db import RaidDexDB
from raiddex.bot.autocomplete import MAX_CHOICES

# If we keep this separate from the Bot Commands, we should be able to test this
# albeit with a dependency on the DB for optimal results.

async def pokemon_name(interaction: Interaction, current_value: str) -> list[Choice[str]]:
    raiddex_db = RaidDexDB()
    pokemons_table = raiddex_db.get_pokemon_table()

    if len(current_value) < 3:
        # Don't start autocomplete until we have at least 3 characters.
        return []

    names = pokemons_table.search_pokemon_name(current_value, num_results=MAX_CHOICES)

    suggestions = [
        Choice(name=name.capitalize(), value=name) for name in names
    ]

    return suggestions