from discord import Interaction
from discord.app_commands import Choice

from raiddex.core.tools.pvp_rank import PvPLeague

async def pvp_league_name(
    interaction: Interaction,
    current_value: str
) -> list[Choice[str]]:
    if current_value == "":
        suggestions = [
            Choice(name=league.name.capitalize(), value=league.name) for league in PvPLeague
        ]
    else:
        suggestions = [
            Choice(name=league.name.capitalize(), value=league.name) for league in PvPLeague
            if league.name.startswith(current_value.upper())
        ]

    return suggestions
