import logging

from typing import Any, Optional, Union

from discord import Interaction
from psycopg import sql
from psycopg_pool import ConnectionPool

from raiddex.bot.commands import ALL_CMDS
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.connection_config.connection_config import get_conn_config
from raiddex.db.db_pool import DBPool
from raiddex.db.telemetry.tables.ping import PingTelemetry
from raiddex.db.telemetry.tables.pvp_rank import PvpRankTelemetry
from raiddex.db.telemetry.telemetry_table_factory import TelemetryTableFactory

logger = logging.getLogger(__name__)


class TelemetryDB:
    
    managed_tables = {
        telem_cls.name: telem_cls for telem_cls in [  
            TelemetryTableFactory().create_table_cls(cmd)
            for cmd in ALL_CMDS
        ]
    }

    def __init__(self, db_pool: Optional[ConnectionPool] = None):
        if db_pool is None:
            db_pool = DBPool.telemetry_pool
        self.telemetry_pool = db_pool

        conn_config = get_conn_config()
        self.telemetry_db = conn_config.get_telemetry_db()
        self.telemetry_user_name = conn_config.get_telemetry_user_name()

    def list_tables(self) -> list[str]:
        """
        Get the list of tables present in the `telemetry` database.

        This list excludes any system tables that may be present or viewable to the
        `telemetry` role.
        """
        query = sql.SQL("""
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema <> 'pg_catalog' AND
                table_schema <> 'information_schema'
        """)

        with self.telemetry_pool.connection() as conn:
            result = conn.execute(query)
            tables = [table[0] for table in result.fetchall()]
        
        return tables

    def _initialize_table(self, table: AbstractTable) -> None:
        with DBPool.get_su_conn(self.telemetry_db) as su_conn:
            create_table_cmd = table.create_table_sql()
            su_conn.execute(create_table_cmd)
            logger.warning(f"Table '{table.name}' created in Database '{self.telemetry_db}'.")
            su_conn.execute(
                sql.SQL(
                    "GRANT SELECT, INSERT, UPDATE ON {table_name} TO {role_name}"
                ).format(
                    table_name=sql.Identifier(table.name),
                    role_name=sql.Identifier(self.telemetry_user_name)
                )
            )
            logger.warning(f"Role '{self.telemetry_user_name}' granted ADDITION access to '{table.name}'.")

    def initialize_tables(self) -> None:
        """
        Create Telemetry data tables and grant appropriate permissions to the telemetry user.

        Superuser needs to connect to the telemetry db for this to work correctly.

        TODO: Consider renaming to initialize_database
        """
        existing_tables = self.list_tables()
        tables_to_create = [
            table for name, table in self.managed_tables.items()
            if name not in existing_tables
        ]

        logger.info(f"Existing tables located: {existing_tables}")
        for table_cls in tables_to_create:
            table = table_cls(self.telemetry_pool)
            self._initialize_table(table)
            table.populate_table(None)

    def get_interaction_args(self, interaction: Interaction) -> dict[str, Union[int, str]]:
        """
        Get a flat dictionary of the arguments provided by the user.

        Returns an empty dictionary if there are no arguments provided.
        """
        args = {}
        for param in interaction.data.get("options", {}):
            param_name = param["name"]
            param_value = param["value"]
            args[param_name] = param_value
        
        return args

    def insert_app_command_invocation(
        self,
        interaction: Interaction,
        status: str
    ) -> None:
        """
        Insert a record of an AppCommand being invoked.

        Note: Performance data should not be collected here due to the asynchronous aspect
            of this library.
        """
        command_name = interaction.command.name
        command_params = self.get_interaction_args(interaction)

        data = {
            "interaction_id": interaction.id,
            "guild_id": interaction.guild_id,
            "channel_id": interaction.channel_id,
            "user_id": interaction.user.id,
            "status": status
        }
        data.update(command_params)

        insert_cmd = sql.SQL(
            """
            INSERT INTO {table_name} ({column_names})
            VALUES ({column_placeholders})
            """
        ).format(
            table_name=sql.Identifier(command_name),
            column_names=sql.SQL(", ").join(map(sql.Identifier, data.keys())),
            column_placeholders=sql.SQL(", ").join(map(sql.Placeholder, data.keys()))
        )
        with self.telemetry_pool.connection() as conn:
            conn.execute(insert_cmd, data)
            logger.debug(f"Logged AppCommand '{command_name}' invocation for Interaction '{interaction.id}'.")

    async def app_command_complete(self, interaction: Interaction) -> None:
        """
        Mark the AppCommand as being successfully completed.
        """
        self.insert_app_command_invocation(interaction, "COMPLETED")

    async def app_command_error(self, interaction: Interaction) -> None:
        """
        Mark the AppCommand as failing to complete.
        """
        self.insert_app_command_invocation(interaction, "FAILED")
    
    async def app_command_user_error(self, interaction: Interaction) -> None:
        """
        Mark the AppCommand as failing to complete do to User Input error.
        """
        self.insert_app_command_invocation(interaction, "USER_ERROR")
