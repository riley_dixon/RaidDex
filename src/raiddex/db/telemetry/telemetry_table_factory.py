

from typing import List, Type

from discord.app_commands import Command, Parameter
from discord import AppCommandOptionType
from psycopg_pool import ConnectionPool

from raiddex.db.base_column import Column
from raiddex.db.telemetry.telemetry_table import TelemetryTable


class TelemetryTableFactory:
    """
    Automate the creation of telemetry tables based on the AppCommand signature.

    This does not prevent other telemetry tables from being specified or created.

    The trade off for this automation however is we need to approximate data types
    and it can lead to some inefficiencies. Namely, we will only support storing
    numbers and variable length text. Since this is telemetry data rather than hot
    compute data, performance is not as much of an issue here.

    A future implementation could be providing a hook in the AppCommand to override
    a particular data type. This is not needed currently however.
    """

    _discord_type_to_sql = {
        AppCommandOptionType.string: "varchar(50)",
        AppCommandOptionType.integer: "bigint",
        AppCommandOptionType.number: "double precision"
    }
    
    def _construct_column_list(self, cmd_parameters: List[Parameter]) -> List[Column]:
        sql_columns = []
        for param in cmd_parameters:
            try:
                param_type = self._discord_type_to_sql[param.type]
            except KeyError:
                raise Exception(f"Unrecognized param type `{param.type}` for `{param.name}`.")

            column = Column(param.name, param_type)
            sql_columns.append(column)
        
        return sql_columns

    def _get_table_cls(self, cmd: Command) -> Type[TelemetryTable]:
        """
        Return a TelemetryTable class that defines how to construct the SQL table.

        If there is a custom implementation for a certain TelemetryTable, return that
        subclass instead. Otherwise use the default TelemetryTable class.
        """
        return TelemetryTable
    
    def _create_generic_telemetry_cls(self, cmd: Command, base_cls = TelemetryTable) -> type[TelemetryTable]:
        # distinguishing between our name and builtin __name__
        new_cls__name__ = cmd.name + base_cls.__name__
        cls_columns = base_cls.columns + self._construct_column_list(cmd.parameters)

        new_cls_attrs = {
            "name": cmd.name,
            "columns": cls_columns
        }
        new_cls: type[TelemetryTable] = type(new_cls__name__, (base_cls,), new_cls_attrs)

        return new_cls

    def create_table_cls(self, discord_cmd: Command) -> type[TelemetryTable]:
        """
        Create a dynamic TelemetryTable class for a certain Discord Command.

        Note: This only creates an instance of the class.
        """
        table_cls = self._get_table_cls(discord_cmd)
        if table_cls is TelemetryTable:
            table_cls = self._create_generic_telemetry_cls(discord_cmd, TelemetryTable)

        return table_cls
