#from src.bot.commands import ALL_CMDS
from typing import List, Optional

#from discord.app_commands import AppCommand
from psycopg_pool import ConnectionPool
from raiddex.db.base_column import Column

from raiddex.db.abstract_table import AbstractTable

class TelemetryTable(AbstractTable):
    """
    Base class for tables in the Telemetry Database.

    This class should not be instantiated directly. Instead, it should be created
    through 1 of 2 different means:
    1) TelemetryTableFactory for a generic TelemetryTable.
    2) Subclassing and implementing the "name" attribute.

    Since much of our telemetry data is going to follow a similar schema,
    we can just create generic classes at run time to automate creating
    and managing Telemetry data, rather than needing to define a Telemetry
    class for each Discord AppCommand we will support.
    """
    
    columns = [
        Column("interaction_id", "bigint", "PRIMARY KEY"),
        Column("guild_id", "bigint"),
        Column("channel_id", "bigint"),
        Column("user_id", "bigint"),
        Column("status", "varchar(10)")
    ]

    def __init__(
        self,
        db_pool: Optional[ConnectionPool],
    ):
        super().__init__(db_pool)

    @property
    def table_attributes(self):
        return ""
