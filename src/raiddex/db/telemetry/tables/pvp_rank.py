import logging

from enum import IntEnum

from psycopg import sql

from raiddex.bot.commands.pvp_rank.pvp_rank import pvprank
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column


logger = logging.getLogger(__name__)


class CommandStatus(IntEnum):
    """
    Future Enum to be used for status.
    """
    running = 0
    success = 1
    failed = 2


class PvpRankTelemetry(AbstractTable):
    """
    PostgreSQL table to track usage of the PvPRank command.
    """
    
    name = pvprank.name
    columns = [
        Column("interaction_id", "bigint", "PRIMARY KEY"),
        Column("guild_id", "bigint"),
        Column("channel_id", "bigint"),
        Column("user_id", "bigint"),
        Column("status", "varchar(10)"),
        Column("league", "varchar(10)"),
        Column("pokemon", "varchar(20)"),
        Column("form", "varchar(25)"),
        Column("attack_iv", "smallint"),
        Column("defense_iv", "smallint"),
        Column("stamina_iv", "smallint")
    ]
    table_attributes = ""
