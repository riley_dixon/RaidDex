"""
We really do not need to track telemetry stats for the ping command.
However it can be used as a good validator for testing.
"""
import logging

from enum import IntEnum

from raiddex.bot.commands.ping.ping import ping
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column


logger = logging.getLogger(__name__)


class CommandStatus(IntEnum):
    """
    Future Enum to be used for status.
    """
    running = 0
    success = 1
    failed = 2


class PingTelemetry(AbstractTable):
    """
    PostgreSQL table to track usage of the Ping command.
    """
    
    name = ping.name
    columns = [
        Column("interaction_id", "bigint", "PRIMARY KEY"),
        Column("guild_id", "bigint"),
        Column("channel_id", "bigint"),
        Column("user_id", "bigint"),
        Column("status", "varchar(10)")
    ]
    table_attributes = ""
