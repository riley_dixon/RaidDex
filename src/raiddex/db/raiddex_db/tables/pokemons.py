import logging

from dataclasses import dataclass
from typing import Optional

from psycopg import sql

from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.parser.parser import Parser
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column
from raiddex.db.raiddex_db.tables.default_forms import DefaultFormsTable
from raiddex.db.row_factories.pokemon_row import PokemonRowFactory


logger = logging.getLogger(__name__)


@dataclass
class SearchedPokemonName:
    name: str
    alias: Optional[str]


@dataclass
class SearchedPokemonForm:
    form: str
    alias: Optional[str]


class PokemonsTable(AbstractTable):

    name = "pokemons"
    columns = [
        Column("name", "varchar(20)"),
        Column("name_alias", "varchar(20)"),
        Column("form", "varchar(25)"),
        Column("form_alias", "varchar(25)"),
        Column("id", "int2", "NOT NULL"),
        Column("type1", "varchar(10)", "NOT NULL"),
        Column("type2", "varchar(10)"),
        Column("base_attack", "int2", "NOT NULL"),
        Column("base_defense", "int2", "NOT NULL"),
        Column("base_stamina", "int2", "NOT NULL")
    ]
    table_attributes = "PRIMARY KEY (name, form)"

    def populate_table(self, data_parser: Parser) -> None:
        self.add_pokemons(data_parser.pokemons.values())

    def _add_missing_pokemon(
        self,
        parsed_pokemons: dict[tuple[str, str], Pokemon],
        db_pokemons: dict[tuple[str, str], Pokemon]
    ) -> list[Pokemon]:
        """
        Add parsed_pokemon entries that are not found in the Database.

        Returns a list of pokemon that were missing from the Database.

        On boot, parsed_pokemons is our source of truth.
        """
        missing_pokemons = [
            pokemon
            for pokemon in parsed_pokemons.values()
            if (pokemon.name, pokemon.form) not in db_pokemons
        ]
        if len(missing_pokemons) > 0:
            missing_names = [
                f"{pokemon.name} ({pokemon.form})" for pokemon in missing_pokemons
            ]
            self.add_pokemons(missing_pokemons)
            logger.info(f"Missing Pokemon being added to the DB: {missing_names}")
        else:
            logger.info(f"No missing Pokemon need to be added for table '{self.name}'.")

        return missing_pokemons

    def _update_existing_pokemon(
        self,
        parsed_pokemons: list[Pokemon],
        db_pokemons: list[Pokemon]
    ) -> list[Pokemon]:
        """
        Update pokemon in the Database that are different from the parsed entries.

        Returns a list of pokemon that were updated in the Database.

        On boot, parsed_pokemons is our source of truth.
        """
        update_pokemons = [
            pokemon
            for pokemon in parsed_pokemons.values()
            if pokemon != db_pokemons[(pokemon.name, pokemon.form)]
        ]
        if len(update_pokemons) > 0:
            update_names = [
                f"{pokemon.name} ({pokemon.form})" for pokemon in update_pokemons
            ]
            self.update_pokemons(update_pokemons)
            logger.info(f"Pokemon being updated in the DB: {update_names}")
        else:
            logger.info(f"No existing Pokemon need to be updated for table '{self.name}'.")

        return update_pokemons

    def _get_all_pokemon(self) -> list[Pokemon]:
        """
        Get all Pokemon stored in PostgreSQL.

        Not recommended for general use. Intended for startup activities.
        If needed, make a more specific function that can filter results.
        """
        query = sql.SQL("SELECT * FROM pokemons")

        with self.db_pool.connection() as conn:
            cursor = conn.cursor(row_factory=PokemonRowFactory)
            result = cursor.execute(query)
            pokemons = result.fetchall()
        
        return pokemons

    def _create_sql_key_value_pair(self, column_name):
        sql_pair = sql.SQL(
            "{column_name} = {column_placeholder}"
            ).format(
            column_name=sql.Identifier(column_name),
            column_placeholder=sql.Placeholder(column_name)
        )

        return sql_pair

    def update_pokemons(self, pokemon_to_update: list[Pokemon]) -> None:
        """
        Update individual Pokemon with new values.

        Updating the name or form is not permitted.

        This will update each of the specified Pokemon with new values.
        """
        updatable_columns = self.column_names.copy()
        updatable_columns.remove("name")
        updatable_columns.remove("form")

        update_cmd = sql.SQL(
            """
            UPDATE {table_name}
            SET {param_placeholders}
            WHERE name = {name} AND form = {form}
            """
            ).format(
            table_name=sql.Identifier(self.name),
            param_placeholders=sql.SQL(", ").join(map(self._create_sql_key_value_pair, updatable_columns)),
            name=sql.Placeholder("name"),
            form=sql.Placeholder("form")
        )

        with self.db_pool.connection() as conn:
            with conn.transaction():
                for pokemon in pokemon_to_update:
                    params = {
                        column_name: getattr(pokemon, column_name)
                        for column_name in self.column_names
                    }
                    conn.execute(update_cmd, params)
                    logger.debug(f"Updated Pokemon {pokemon.name} ({pokemon.form}).")

    def update_table(self, data_parser: Parser) -> None:
        """
        Update rows in the Database with newer data_parser information.
        """
        # Turn into dict's for faster access of members.
        parsed_pokemons = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in data_parser.pokemons.values()
        }
        db_pokemons = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in self._get_all_pokemon()
        }
        
        # First, add missing Pokemon
        missing_pokemons = self._add_missing_pokemon(
            parsed_pokemons,
            db_pokemons
        )
        
        # Second, exclude newly added missing pokemon from update
        for missing_pokemon in missing_pokemons:
            del parsed_pokemons[(missing_pokemon.name, missing_pokemon.form)]
        
        # Third, update any existing Pokemon with new attributes.
        updated_pokemons = self._update_existing_pokemon(
            parsed_pokemons,
            db_pokemons
        )

    def add_pokemon(self, pokemon: Pokemon) -> None:
        """
        Insert one Pokemon into PostgreSQL.

        Note: (name, form) are Primary Keys.
        """
        insert_cmd = sql.SQL(
            """
            INSERT INTO {table_name} ({column_names})
            VALUES ({param_placeholders})
            """).format(
                table_name=sql.Identifier(self.name),
                column_names=sql.SQL(", ").join(map(sql.Identifier, self.column_names)),
                param_placeholders=sql.SQL(", ").join(map(sql.Placeholder, self.column_names))
        )
        
        with self.db_pool.connection() as conn:
            params = {
                column_name: getattr(pokemon, column_name)
                for column_name in self.column_names
            }
            result = conn.execute(insert_cmd, params)
            logger.debug(f"Added Pokemon '{pokemon.name}' into PostgreSQL with params '{params}'.")

    def add_pokemons(self, pokemons: list[Pokemon]) -> None:
        """
        Insert many Pokemon into PostgreSQL.

        Note: This is the same logic as `add_pokemon`, just setup for multiple efficient inserts.

        Note: (name, form) are Primary Keys.
        """
        
        insert_cmd = sql.SQL(
            """
            INSERT INTO {table_name} ({column_names})
            VALUES ({param_placeholders})
            """).format(
                table_name=sql.Identifier(self.name),
                column_names=sql.SQL(", ").join(map(sql.Identifier, self.column_names)),
                param_placeholders=sql.SQL(", ").join(map(sql.Placeholder, self.column_names))
        )

        with self.db_pool.connection() as conn:
            with conn.transaction():
                for pokemon in pokemons:
                    params = {
                        column_name: getattr(pokemon, column_name)
                        for column_name in self.column_names
                    }
                    result = conn.execute(insert_cmd, params)
        logger.debug(f"Batch added {len(pokemons)} Pokemon into PostgreSQL.")
    
    def get_pokemon(self, pokemon_name: str, pokemon_form: str) -> Pokemon:
        """
        Retrieve a `Pokemon` object from Postgres.

        Name and Form can either be the "main" internal name, or an assigned alias.
        """

        if pokemon_form == "" or pokemon_form == "NORMAL":
            # TODO: Make the "NORMAL" string a sentinel default rather than just
            #   passing around string literals.
            default_form_table = DefaultFormsTable(self.db_pool)
            default_form = default_form_table.get_default_form(pokemon_name)
            if default_form is not None:
                logger.debug(f"get_pokemon: Using default_form '{default_form}' for '{pokemon_name}'.")
                pokemon_form = default_form

        query = sql.SQL("""
            SELECT *
            FROM pokemons
            WHERE
                (pokemons.name = {name} OR pokemons.name_alias = {name}) AND
                (pokemons.form = {form} OR pokemons.form_alias = {form})
            """).format(
            name=sql.Literal(pokemon_name), form=sql.Literal(pokemon_form)
        )

        with self.db_pool.connection() as conn:
            cursor = conn.cursor(row_factory=PokemonRowFactory)
            result = cursor.execute(query)
            pokemon = result.fetchone()

        return pokemon

    def search_pokemon_name(self, pokemon_name: str, num_results: int = 1) -> list[SearchedPokemonName]:
        """
        Search for distinct Pokemon starting with `pokemon_name`.

        Returns a list of Pokemon name's or aliases. If an alias is present for a Pokemon, that is
        returned over the pokemon name.

        `num_results` - How many results to return.

        Note: We can expand the accepted args on an as needed basis.

        Note: Basic search algorithm to allow for first iteration of autocomplete for Pokemon.
            An improvement may be auto-correction by pulling list of unique pokemon names and performing
            some kind of scoring based off of that.
        """
        pokemon_name = f"^{pokemon_name.upper()}"
        query = sql.SQL("""
            SELECT DISTINCT ON (pokemons.name) pokemons.name, pokemons.name_alias
            FROM pokemons
            WHERE
                pokemons.name ~* {name} OR
                pokemons.name_alias ~* {name}
            ORDER BY pokemons.name ASC
            """).format(
            name=sql.Literal(pokemon_name)
        )
        
        with self.db_pool.connection() as conn:
            cursor = conn.cursor()
            result = cursor.execute(query)
            pokemons = result.fetchmany(size=num_results)
            pokemons = [
                SearchedPokemonName(p[0], p[1])
                for p in pokemons
            ] # Extract names out of tuples
        
        found_names = [p.name for p in pokemons]
        logger.debug(
            f"search_pokemon_name: Search for '{pokemon_name}' yielded '{found_names}' as possible results."
        )

        return pokemons
    
    def search_pokemon_form(self, pokemon_form: str, pokemon_name: Optional[str] = None, num_results: int = 1) -> list[SearchedPokemonForm]:
        """
        Search for distinct forms starting with `pokemon_form`.

        Optionally, filter to forms present on Pokemon name or alias_name matching `pokemon_name`.
        
        Returns a list of Pokemon form's or aliases of forms. If an alias is present for a form, that is
        returned over the form name.

        `num_results` - How many results to return.

        Note: We can expand the accepted args on an as needed basis.

        Note: Basic search algorithm to allow for first iteration of autocomplete for Pokemon.
            An improvement may be auto-correction by pulling list of unique pokemon names and performing
            some kind of scoring based off of that.
        """
        pokemon_form = f"^{pokemon_form.upper()}"
        if pokemon_name is not None:
            pokemon_name = f"^{pokemon_name.upper()}"
            logger.debug(
                f"search_pokemon_forms: Filtering search results for Pokemon '{pokemon_name}'."
            )

        query = sql.SQL("""
            SELECT DISTINCT ON (pokemons.form) pokemons.form, pokemons.form_alias
            FROM pokemons
            WHERE
                (pokemons.form ~* {form} OR pokemons.form_alias ~* {form}) AND (
                    {name} IS NULL OR pokemons.name ~* {name} OR pokemons.name_alias ~* {name}
                )
            ORDER BY pokemons.form ASC
            """).format(
            form=sql.Literal(pokemon_form), name=sql.Literal(pokemon_name)
        )

        with self.db_pool.connection() as conn:
            cursor = conn.cursor()
            result = cursor.execute(query)
            forms = result.fetchmany(size=num_results)
            forms = [
                SearchedPokemonForm(f[0], f[1])
                for f in forms
            ] # Extract form names from tuples.
        
        found_forms = [f.form for f in forms]
        logger.debug(
            f"search_pokemon_forms: Search for form '{pokemon_form}' yielded '{found_forms}' as possible results."
        )

        return forms
