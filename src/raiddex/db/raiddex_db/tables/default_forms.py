import logging

from typing import NamedTuple

from psycopg import sql
from psycopg.rows import namedtuple_row

from raiddex.core.parser.parser import Parser
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column


logger = logging.getLogger(__name__)


class DefaultFormsTable(AbstractTable):

    name = "default_forms"
    columns = [
        Column("name", "varchar(20)"),
        Column("form", "varchar(25)")
    ]
    table_attributes = (
        "PRIMARY KEY (name), "
        "FOREIGN KEY (name, form) REFERENCES pokemons (name, form)"
    )

    def _add_missing_default_forms(
        self,
        parsed_forms: dict[str, str],
        db_forms: dict[str, str]
    ) -> dict[str, str]:
        """
        Add parsed_pokemon entries that are not found in the Database.

        Returns a list of pokemon that were missing from the Database.

        On boot, parsed_pokemons is our source of truth.
        """
        missing_forms = {
            name: form
            for name, form in parsed_forms.items()
            if name not in db_forms
        }
        if len(missing_forms) > 0:
            missing_names = [
                f"{name}->{form}" for name, form in missing_forms.items()
            ]
            self.add_default_forms(missing_forms)
            logger.info(f"Missing default form being added to the DB: {missing_names}")
        
        else:
            logger.info(f"No missing default forms need to be added for table '{self.name}'.")

        return missing_forms

    def _create_sql_key_value_pair(self, column_name):
        sql_pair = sql.SQL(
            "{column_name} = {column_placeholder}"
            ).format(
            column_name=sql.Identifier(column_name),
            column_placeholder=sql.Placeholder(column_name)
        )

        return sql_pair

    def update_default_forms(self, default_forms_to_update: dict[str, str]) -> None:
        """
        Update individual default forms with new values.

        Updating the name is not permitted.

        This will update each of the specified Pokemon with new values.
        """
        updatable_columns = self.column_names.copy()
        updatable_columns.remove("name")

        update_cmd = sql.SQL(
            """
            UPDATE {table_name}
            SET {param_placeholders}
            WHERE name = {name}
            """
            ).format(
            table_name=sql.Identifier(self.name),
            param_placeholders=sql.SQL(", ").join(map(self._create_sql_key_value_pair, updatable_columns)),
            name=sql.Placeholder("name")
        )
        with self.db_pool.connection() as conn:
            with conn.transaction():
                for name, form in default_forms_to_update.items():
                    params = {
                        "name": name,
                        "form": form
                    }
                    conn.execute(update_cmd, params)
                    logger.debug(f"Updated default form {name}->{form}.")

    def _update_existing_default_forms(
        self,
        parsed_default_forms: dict[str, str],
        db_default_forms: dict[str, str]
    ) -> dict[str, str]:
        """
        Update default forms in the Database that are different from the parsed entries.

        Returns a list of default forms that were updated in the Database.

        On boot, parsed_default_forms is our source of truth.
        """
        update_default_forms = {
            name: form
            for name, form in parsed_default_forms.items()
            if form != db_default_forms[name]
        }
        if len(update_default_forms) > 0:
            update_names = [
                f"{name}->{form}" for name, form in update_default_forms.items()
            ]
            self.update_default_forms(update_default_forms)
            logger.info(f"Default Form being updated in the DB: {update_names}")
        else:
            logger.info(f"No existing default forms need to be updated for table '{self.name}'.")

        return update_default_forms

    def update_table(self, data_parser: Parser):
        parsed_default_forms = data_parser.custom_default_forms.copy()
        db_default_forms = self._get_all_default_forms()

        # First, add missing default forms
        missing_default_forms = self._add_missing_default_forms(
            parsed_default_forms,
            db_default_forms
        )
        
        # Second, exclude newly added missing default forms from update
        for missing_default_form in missing_default_forms.keys():
            del parsed_default_forms[missing_default_form]

        # Third, update any existing Pokemon with new attributes.
        updated_default_forms = self._update_existing_default_forms(
            parsed_default_forms,
            db_default_forms
        )

    def _get_all_default_forms(self) -> dict[str, str]:
        """
        Get all DefaultForms stored in PostgreSQL.

        Not recommended for general use. Intended for startup activities.
        If needed, make a more specific function that can filter results.
        """
        query = sql.SQL("SELECT * FROM {table_name}").format(table_name=sql.Identifier(self.name))

        with self.db_pool.connection() as conn:
            cursor = conn.cursor(row_factory=namedtuple_row)
            result = cursor.execute(query)
            default_forms = result.fetchall()
        
        default_forms = {
            name: form
            for name, form in default_forms
        }

        return default_forms

    def populate_table(self, data_parser: Parser) -> None:
        self.add_default_forms(data_parser.custom_default_forms)

    def add_default_forms(self, default_forms: dict[str, str]) -> None:
        """
        Add many default forms at once.
        """
        insert_cmd = sql.SQL(
            """
            INSERT INTO {table_name} ({column_names})
            VALUES ({param_placeholders})
            """).format(
            table_name=sql.Identifier(self.name),
            column_names=sql.SQL(", ").join(map(sql.Identifier, self.column_names)),
            param_placeholders=sql.SQL(", ").join(map(sql.Placeholder, self.column_names))
        )

        with self.db_pool.connection() as conn:
            with conn.transaction():
                for name, form in default_forms.items():
                    params = {
                        "name": name,
                        "form": form
                    }
                    result = conn.execute(insert_cmd, params)
                    logger.debug(f"Default form for '{name}' set to '{form}'.")
        logger.debug(f"Batch added {len(default_forms)} Default Forms into PostgreSQL.")

    def add_default_form(self, pokemon_name: str, form: str):
        insert_cmd = sql.SQL("""
            INSERT INTO default_forms (name, form)
            VALUES ({name}, {form})
        """).format(
            name=sql.Literal(pokemon_name), form=sql.Literal(form)
        )

        with self.db_pool.connection() as conn:
            result = conn.execute(insert_cmd)
            logger.debug(f"Default form for '{pokemon_name}' set to '{form}'.")
    
    def get_default_form(self, pokemon_name: str) -> str:
        query_cmd = sql.SQL("""
            SELECT default_forms.form
            FROM default_forms
            WHERE default_forms.name = {name}
        """).format(
            name=sql.Literal(pokemon_name)
        )

        with self.db_pool.connection() as conn:
            cursor = conn.cursor()
            result = cursor.execute(query_cmd)
            pokemon_form = result.fetchone()
            if pokemon_form is not None:
                pokemon_form = pokemon_form[0]
        
        return pokemon_form
