import logging

from typing import List, Optional
from psycopg import sql
from psycopg_pool import ConnectionPool
from raiddex.core.parser.parser import Parser
from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column

from raiddex.db.connection_config import get_conn_config
from raiddex.db.db_pool import DBPool
from raiddex.db.raiddex_db.tables.default_forms import DefaultFormsTable
from raiddex.db.raiddex_db.tables.pokemons import PokemonsTable


logger = logging.getLogger(__name__)


class RaidDexDB:

    # Ideally, this should be replaced with a dependency graph of tables.
    # In the interest of time we will just use a list.
    managed_tables: list[type[AbstractTable]] = [
        PokemonsTable, DefaultFormsTable
    ]

    def __init__(self, db_pool: Optional[ConnectionPool] = None):
        """
        Create a Management object for the `raiddex` Postgres DB.

        If db_pool is not specified, then it will default to the ConnectionPool
        stored in `DBPool.raiddex_pool` for all connections.
        """
        if db_pool is None:
            db_pool = DBPool.raiddex_pool
        self.raiddex_pool = db_pool

        conn_config = get_conn_config()
        self.raiddex_db = conn_config.get_raiddex_db()
        self.raiddex_user_name = conn_config.get_raiddex_user_name()

    def list_tables(self) -> list[str]:
        """
        Get the list of tables present in the `raiddex` database.

        This list excludes any system tables that may be present or viewable to the
        `raiddex` role.
        """
        query = sql.SQL("""
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema <> 'pg_catalog' AND
                table_schema <> 'information_schema'
        """)

        with self.raiddex_pool.connection() as conn:
            result = conn.execute(query)
            tables = [table[0] for table in result.fetchall()]
        
        return tables

    def _initialize_table(self, table: AbstractTable) -> None:
        with DBPool.get_su_conn(self.raiddex_db) as su_conn:
            create_table_cmd = table.create_table_sql()
            su_conn.execute(create_table_cmd)
            logger.warning(f"Table '{table.name}' created in Database '{self.raiddex_db}'.")
            su_conn.execute(
                sql.SQL(
                    "GRANT SELECT, INSERT, UPDATE ON {table_name} TO raiddex"
                ).format(
                    table_name=sql.Identifier(table.name)
                )
            )
            logger.warning(f"Role '{self.raiddex_user_name}' granted ADDITION access to '{table.name}'.")

    def initialize_tables(self, data_parser: Parser) -> None:
        """
        Create RaidDex data tables and grant appropriate permissions to raiddex user.

        Superuser needs to connect to the raiddex db for this to work correctly.

        TODO: Consider renaming to initialize_database
        """

        existing_tables = self.list_tables()
        tables_to_create = [
            table for table in self.managed_tables
            if table.name not in existing_tables
        ]
        tables_to_check = [
            table for table in self.managed_tables
            if table.name in existing_tables
        ]

        logger.info(f"Existing tables located: {existing_tables}")
        for table_cls in tables_to_create:
            table = table_cls(self.raiddex_pool)
            self._initialize_table(table)
            table.populate_table(data_parser)
        
        for table_cls in tables_to_check:
            logger.debug(f"Checking table '{table_cls.name}' for updates.")
            table = table_cls(self.raiddex_pool)
            self.sync_table_schema(table)
            table.update_table(data_parser)

    def _get_missing_columns(
        self,
        table: AbstractTable
    ) -> List[Column]:
        current_columns = table.list_columns()
        missing_columns = [
            column for column in table.columns
            if column.name not in current_columns
        ]

        return missing_columns
    
    def _update_table_schema(
        self,
        table: AbstractTable,
        columns_to_update: List[Column]
    ) -> None:
        base_query = sql.SQL(
            """
            ALTER TABLE {table_name} ADD COLUMN {column_attrs}
            """
        )

        with DBPool.get_su_conn(self.raiddex_db) as su_conn:
            with su_conn.transaction():
                for missing_column in columns_to_update:
                    query = base_query.format(
                        table_name=sql.SQL(table.name),
                        column_attrs=missing_column.to_sql()
                    )
                    su_conn.execute(query)
                    logger.warning(f"Added column '{missing_column.name}' to table '{table.name}'.")
    
    def sync_table_schema(self, table: AbstractTable) -> None:
        """
        Update the PostgreSQL table schema to match the current source code schema.

        Source Code currently defines how the table schema should be initially constructed,
        so if the table schema needs to be updated in source, then the PostgreSQL table
        should be updated as well.

        Supported Operations:
        - Add new Columns
        """
        missing_columns = self._get_missing_columns(table)

        if missing_columns:
            logger.info(f"Table '{table.name}' is missing columns '{missing_columns}'.")
            self._update_table_schema(table, missing_columns)
        else:
            logger.info(f"No schema updates needed for table '{table.name}'.")

    def get_pokemon_table(self) -> PokemonsTable:
        return PokemonsTable(self.raiddex_pool)

    def get_default_form_table(self) -> DefaultFormsTable:
        return DefaultFormsTable(self.raiddex_pool)
    