import json
import logging
import pathlib

from typing import List
from psycopg import sql, Connection
from raiddex.core.parser.parser import Parser

from raiddex.db.db_pool import DBPool
from raiddex.db.raiddex_db import RaidDexDB
from raiddex.db.telemetry.telemetry_db import TelemetryDB

from raiddex.db.connection_config import get_conn_config


logger = logging.getLogger(__name__)


class DBManager():
    """
    Management class for DB actions, operates as cluster superuser.

    Responsible for setting up DB users, databases, and tables.

    After entity creation, adequate permissions should be given to
    other DB users to perform regular queries & inserts.
    """

    def __init__(self) -> None:
        self.raiddex_db = RaidDexDB()
        self.telemetry_db = TelemetryDB()

    def _create_user(self, user_name: str, user_pass: str = None) -> None:
        """
        Create a new User (Role) Account within the PostgreSQL cluster.

        Must be executed by the superuser account.
        """
        query = sql.SQL("CREATE ROLE {user_name} WITH LOGIN PASSWORD {user_pass}").format(
            user_name=sql.Identifier(user_name), user_pass=sql.Literal(user_pass)
        )

        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute(query)
            logger.warning(f"PostgreSQL Role `{user_name}` added to DB cluster.")

    def _drop_user(self, user_name: str) -> None:
        """
        Drop a user from the PostgreSQL cluster.

        Must be executed by the superuser account.
        """
        query = sql.SQL("DROP ROLE {user_name}").format(
            user_name=sql.Identifier(user_name)
        )

        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute(query)
            logger.warning(f"PostgreSQL Role '{user_name}' removed from DB cluster.")

    def _list_users(self) -> list[str]:
        """
        Return a list of users present in the DB.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"usename\" FROM pg_user")
            user_names = [user[0] for user in result.fetchall()]
        return user_names

    def _create_database(self, db_name: str) -> None:
        """
        Create a new Database within the PostgreSQL cluster.

        db_name - Name of the new database.

        Must be executed by the superuser account.
        """
        query = sql.SQL("CREATE DATABASE {db_name}").format(
            db_name=sql.Identifier(db_name)
        )

        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute(query)
            logger.warning(f"PostgreSQL Database '{db_name}' added to DB cluster.")

    def _drop_database(self, db_name: str) -> None:
        """
        Drop a database from the PostgreSQL cluster.

        Must be executed by the superuser account.
        """
        query = sql.SQL("DROP DATABASE {db_name}").format(
            db_name=sql.Identifier(db_name)
        )

        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute(query)
            logger.warning(f"PostgreSQL Database '{db_name}' removed from DB cluster.")

    def _list_databases(self) -> list[str]:
        """
        List the database names stored in the PostgreSQL cluster.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"datname\" FROM pg_database")
            db_names = [db[0] for db in result.fetchall()]
        return db_names

    def _grant_db_access(self, db_name: str, user_name: str) -> None:
        """
        Grant basic connect access to a PostgreSQL database to a specific user.

        Must be executed by the superuser account.
        """
        query = sql.SQL("GRANT CONNECT ON DATABASE {db_name} TO {user_name}").format(
            db_name=sql.Identifier(db_name), user_name=sql.Identifier(user_name)
        )

        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute(query)
            logger.warning(f"PostgreSQL Role '{user_name}' granted connect access to database '{db_name}'.")

    def create_users(self) -> None:
        """
        Create the various user accounts that will be used to access the PostgreSQL cluster.
        """
        conn_config = get_conn_config()
        raiddex_user_name = conn_config.get_raiddex_user_name()
        raiddex_user_pw = conn_config.get_raiddex_user_password()
        telemetry_user_name = conn_config.get_telemetry_user_name()
        telemetry_user_pw = conn_config.get_telemetry_user_password()

        users = self._list_users()
        logger.info(f"Existing Roles on DB initialization: {users}")
        if raiddex_user_name not in users:
            self._create_user(raiddex_user_name, raiddex_user_pw)
        if telemetry_user_name not in users:
            self._create_user(telemetry_user_name, telemetry_user_pw)

    def create_databases(self) -> None:
        """
        Create the various Databases that will be used in the PostgreSQL cluster.
        """
        conn_config = get_conn_config()
        raiddex_db = conn_config.get_raiddex_db()
        raiddex_user_name = conn_config.get_raiddex_user_name()
        telemetry_db = conn_config.get_telemetry_db()
        telemetry_user_name = conn_config.get_telemetry_user_name()

        databases = self._list_databases()
        logger.info(f"Existing Databases on DB initialization: {databases}")
        if raiddex_db not in databases:
            self._create_database(raiddex_db)
            self._grant_db_access(raiddex_db, raiddex_user_name)
        
        if telemetry_db not in databases:
            self._create_database(telemetry_db)
            self._grant_db_access(telemetry_db, telemetry_user_name)

    def initialize_db(self, data_parser: Parser) -> None:
        """
        Initialize the Postgres Database for the RaidDex system.

        data_parser - A Parser object able to parse through an external data source
            (e.g. PokeMiner) for initial Database seeding.

        TODO: Consider renaming to initialize_databases.
        """
        DBPool.superuser_pool.open(wait=True)
        # Step 1: Check for Users & Databases.
        # Step 2: Allow sub-users to do table specific checks.
        self.create_users()
        self.create_databases()
        
        DBPool.raiddex_pool.open(wait=True)
        DBPool.telemetry_pool.open(wait=True)

        self.raiddex_db.initialize_tables(data_parser)
        self.telemetry_db.initialize_tables()
