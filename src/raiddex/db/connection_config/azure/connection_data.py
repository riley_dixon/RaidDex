import json
from importlib.resources import files


class AzureDBConnectionData:
    """
    Fetch Connection Data for connecting to Azure.
    """

    def __init__(self) -> None:
        self.default_db = None
        self.db_address = None
        self.db_port = 5432
        self.raiddex_db = None
        self.raiddex_user_name = None
        self.raiddex_user_pw = None
        self.su_name = None
        self.su_pw = None
        self.telemetry_db = None
        self.telemetry_user_name = None
        self.telemetry_user_pw = None
        self._fetch_data()
    
    def _fetch_data(self):
        other_secrets_path = files("raiddex").joinpath(".secrets.json")
        with other_secrets_path.open("r") as other_secrets_file:
            db_secrets = json.load(other_secrets_file)["db"]
            
            self.db_address = db_secrets["db_address"]
            self.default_db = db_secrets["default_db"]
            self.su_name = db_secrets["db_admin_user_name"]
            self.su_pw = db_secrets["db_admin_pw"]
            self.raiddex_db = db_secrets["raiddex_db_name"]
            self.raiddex_user_name = db_secrets["raiddex_user_name"]
            self.raiddex_user_pw = db_secrets["raiddex_user_pw"]
            self.telemetry_db = db_secrets["telemetry_db_name"]
            self.telemetry_user_name = db_secrets["telemetry_user_name"]
            self.telemetry_user_pw = db_secrets["telemetry_user_pw"]
