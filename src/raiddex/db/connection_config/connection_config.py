import os
import pathlib
from raiddex.db.connection_config.azure.connection_data import AzureDBConnectionData

from raiddex.db.connection_config.gitlab_ci.connection_data import GitlabCIDBConnectionData
from raiddex.db.connection_config.local.connection_data import LocalDBConnectionData
from raiddex.utils.op_env import OperatingEnvironment as OpEnv, get_operating_environment

class DBConnectionConfig:

    def __init__(self) -> None:
        self.op_env = get_operating_environment()
        self.data = None
        self._fetch_data()
    
    def _fetch_data(self):
        if self.op_env in OpEnv.DEVELOPMENT | OpEnv.TESTING:
            self.data = LocalDBConnectionData(self.op_env)
        elif self.op_env == OpEnv.GITLAB_CI:
            self.data = GitlabCIDBConnectionData()
        elif self.op_env == OpEnv.PRODUCTION:
            self.data = AzureDBConnectionData()
        else:
            raise Exception(f"UNKNOWN OPERATING ENVIRONMENT: {self.op_env}")

    def get_db_address(self):
        return self.data.db_address

    def get_db_port(self):
        return self.data.db_port

    def get_default_db(self):
        return self.data.default_db

    def get_raiddex_db(self):
        return self.data.raiddex_db
    
    def get_raiddex_user_name(self):
        return self.data.raiddex_user_name
    
    def get_raiddex_user_password(self):
        return self.data.raiddex_user_pw

    def get_su_name(self):
        return self.data.su_name

    def get_su_password(self):
        return self.data.su_pw
    
    def get_telemetry_db(self):
        return self.data.telemetry_db

    def get_telemetry_user_name(self):
        return self.data.telemetry_user_name

    def get_telemetry_user_password(self):
        return self.data.telemetry_user_pw

CONN_CONFIG = None

def get_conn_config() -> DBConnectionConfig:
    global CONN_CONFIG
    if CONN_CONFIG is None:
        CONN_CONFIG = DBConnectionConfig()
    return CONN_CONFIG