import json
import os
import pathlib


class GitlabCIDBConnectionData:
    """
    Fetch Connection Data from the GitlabCI.
    """

    def __init__(self) -> None:
        self.default_db = os.environ["POSTGRES_DB"]
        self.db_address = "postgres"
        self.db_port = 5432
        self.raiddex_db = os.environ["POSTGRES_RAIDDEX_DB"]
        self.raiddex_user_name = os.environ["POSTGRES_RAIDDEX_USER"]
        self.raiddex_user_pw = os.environ["POSTGRES_RAIDDEX_PASSWORD"]
        self.su_name = os.environ["POSTGRES_USER"]
        self.su_pw = os.environ["POSTGRES_PASSWORD"]
        self.telemetry_db = os.environ["POSTGRES_TELEMETRY_DB"]
        self.telemetry_user_name = os.environ["POSTGRES_TELEMETRY_USER"]
        self.telemetry_user_pw = os.environ["POSTGRES_TELEMETRY_PASSWORD"]
