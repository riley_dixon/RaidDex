import json
import pathlib
from importlib.resources import files

from raiddex.utils.op_env import OperatingEnvironment as OpEnv


class LocalDBConnectionData:
    """
    Fetch Connection Data from a local source.
    """

    def __init__(self, op_env: OpEnv) -> None:
        self.default_db = None
        self.db_address = "127.0.0.1"
        self.db_port = self._determine_port(op_env)
        self.raiddex_db = None
        self.raiddex_user_name = None
        self.raiddex_user_pw = None
        self.su_name = None
        self.su_pw = None
        self.telemetry_db = None
        self.telemetry_user_name = None
        self.telemetry_user_pw = None
        self._fetch_data()

    def _fetch_data(self):
        path = pathlib.Path(__file__).parent.resolve()
        # NOTE: The following 3 files are kept separate from the main secrets file.
        #   This is to allow Docker to pull default PostgreSQL credentials from the same
        #   source that RaidDex will when it requires a Database connection.
        #   These are dummy credentials and stored in the repo and are not used in prod,
        #   but offer an easy way to setup and configure a DB for local development & testing.
        with open(path / "pg-default-db", mode="r") as su_name_file:
            self.default_db = su_name_file.read()
        with open(path / "pg-su-name", mode="r") as su_name_file:
            self.su_name = su_name_file.read()
        with open(path / "pg-su-password", mode="r") as su_pw_file:
            self.su_pw = su_pw_file.read()

        other_secrets_path = files("raiddex").joinpath(".secrets.json")
        with other_secrets_path.open("r") as other_secrets_file:
            db_secrets = json.load(other_secrets_file)["db"]
            self.raiddex_db = db_secrets["raiddex_db_name"]
            self.raiddex_user_name = db_secrets["raiddex_user_name"]
            self.raiddex_user_pw = db_secrets["raiddex_user_pw"]
            self.telemetry_db = db_secrets["telemetry_db_name"]
            self.telemetry_user_name = db_secrets["telemetry_user_name"]
            self.telemetry_user_pw = db_secrets["telemetry_user_pw"]
        
    def _determine_port(self, op_env: OpEnv):
        """
        To allow concurrent DB's to operate on the same host, the port changes depending
        on what operating environment we are executing in.
        """
        if op_env == OpEnv.DEVELOPMENT:
            db_port = 5434
        elif op_env == OpEnv.TESTING:
            db_port = 5435
        else:
            raise ValueError(f"UNKNOWN OPERATING ENVIRONMENT: {op_env}")

        return db_port
