from dataclasses import dataclass
from typing import Optional

from psycopg import sql

@dataclass
class Column:
    name: str
    type: str
    constraints: Optional[str] = None

    def to_sql(self):
        attrs = (self.name, self.type, self.constraints)
        attrs = (sql.SQL(attr) for attr in attrs if attr is not None)
        return sql.SQL(" ").join(attrs)
