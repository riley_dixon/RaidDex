from abc import ABC, abstractmethod
from typing import Optional

from psycopg import sql
from psycopg_pool import ConnectionPool
from raiddex.core.parser.parser import Parser

from raiddex.db.base_column import Column

class AbstractTable(ABC):

    # Access through AbstractTable.column_names
    _column_names = []

    def __init__(self, db_pool: Optional[ConnectionPool]):
        self.db_pool = db_pool

    @property
    @abstractmethod
    def name(self) -> str:
        """
        The name of the PostgreSQL table.
        """
        pass

    @property
    @abstractmethod
    def columns(self) -> list[Column]:
        """
        List of all columns in the table and their configuration.
        """
        pass

    @property
    @abstractmethod
    def table_attributes(self) -> str:
        """
        Any table constraints/attributes to be added.

        (e.g. any SQL to be appended after the column definition)
        """
        pass

    @property
    def column_names(self) -> list[str]:
        """
        Get a view of the names of the columns for this Table.
        """
        cls = self.__class__
        if len(cls._column_names) == 0:
            cls._column_names = [
                column.name for column in self.columns
            ]
        return cls._column_names


    def create_table_sql(self) -> sql.Composed:
        """
        Construct an SQL statement to create a table.

        Note: Our DB configuration is that only the DB superuser is allowed to create tables.
            Since self.db_pool is supposed to be a ConnectionPool under a regular user account,
            we leave it to a managing object with access to the superuser ConnectionPool to
            orchestrate the table creation.
        """
        if self.table_attributes == "":
            create_cmd = sql.SQL("""
                CREATE TABLE {name} (
                    {columns}
                )
            """).format(
                name=sql.SQL(self.name),
                columns=sql.SQL(", ").join((column.to_sql() for column in self.columns)),
            )
        else:
            create_cmd = sql.SQL("""
                CREATE TABLE {name} (
                    {columns},
                    {table_attributes}
                )
            """).format(
                name=sql.SQL(self.name),
                columns=sql.SQL(", ").join((column.to_sql() for column in self.columns)),
                table_attributes=sql.SQL(self.table_attributes)
            )

        return create_cmd
    
    def list_columns(self) -> list[str]:
        """
        Get the list of columns in the table.
        """
        query = sql.SQL("""
            SELECT column_name
            FROM information_schema.columns
            WHERE table_name = {table_name}
            """).format(
                table_name=sql.Literal(self.name)
        )

        with self.db_pool.connection() as conn:
            result = conn.execute(query)
            columns = [column[0] for column in result.fetchall()]
        
        return columns

    def populate_table(self, data_parser: Parser) -> None:
        """
        Seed an initial data set into the table.

        The default behaviour is a no-op, and it is left to implementing classes
        to define what data is initially seeded, and how.
        """
        pass

    def update_table(self, data_parser: Parser) -> None:
        """
        Update the existing contents of the table.

        Unlike populate_table, this method compares existing values in the database
        and updates them with the latest data available from the `Parser`. Typically,
        there should be a method to add new data, update existing table, and (rarely)
        delete unneeded data.

        NOTE: This is not the place to modify the table schema. This needs to be handled
            by the class responsible for database management (e.g. raiddex_db).
        """
        pass
