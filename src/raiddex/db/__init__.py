"""
Manage anything to do with the PostgreSQL database.

This module contains any work related to connecting to the database,
and inserting or modifying data.

For automated setup and testing purposes, it will also be able to configure
the database if there are missing databases, tables, or users.

Work as superuser should be as limited as possible, and restrictions on deleting
data also imposed.
"""
