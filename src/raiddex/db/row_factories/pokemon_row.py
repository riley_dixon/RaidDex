from typing import Any, Sequence
from psycopg import Cursor

from raiddex.core.models.pokemon.pokemon import Pokemon

class PokemonRowFactory:
    """
    Automatically convert Pokemon PostgreSQL queries into Pokemon objects.
    """
    def __init__(self, cursor: Cursor[Any]):
        self.fields = [c.name for c in cursor.description]
    
    def __call__(self, values: Sequence[Any]) -> Pokemon:
        row = dict(zip(self.fields, values))
        return Pokemon.parse_from_postgres(row)
