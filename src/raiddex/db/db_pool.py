from psycopg import Connection
from psycopg_pool import ConnectionPool

from raiddex.db.connection_config import get_conn_config

class DBPool:
    """
    Wrapper class for handling DB connections.

    Each database will get their own separate connection to limit the scope of each user.

    Assumes that the default database "postgres" exists for initial superuser connection.
    
    Database cluster must already exist and be running on the system, otherwise this process will fail.
    """
    
    raiddex_pool = None
    superuser_pool = None
    telemetry_pool = None

    @classmethod
    def get_su_conn(cls, db_name: str) -> Connection:
        """
        Get a temporary superuser connection to a non-default database.

        This connection should only be used for temporary DB management purposes, such
        as initializing Tables.
        """
        connection_data = get_conn_config()
        user = connection_data.get_su_name()
        pw = connection_data.get_su_password()
        address = connection_data.get_db_address()
        port = connection_data.get_db_port()
        if pw is not None or pw != "":
            user = f"{user}:{pw}"

        su_conn = Connection.connect(
            f"postgresql://{user}@{address}:{port}/{db_name}",
            autocommit=True
        )

        return su_conn

    @classmethod
    def create_pool(
        cls,
        user: str,
        password: str,
        address: str,
        port: int,
        db_name: str,
        pool_name: str
    ) -> ConnectionPool:
        """
        Create a pool for a specific PostgreSQL database.
        
        Pool must be opened prior to use.

        NOTE: To handle not importing passwords into the code, passwords are stored in a
              .pgpass file located at $HOME.
        TODO: If needed, support custom args in address field.
        """
        if password is not None or password != "":
            user = f"{user}:{password}"

        if port is not None:
            address = f"{address}:{port}"

        pool = ConnectionPool(
            f"postgresql://{user}@{address}/{db_name}",
            kwargs={"autocommit": True},
            open=False,
            name=pool_name
        )

        return pool

    @classmethod
    def create_pools(cls) -> None:
        """
        Initialize the ConnectionPools used to communicate with the PostgreSQL databases.

        These pools are not automatically opened. They need to be opened (see .open()) prior to
        running any SQL queries/commands. This is intentional as we assume these ConnectionPools
        are created prior to the Databases that they point to being created in PostgreSQL. The
        only exception to this is the default `postgres` Database that is created automatically
        upon PostgreSQL cluster creation.
        """
        connection_data = get_conn_config()
        cls.superuser_pool = DBPool.create_pool(
            connection_data.get_su_name(),
            connection_data.get_su_password(),
            connection_data.get_db_address(),
            connection_data.get_db_port(),
            connection_data.get_default_db(),
            "superuser-pool"
        )
        cls.raiddex_pool = DBPool.create_pool(
            connection_data.get_raiddex_user_name(),
            connection_data.get_raiddex_user_password(),
            connection_data.get_db_address(),
            connection_data.get_db_port(),
            connection_data.get_raiddex_db(),
            "raiddex-pool"
        )

        cls.telemetry_pool = DBPool.create_pool(
            connection_data.get_telemetry_user_name(),
            connection_data.get_telemetry_user_password(),
            connection_data.get_db_address(),
            connection_data.get_db_port(),
            connection_data.get_telemetry_db(),
            "telemetry-pool"
        )

    @classmethod
    def close_pools(cls) -> None:
        """
        Close pool connections so no further queries can be made.

        WARNING: If the pool is closed shortly after opening the pool,
            there may be some connections that get opened after the pool has been closed.
            It is up the caller to deal with this race condition. Generally, this can be
            avoided by setting open(wait=True) at the cost of additional startup overhead.
        """
        cls.superuser_pool.close()
        cls.raiddex_pool.close()
        cls.telemetry_pool.close()
