# RaidDex - Pokemon GO Analysis Service
[![pipeline status](https://gitlab.com/riley_dixon/RaidDex/badges/master/pipeline.svg?ignore_skipped=true&key_text=CI/CD%20Pipeline&key_width=95)](https://gitlab.com/riley_dixon/RaidDex/-/commits/master)

Soft launched on Pokemon GO Raiders Discord Server on January 2024!

RaidDex is a Pokemon GO information service to enable players to make intelligent decisions in their gameplay. RaidDex is developed in Python, powered by PostgreSQL, and hosted on Microsoft Azure. Discord is utilized as the current front-end to interact with the service, including QoL improvements to improve the user experience.

Player Features:
- Ranking of Pokemon for use in PvP battles.
- Ability to add custom Pokemon for analysis, including Pokemon yet to be available.
- More to come!

Management Features:
- Utilizes mined data from PokeMiner's to minimize management of game changes.
- Seemlessly supports adding custom information not provided by mined data.
- Built-in Telemetry to analyze user interactions and bug detection.

Requires setting up a virtualvenv:
python3.10 -m venv --upgrade-deps ./venv


### Linux Dependencies
| Package Name  | Version | Purpose |
| ------------- | ------- | ------- |
| python3-dev   | 3.10.12 | Interpreter for Running RaidBot. Development version required for psycopg[c]. |
| python3-pip   | 22.0.2  | For installing Python packages. |
| python3-venv  | 3.10.12 | For creating a virtual environment for development and testing of RaidDex. |
| postgresql-15 | 15.5.1  | SQL DB utilized for RaidDex. |
| gcc           | \<any>  | C compiler. Required by psyopg[c]. |
| libpq-dev     | 16.1-1  | PSQL development library. Development version required by psycopg[c]. |
| *make*        | \<any>  | (Optional) Shortcuts for some common development tasks. |

### Python Dependencies
| Package Name  | Version | Purpose |
| ------------- | ------- | ------- |
| discord.py    | 2.3.2   | Python library for interacting with Discord. | 
| psycopg[c]    | 3.1.10  | C-implementation of Psycopg, used to interact with PostgreSQL DB. |
| psycopg-pool  | 3.1.7   | Psycopg companion library for utilizing Pools for connecting with PostgreSQL DB. |

### Copyright
Full copyright notices can be found in [COPYRIGHT.md](COPYRIGHT.md).
