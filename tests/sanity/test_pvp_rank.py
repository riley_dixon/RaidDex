import re

from unittest import skip
from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier

from raiddex.core.models.pokemon.pokemon import (
    Pokemon, TEMPLATE_ID_FILTER
)
from raiddex.core.tools.pvp_rank import PvPIVStats, PvPIVStat, PvPRankTool, get_iv_hex

from tests.sanity import SanityTest

class TestPvPRank(SanityTest):
    
    @classmethod
    def get_pokemon_data(cls) -> list[Pokemon]:
        """
        Return a list of all the Pokemon in the parsed PokeMiner data.

        This method will reject/discard any Pokemon entry that is was created
        for the purpose of transfering to Pokemon HOME.
        """
        cls.pokemon_data_list = []
        for node in cls.poke_miner_data:
            if (
                re.match(TEMPLATE_ID_FILTER, node["templateId"]) is None
                or (re.search(r"HOME_(FORM_)?REVERSION", node["templateId"])
                    is not None)
            ):
                continue
            cls.pokemon_data_list.append(
                Pokemon.parse_from_pokeminers(node)
            )

        # If CPMultiplier has not been parsed yet, load this data.
        if CPMultiplier._CP_MULTIPLIERS == []:
            for node in cls.poke_miner_data:
                if re.match(r"PLAYER_LEVEL_SETTINGS", node["templateId"]):
                    raw_cpm_data = node["data"]["playerLevel"]["cpMultiplier"]
                    CPMultiplier.parse_from_data(raw_cpm_data)
                    break

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.get_pokemon_data()
    
    def setUp(self) -> None:
        super().setUp()
        self.tool = PvPRankTool()

    def _get_pokemon_from_data(self, name: str) -> Pokemon:
        for pokemon in self.pokemon_data_list:
            if pokemon.name == name.upper():
                return pokemon
        
        raise Exception(f"Pokemon '{name}' not found in data.")

    @skip("Very long test >60 sec.")
    def test_pvp_rank_sanity(self):
        """
        Run tool against all registered Pokemon.
        """
        for pokemon in self.pokemon_data_list:
            # Consider multiprocessing
            print(pokemon.name)
            self.tool.calculate_ranks(pokemon, 1500)
    
    @skip("Long, but not as long.")
    def test_pvp_rank_sanity_parallel(self):
        from multiprocessing import Pool
        args = [
            (pokemon, 1500) for pokemon in self.pokemon_data_list
        ]

        with Pool() as mp_pool:
            results = mp_pool.starmap(self.tool.calculate_ranks, args)
            for result in results:
                self.assertIsInstance(result, PvPIVStats)

    def test_pvp_rank_top_mew_GL(self):
        # Could be moved to UnitTest
        mew = self._get_pokemon_from_data("mew")
        mew_ranks = self.tool.calculate_ranks(mew, 1500)

        top_mew = mew_ranks.ranks[0]
        self.assertEqual(1, top_mew.rank)
        self.assertEqual(1500, top_mew.optimal_cp)
        self.assertEqual(17.5, round(top_mew.optimal_level, 2))
        self.assertEqual(100.00, round(top_mew.score_percent, 2))
        self.assertEqual(1918.9, round(top_mew.score, 1))
    
    def test_pvp_rank_min_mew_GL(self):
        # Could be moved to UnitTest
        mew = self._get_pokemon_from_data("mew")
        mew_ranks = self.tool.calculate_ranks(mew, 1500)

        min_mew = mew_ranks.get_ivs(10, 10, 10)
        self.assertEqual(2627, min_mew.rank)
        self.assertEqual(1472, min_mew.optimal_cp)
        self.assertEqual(16.5, round(min_mew.optimal_level, 2))
        self.assertEqual(94.32, round(min_mew.score_percent, 2))
        self.assertEqual(1809.9, round(min_mew.score, 1))
    
    def test_pvp_rank_min_mew_UL(self):
        # Could be moved to UnitTest
        mew = self._get_pokemon_from_data("mew")
        mew_ranks = self.tool.calculate_ranks(mew, 2500)

        min_mew = mew_ranks.get_ivs(10, 10, 10)
        self.assertEqual(755, min_mew.rank)
        self.assertEqual(2499, min_mew.optimal_cp)
        self.assertEqual(28.0, round(min_mew.optimal_level, 2))
        self.assertEqual(97.15, round(min_mew.score_percent, 2))
        self.assertEqual(4014.7, round(min_mew.score, 1))

    def test_pvp_rank_min_mew_ML(self):
        # Could be moved to UnitTest
        mew = self._get_pokemon_from_data("mew")
        mew_ranks = self.tool.calculate_ranks(mew, -1, 40)

        min_mew = mew_ranks.get_ivs(10, 10, 10)
        self.assertEqual(760, min_mew.rank)
        self.assertEqual(3124, min_mew.optimal_cp)
        self.assertEqual(40.0, round(min_mew.optimal_level, 2))
        self.assertEqual(93.58, round(min_mew.score_percent, 2))
        self.assertEqual(5592.4, round(min_mew.score, 1))

    def test_pvp_rank_min_mew_ML_with_XL_and_BF(self):
        # Could be moved to UnitTest
        mew = self._get_pokemon_from_data("mew")
        mew_ranks = self.tool.calculate_ranks(mew, -1)

        min_mew = mew_ranks.get_ivs(10, 10, 10)
        self.assertEqual(772, min_mew.rank)
        self.assertEqual(3574, min_mew.optimal_cp)
        self.assertEqual(51.0, round(min_mew.optimal_level, 2))
        self.assertEqual(93.71, round(min_mew.score_percent, 2))
        self.assertEqual(6847.5, round(min_mew.score, 1))

    def test_pvp_rank_equal_stat_prod(self):
        dialga = self._get_pokemon_from_data("dialga")
        ranks = self.tool.calculate_ranks(dialga, -1)

        first_dialga = ranks.ranks[0]
        second_dialga = ranks.ranks[1]

        self.assertEqual(first_dialga.score, second_dialga.score)
        self.assertEqual(15, first_dialga.attack_iv)
        self.assertEqual(15, first_dialga.defense_iv)
        self.assertEqual(15, first_dialga.stamina_iv)
        self.assertEqual(15, second_dialga.attack_iv)
        self.assertEqual(15, second_dialga.defense_iv)
        self.assertEqual(14, second_dialga.stamina_iv)
