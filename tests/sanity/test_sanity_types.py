import re

from tests.sanity import SanityTest

from raiddex.core.models.types.types import (
    PokemonType, TypeMatchups, TEMPLATE_ID_FILTER
)


class TestSanityTypes(SanityTest):
    
    @classmethod
    def get_type_data(cls):
        cls.types = []
        for node in cls.poke_miner_data:
            if re.match(TEMPLATE_ID_FILTER, node["templateId"]) is not None:
                cls.types.append(node)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.get_type_data()
    
    def setUp(self):
        self.assertEqual(len(self.types), 18)  # There is 18 total types in game.

        for type_data in self.types:
            try:
                TypeMatchups.register_type_from_data(type_data)
            except Exception:
                self.fail(f"Failed registering type {type_data['templateId']}")
        
        self.assertTrue(TypeMatchups._INITIALIZED)

    def test_effective_attack(self):
        attack_type = PokemonType.NORMAL
        defending_pokemon_type = [PokemonType.NORMAL]

        expected_multiplier = 1.0
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_super_effective_attack(self):
        attack_type = PokemonType.ELECTRIC
        defending_pokemon_type = [PokemonType.WATER]

        expected_multiplier = 1.6
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_double_super_effective_attack(self):
        attack_type = PokemonType.GRASS
        defending_pokemon_type = [
            PokemonType.WATER, PokemonType.GROUND
        ]

        expected_multiplier = 1.6 * 1.6
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_not_very_effective_attack(self):
        attack_type = PokemonType.FIRE
        defending_pokemon_type = [PokemonType.ROCK]

        expected_multiplier = 0.625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_double_not_very_effective(self):
        attack_type = PokemonType.FIGHTING
        defending_pokemon_type = [
            PokemonType.FLYING, PokemonType.PSYCHIC
        ]

        expected_multiplier = 0.625 * 0.625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_no_effect_attack(self):
        attack_type = PokemonType.DRAGON
        defending_pokemon_type = [
            PokemonType.FAIRY
        ]

        expected_multiplier = 0.390625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_double_no_effect_attack(self):
        """
        This type matchup is actually theoretically impossible.
        """
        expected_multiplier = 0.390625 * 0.390625
        pass

    def test_super_and_not_very_effective_attack(self):
        attack_type = PokemonType.POISON
        defending_pokemon_type = [
            PokemonType.GRASS, PokemonType.GHOST
        ]

        expected_multiplier = 1.6 * 0.625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_super_and_no_effect_attack(self):
        attack_type = PokemonType.GROUND
        defending_pokemon_type = [
            PokemonType.STEEL, PokemonType.FLYING
        ]

        expected_multiplier = 1.6 * 0.390625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

    def test_not_very_effective_and_no_effect_attack(self):
        attack_type = PokemonType.GHOST
        defending_pokemon_type = [
            PokemonType.DARK, PokemonType.NORMAL
        ]

        expected_multiplier = 0.625 * 0.390625
        
        multiplier = TypeMatchups.get_type_multiplier(
            attack_type,
            defending_pokemon_type
        )

        self.assertAlmostEqual(expected_multiplier, multiplier)

