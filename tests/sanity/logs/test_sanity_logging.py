import logging
import os
import pathlib
import tempfile
import time

from datetime import datetime, timedelta, timezone
from logging.handlers import TimedRotatingFileHandler

from .. import SanityTest
from raiddex.utils.setup_logging import swap_datetime_order

class TestSanityLogger(SanityTest):

    def test_timed_rotating_file_handler(self):
        """
        Basic test to just verify that the TimedRotatingFileHandler
        with custom `namer` (and maybe `rotator`) attributes works
        as intended.

        While this does not test our actual configured TimedRotatingFileHandler
        it should be safe to assume that the standard config works fine for this
        library, and we can extrapolate it to our logger under test here.

        NOTE: While this does not exist as a `true` unit test, it is still valuable
            to verify that our non-standard Handler is tested. As a result this does
            leave a race condition present if for some reason tests are running
            exceptionally slowly. This would be caused by the logger not emitting
            a message for each second. Running 120 iterations of this test with an
            expected at best execution time of 600 seconds completed in 601.0 seconds
            (including all other overhead). As such, while I recognize the race condition
            exists, it is unlikely this test will fail.
        """
        base_filename = "test_log.log"
        test_logger = logging.getLogger(__name__)
        backup_count = 3
        sleep_time = 5
        with tempfile.TemporaryDirectory() as temp_dir_path:
            full_path = pathlib.Path(temp_dir_path) / base_filename
            handler = TimedRotatingFileHandler(
                filename=full_path,
                when="S",
                backupCount=backup_count, # 3 seconds of logs kept.
                delay=True,
                utc=True
            )
            handler.namer = swap_datetime_order
            test_logger.addHandler(handler)
            test_logger.propagate = False
            self.addCleanup(handler.close)
            self.addCleanup(test_logger.removeHandler, handler)
            
            start_time = datetime.now(timezone.utc)
            for i in range(0, sleep_time):
                time.sleep(1)
                test_logger.error(f"Test error message {i}")

            # Expect 1 current file and 3 backup.
            files = os.listdir(temp_dir_path)
            self.assertEqual(4, len(files))
            self.assertIn(base_filename, files)
            files.remove(base_filename)

            name, extension = base_filename.split(".")
            for i in range(0, backup_count):
                # sleep_time-1-[0, backup_count) produces the expected file names.
                expected_datetime = (
                    start_time + timedelta(seconds=sleep_time-1-i)
                ).strftime("%Y-%m-%d_%H-%M-%S")
                expected_filename = f"{name}.{expected_datetime}.{extension}"
                self.assertIn(expected_filename, files)
