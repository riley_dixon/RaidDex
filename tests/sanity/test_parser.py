from tests.sanity import SanityTest

from raiddex.core.parser.parser import Parser
from raiddex.core.raider_dex import RaiderDex

class TestSanityPokemon(SanityTest):

    def setUp(self) -> None:
        super().setUp()
        self.parser = Parser()
    
    def test_parser(self):
        self.parser.parse_all_data()
