"""
To be used for testing the DB Upgrade Path. Usually this script is invoked more than once during a test run.

On the first invocation, this script generally should initialize the DB into a good known state.
On subsequent invocations, this script should test the upgrade path from a known good state to the
current branch being tested.

This script does not drive these subsequent executions. It is expected that code will have changed
between invocations and it is left up to the caller to synchronize test environments.
"""
from raiddex.core.parser.parser import Parser
from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager

data_parser = Parser()
data_parser.parse_all_data()

DBPool.create_pools()

db_manager = DBManager()
db_manager.initialize_db(data_parser)
# Isn't that easy?!