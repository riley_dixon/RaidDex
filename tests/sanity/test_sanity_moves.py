import re

from tests.sanity import SanityTest

from raiddex.core.models.moves.pve_move import PvEMove
from raiddex.core.models.moves.pve_move import TEMPLATE_ID_FILTER as PVE_FILTER
from raiddex.core.models.moves.pvp_move import PvPMove
from raiddex.core.models.moves.pvp_move import TEMPLATE_ID_FILTER as PVP_FILTER

class TestSanityMoves(SanityTest):
    
    @classmethod
    def get_move_data(cls):
        """
        Return a list of all the moves in the parsed PokeMiner data.
        """
        cls.pve_moves = []
        cls.pvp_moves = []

        for node in cls.poke_miner_data:
            if re.match(PVE_FILTER, node["templateId"]) is not None:
                cls.pve_moves.append(node)
            elif re.match(PVE_FILTER, node["templateId"]) is not None:
                cls.pvp_moves.append(node)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.get_move_data()
    
    def test_sanity_init_pve(self):
        for move_data in self.pve_moves:
            try:
                PvEMove.parse_from_data(move_data)
            except Exception:
                self.fail(f"Failed initializing PvE Move {move_data['templateId']}")

    def test_sanity_init_pvp(self):
        for move_data in self.pvp_moves:
            try:
                PvPMove.parse_from_data(move_data)
            except Exception:
                self.fail(f"Failed initializing PvP Move {move_data['templateId']}")
