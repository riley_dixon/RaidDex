import re

from tests.sanity import SanityTest

from raiddex.core.models.forms.forms import Forms, TEMPLATE_ID_FILTER

class TestSanityForms(SanityTest):
    
    @classmethod
    def get_form_data(cls):
        cls.forms = []
        for node in cls.poke_miner_data:
            if re.match(TEMPLATE_ID_FILTER, node["templateId"]) is not None:
                cls.forms.append(node)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.get_form_data()
    
    def test_sanity_init(self):
        for form_data in self.forms:
            try:
                Forms.parse_from_data(form_data)
            except Exception:
                self.fail(f"Failed initializing Form {form_data['templateId']}")