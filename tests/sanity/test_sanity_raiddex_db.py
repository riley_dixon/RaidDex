import logging
import pathlib
import tempfile
import urllib.request

from unittest.mock import patch

from psycopg import sql
from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.models.types.types import PokemonType

from raiddex.db.abstract_table import AbstractTable
from raiddex.db.connection_config.connection_config import get_conn_config
from raiddex.db.raiddex_db.raiddex_db import RaidDexDB
from raiddex.core.parser.parser import Parser
from raiddex.db.db_manager import DBManager
from raiddex.db.db_pool import DBPool

from tests.sanity import SanityTest


class TestSanityRaidDexDB(SanityTest):
    
    @classmethod
    def disable_debug_logs(cls):
        """
        Helper function to disable debug logs for testing.

        This can probably be removed once the test runs properly in a TESTING env.
        """
        from raiddex.core.models.pokemon.pokemon_factory import PokemonFactory
        cls.logger_pk_factory = logging.getLogger(PokemonFactory.__module__)
        cls.old_log_level = cls.logger_pk_factory.level
        cls.logger_pk_factory.setLevel(logging.INFO)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.disable_debug_logs()
        
        cls.parser = Parser()
        cls.parser.parse_all_data()

        DBPool.create_pools()
        DBPool.superuser_pool.open()

        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.raiddex_db_name = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()
        cls.raiddex_pw = conn_config.get_raiddex_user_password()

        cls.db_manager = DBManager()
        cls.db_manager._create_user(cls.raiddex_user, cls.raiddex_pw)
        cls.db_manager._create_database(cls.raiddex_db_name)
        cls.db_manager._grant_db_access(cls.raiddex_db_name, cls.raiddex_user)
        DBPool.raiddex_pool.open()

        cls.addClassCleanup(DBPool.close_pools)
        cls.addClassCleanup(cls.db_manager._drop_user, cls.raiddex_user)
        cls.addClassCleanup(cls.db_manager._drop_database, cls.raiddex_db_name)
        cls.addClassCleanup(DBPool.raiddex_pool.close)

        cls.raiddex_db = RaidDexDB()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.logger_pk_factory.setLevel(cls.old_log_level)
        super().tearDownClass()

    def setUp(self):
        pass

    def _get_test_pokemon(self) -> Pokemon:
        test_pokemon = Pokemon()
        test_pokemon.name = "TEST_1"
        test_pokemon.name_alias = None
        test_pokemon.form = "NORMAL"
        test_pokemon.form_alias = None
        test_pokemon.id = 0
        test_pokemon.types = [PokemonType.FIGHTING, PokemonType.NORMAL]
        test_pokemon.base_attack = 1
        test_pokemon.base_defense = 2
        test_pokemon.base_stamina = 3

        return test_pokemon

    def _drop_table(self, table: AbstractTable):
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            drop_cmd = sql.SQL("DROP TABLE {table_name}").format(
                table_name=sql.Identifier(table.name)
            )
            su_conn.execute(drop_cmd)

    def test_initialize_tables(self):
        """
        Sanity test to ensure RaidDexDB tables can be populated.
        """
        expected_tables = RaidDexDB.managed_tables
        for table in expected_tables:
            self.addCleanup(self._drop_table, table)

        self.raiddex_db.initialize_tables(self.parser)

        actual_tables = self.raiddex_db.list_tables()
        for table in expected_tables:
            expected_table_name = table.name
            self.assertIn(expected_table_name, actual_tables)

    def test_update_table(self):
        """
        Sanity test new data being added into the DB.

        This test fetches the game_master data files from the master branch from Gitlab,
        initializes the DB with it, and then tries to reinitialize the DB under the game
        master files in the current branch.

        WARNING: Running this test directly only tests adding new data/updating existing data.
            Schema changes are not tested here as we cannot tell what the "starting point"
            should be. For this, we need to change the files on disk, or implement some
            other mechanism to keep track of schema's across versions.

            See run_upgrade_db_test.sh for logic to run this within a Docker container
            to avoid changing files on disk.

        This is intended as a faster test than the system test that performs a more
        comprehensive upgrade from different git refs.
        """
        expected_tables = RaidDexDB.managed_tables
        for table in expected_tables:
            self.addCleanup(self._drop_table, table)
        test_logger = logging.getLogger()
        
        CUSTOM_DATA_GITLAB_MASTER_URL = "https://gitlab.com/riley_dixon/RaidDex/-/raw/master/src/raiddex/assets/custom_game_master.json?ref_type=heads"
        POKEMINER_DATA_GITLAB_MASTER_URL = "https://gitlab.com/riley_dixon/RaidDex/-/raw/master/src/raiddex/assets/pokeminer_game_master.json?ref_type=heads"
        CUSTOM_DATA_MASTER_FILENAME = "custom_data.json"
        POKEMINER_DATA_MASTER_FILENAME = "pokeminer_data.json"

        with tempfile.TemporaryDirectory() as tempdir:
            custom_data_path = pathlib.Path(tempdir, CUSTOM_DATA_MASTER_FILENAME)
            pokeminer_data_path = pathlib.Path(tempdir, POKEMINER_DATA_MASTER_FILENAME)
            # 1) Download to a tempDir the game master files from GitLab
            test_logger.warning("DOWNLOADING FILES FROM GITLAB")
            response = urllib.request.urlopen(CUSTOM_DATA_GITLAB_MASTER_URL)
            custom_data = response.read()
            with open(custom_data_path, "wb") as custom_data_file:
                custom_data_file.write(custom_data)
            del custom_data

            response = urllib.request.urlopen(POKEMINER_DATA_GITLAB_MASTER_URL)
            pokeminer_data = response.read()
            with open(pokeminer_data_path, "wb") as pokeminer_data_file:
                pokeminer_data_file.write(pokeminer_data)
            del pokeminer_data
            
            # 2) Patch the file locations from Parser
            test_logger.warning("PATCHING FILE PATH VARS")
            with patch(Parser.__module__ + ".CUSTOM_DATA_PATH", custom_data_path) as mocked_custom_data:
                with patch(Parser.__module__ + ".PM_GAME_DATA_PATH", pokeminer_data_path) as mocked_pokeminer_data:
                    # 3) Parse the "old" data and initialize the tables with it.
                    test_logger.warning("PARSING OLD DATA")
                    old_data_parser = Parser()
                    old_data_parser.parse_all_data()
                    test_logger.warning("INITIALIZING DB")
                    self.raiddex_db.initialize_tables(old_data_parser)
                    del old_data_parser

        # 4) Parse the data again with the "current" files.
        test_logger.warning("PARSING CURRENT DATA")
        current_data_parser = Parser()
        current_data_parser.parse_all_data()
        # 5) Reinitialize the Database. Manually verify that logs show
        #    any changes that may have taken place.
        test_logger.warning("RE-INITIALIZING DB")
        self.raiddex_db.initialize_tables(current_data_parser)

    def test_get_pokemon_with_default_form(self):
        data_parser = Parser()
        data_parser.parse_all_data()
        self.raiddex_db.initialize_tables(data_parser)
        pokemon_table = self.raiddex_db.get_pokemon_table()

        tested_pokemon = data_parser.pokemons["ORICORIO_BAILE"]
        default_pokemon = pokemon_table.get_pokemon(tested_pokemon.name, "NORMAL")

        self.assertEqual(tested_pokemon, default_pokemon)
