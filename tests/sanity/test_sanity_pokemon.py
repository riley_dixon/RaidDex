import re

from tests.sanity import SanityTest

from raiddex.core.models.pokemon.pokemon import (
    Pokemon, TEMPLATE_ID_FILTER
)

class TestSanityPokemon(SanityTest):

    @classmethod
    def get_pokemon_data(cls):
        """
        Return a list of all the Pokemon in the parsed PokeMiner data.

        This method will reject/discard any Pokemon entry that is was created
        for the purpose of transfering to Pokemon HOME.
        """
        cls.pokemon_data_list = []
        for node in cls.poke_miner_data:
            if (
                re.match(TEMPLATE_ID_FILTER, node["templateId"]) is None
                or (re.search(r"HOME_(FORM_)?REVERSION", node["templateId"])
                    is not None)
            ):
                continue
            cls.pokemon_data_list.append(node)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.get_pokemon_data()

    def test_sanity_init(self):
        for pokemon_data in self.pokemon_data_list:
            try:
                Pokemon.parse_from_pokeminers(pokemon_data)
            except Exception as e:
                # Use templateId in case our filter on the poke_miner_data
                # is defective.
                pokemon_name = pokemon_data["templateId"]
                self.fail(f"Failure initializing Pokemon {pokemon_name}")

    def test_sanity_pokemon_custom_data(self):
        apex_hooh_template_id = "V0250_POKEMON_HO_OH_S"
        apex_hooh = next(
            Pokemon.parse_from_pokeminers(pokemon_data)
            for pokemon_data in self.pokemon_data_list
            if pokemon_data["templateId"] == apex_hooh_template_id
        )
        self.assertEqual(apex_hooh.name_alias, "Ho-oh")
        self.assertEqual(apex_hooh.form_alias, "Apex")
