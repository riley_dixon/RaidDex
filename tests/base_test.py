import importlib.util
import json
import unittest

RAIDDEX_PACKAGE_NAME = "raiddex"
raiddex_spec = importlib.util.find_spec(RAIDDEX_PACKAGE_NAME)
if raiddex_spec is None:
    # Force execution of test suite to end immediately.
    # Preferred to raising an Exception as unittest.discover
    # can mangle Exceptions. This will ensure our error message
    # is clearly presented to the developer.
    import sys
    error_msg = (
        f"Cannot locate the package `{RAIDDEX_PACKAGE_NAME}`.\n"
        f"This usually means that the package has not been installed correctly.\n"
        f"If this is the first time seeing this error, please ensure the following:\n"
        f"    1) You are currently within an *activated* virtual environment.\n"
        f"    2) If this is a development machine, `{RAIDDEX_PACKAGE_NAME}` should\n"
        f"       be installed in an editable manner (e.g. pip install -e `{RAIDDEX_PACKAGE_NAME}`).\n"
        f"    3) If this is a CI/Testing environment, verify that `{RAIDDEX_PACKAGE_NAME}` was\n"
        f"       properly built and installed.\n"
    )
    print(error_msg)
    sys.exit(1)

from raiddex.core.parser.parser import CUSTOM_DATA_PATH, PM_GAME_DATA_PATH, Parser


class BaseTest(unittest.TestCase):
    
    @classmethod
    def load_data(cls):
        """
        Load in the PokeMiner & Custom data JSON files into the test class.

        Any nodes that are disabled in `custom_game_master.json` will not be included.
        """
        with open(PM_GAME_DATA_PATH, "r") as pm_data:
            cls.poke_miner_data: list = json.load(pm_data)
        
        with open(CUSTOM_DATA_PATH, "r") as custom_data:
            cls.custom_data: dict = json.load(custom_data)
            custom_pokemon_data = cls.custom_data["pokemon_data"]
        
        # A bit hackish as we are kind of testing this module.
        # However, assuming the unit tests pass it should work here
        # just fine.
        parser = Parser()
        disabled_nodes = []
        for element in cls.poke_miner_data:
            if element["templateId"] in custom_pokemon_data:
                parser._merge_data(element, custom_pokemon_data[element["templateId"]])

        # Any element that has been disabled in our Custom Data in `custom_game_master.json`
        # Should not be tested.
        cls.poke_miner_data[:] = [
            element for element in cls.poke_miner_data if not element.get("raiddex_disable", False)
        ]

    @classmethod
    def setUpClass(cls, load_data: bool=True):
        """
        If load_data is True, game data will be loaded into the test.
        See `BaseTest.load_data` for more info.
        """
        cls.poke_miner_data = None
        cls.custom_data = None

        if load_data:
            cls.load_data()
