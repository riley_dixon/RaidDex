from unittest import TestCase
from unittest.mock import MagicMock

from raiddex.bot.embeds.image_urls import ImageURLs
from raiddex.core.models.pokemon.pokemon import Pokemon

class MockedPokemon(MagicMock):
    def __init__(self, id=None, form=None) -> None:
        super().__init__(spec_set=Pokemon())
        self.id = id
        self.form = form

class TestImageURLs(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.pokemon_url = ImageURLs.POKEMON_IMAGES_URL

    def test_get_pokemon(self):
        pokemon = MockedPokemon(id=151, form="NORMAL")
        url = ImageURLs.get_pokemon(pokemon)

        self.assertEqual(
            self.pokemon_url + "/pm151.icon.png",
            url
        )

    def test_get_pokemon_form(self):
        pokemon = MockedPokemon(id=122, form="GALARIAN")
        url = ImageURLs.get_pokemon(pokemon)

        self.assertEqual(
            self.pokemon_url + "/pm122.fGALARIAN.icon.png",
            url
        )
