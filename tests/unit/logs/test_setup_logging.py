import logging

from tests.unit import UnitTest

from raiddex.utils import setup_logging
from raiddex.utils.op_env import OperatingEnvironment as OpEnv

class TestSetupLogging(UnitTest):
    
    def test_swap_datetime_order(self):
        test_name = "my_log.log.2023-11-12_16-45-30"
        expected_name = "my_log.2023-11-12_16-45-30.log"

        swapped_name = setup_logging.swap_datetime_order(test_name)

        self.assertEqual(expected_name, swapped_name)

    def test_setup_file_handler_log_levels(self):
        possible_op_envs = list(OpEnv)

        expected_log_levels = {
            OpEnv.DEVELOPMENT: logging.DEBUG,
            OpEnv.TESTING: logging.DEBUG,
            OpEnv.GITLAB_CI: logging.DEBUG,
            OpEnv.PRODUCTION: logging.INFO
        }

        for op_env in possible_op_envs:
            with self.subTest(f"OpEnv: {op_env.name}"):
                handler = setup_logging._setup_file_handler(op_env)
                handler_level = handler.level
                self.assertEqual(expected_log_levels[op_env], handler_level)

    def test_setup_stderr_handler_log_levels(self):
        possible_op_envs = list(OpEnv)

        expected_log_levels = {
            OpEnv.DEVELOPMENT: logging.DEBUG,
            OpEnv.TESTING: logging.ERROR,
            OpEnv.GITLAB_CI: logging.ERROR,
            OpEnv.PRODUCTION: logging.INFO
        }

        for op_env in possible_op_envs:
            with self.subTest(f"OpEnv: {op_env.name}"):
                handler = setup_logging._setup_stderr_handler(op_env)
                handler_level = handler.level
                self.assertEqual(expected_log_levels[op_env], handler_level)

    def test_setup_logger_log_levels(self):
        possible_op_envs = list(OpEnv)

        expected_log_levels = {
            OpEnv.DEVELOPMENT: logging.DEBUG,
            OpEnv.TESTING: logging.DEBUG,
            OpEnv.GITLAB_CI: logging.DEBUG,
            OpEnv.PRODUCTION: logging.INFO
        }

        for op_env in possible_op_envs:
            with self.subTest(f"OpEnv: {op_env.name}"):
                logger = setup_logging._setup_raiddex_logger(op_env)
                logger_level = logger.level
                self.assertEqual(expected_log_levels[op_env], logger_level)
