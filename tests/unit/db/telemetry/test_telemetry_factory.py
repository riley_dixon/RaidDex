from unittest.mock import patch

from tests.unit import UnitTest

from raiddex.db.telemetry.telemetry_table import TelemetryTable
from raiddex.db.telemetry.telemetry_table_factory import AppCommandOptionType, TelemetryTableFactory


class MockedParam:
    def __init__(self, name, _type) -> None:
        self.name = NameError
        self.type = _type


class MockedCommand:
    def __init__(self, name, params) -> None:
        self.name = name
        self.parameters = params


class TestTelemetryTableFactory(UnitTest):

    MODULE_PATH = TelemetryTableFactory.__module__

    def setUp(self) -> None:
        super().setUp()
        self.telemetry_factory = TelemetryTableFactory()

    def test_get_table_cls_default(self):
        mocked_command = MockedCommand("test" ,[])
        received_cls = self.telemetry_factory._get_table_cls(mocked_command)

        self.assertIs(received_cls, TelemetryTable)

    def test_construct_column_list_int(self):
        mocked_param = MockedParam("test_int", AppCommandOptionType.integer)

        column_list = self.telemetry_factory._construct_column_list([mocked_param])

        self.assertEqual(column_list[0].name, mocked_param.name)
        self.assertEqual(column_list[0].type, "bigint")

    def test_construct_column_list_number(self):
        mocked_param = MockedParam("test_number", AppCommandOptionType.number)

        column_list = self.telemetry_factory._construct_column_list([mocked_param])

        self.assertEqual(column_list[0].name, mocked_param.name)
        self.assertEqual(column_list[0].type, "double precision")

    def test_construct_column_list_str(self):
        mocked_param = MockedParam("test_string", AppCommandOptionType.string)

        column_list = self.telemetry_factory._construct_column_list([mocked_param])

        self.assertEqual(column_list[0].name, mocked_param.name)
        self.assertEqual(column_list[0].type, "varchar(50)")

    def test_construct_column_list_unknown(self):
        mocked_param = MockedParam("test_int", "some_other_type")

        with self.assertRaises(Exception):
            self.telemetry_factory._construct_column_list([mocked_param])
    
    def test_construct_column_list_many(self):
        mocked_param1 = MockedParam("test_int", AppCommandOptionType.integer)
        mocked_param2 = MockedParam("test_number", AppCommandOptionType.number)
        mocked_param3 = MockedParam("test_string", AppCommandOptionType.string)
        mocked_params = [mocked_param1, mocked_param2, mocked_param3]

        column_list = self.telemetry_factory._construct_column_list(mocked_params)

        self.assertEqual(len(column_list), 3)

    def test_create_generic_telemetry_cls(self):
        mocked_param1 = MockedParam("test_int", AppCommandOptionType.integer)
        mocked_param2 = MockedParam("test_number", AppCommandOptionType.number)
        mocked_param3 = MockedParam("test_string", AppCommandOptionType.string)
        mocked_params = [mocked_param1, mocked_param2, mocked_param3]

        mocked_cmd = MockedCommand("test_cmd", mocked_params)

        cls = self.telemetry_factory._create_generic_telemetry_cls(mocked_cmd)

        self.assertTrue(issubclass(cls, TelemetryTable))
        column_names = [column.name for column in cls.columns]
        for mocked_param in mocked_params:
            self.assertIn(mocked_param.name, column_names)
        self.assertEqual(mocked_cmd.name, cls.name)

    def test_create_table_cls(self):
        mocked_param1 = MockedParam("test_int", AppCommandOptionType.integer)
        mocked_param2 = MockedParam("test_number", AppCommandOptionType.number)
        mocked_param3 = MockedParam("test_string", AppCommandOptionType.string)
        mocked_params = [mocked_param1, mocked_param2, mocked_param3]

        mocked_cmd = MockedCommand("test_cmd", mocked_params)

        table_cls = self.telemetry_factory.create_table_cls(mocked_cmd)

        column_names = [column.name for column in table_cls.columns]
        for mocked_param in mocked_params:
            self.assertIn(mocked_param.name, column_names)

    def test_create_table_cls_instantiate(self):
        """
        Ensure that we can instantiate the dynamic class without error.
        """
        mocked_param1 = MockedParam("test_int", AppCommandOptionType.integer)
        mocked_cmd = MockedCommand("test_cmd", [mocked_param1])

        table_cls = self.telemetry_factory.create_table_cls(mocked_cmd)

        table_cls(None)