from tests.unit import UnitTest

from raiddex.db.base_column import Column

class TestColumn(UnitTest):

    def setUp(self) -> None:
        super().setUp()
        self.test_column = Column(
            "DummyColumn",
            "int",
            "NOT NULL"
        )
    
    def test_to_sql(self):
        column_sql = self.test_column.to_sql().as_string(None)

        self.assertRegex(
            column_sql,
            r"DummyColumn int NOT NULL"
        )
