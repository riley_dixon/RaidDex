from tests.unit import UnitTest

from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column

class DummyTable(AbstractTable):
    name = "DummyName"
    columns = [
        Column("test1", "varchar(10)"),
        Column("test2", "int"),
        Column("test3", "varchar(30)", "NOT NULL")
    ]
    table_attributes = "PRIMARY KEY (test1, test2)"

class TestAbstractTable(UnitTest):

    def setUp(self):
        super().setUp()
        self.test_table = DummyTable(None)
    
    def test_create_table_sql(self):
        create_cmd = self.test_table.create_table_sql().as_string(None).strip()
        self.assertRegex(
            create_cmd,
            (
                r"CREATE TABLE DummyName \(\s*"
                    r"test1 varchar\(10\),\s*"
                    r"test2 int,\s*"
                    r"test3 varchar\(30\) NOT NULL,\s*"
                    r"PRIMARY KEY \(test1, test2\)\s*"
                r"\)"
            )
        )

    def test_create_table_sql_no_attributes(self):
        self.test_table.table_attributes = ""
        create_cmd = self.test_table.create_table_sql().as_string(None).strip()
        self.assertRegex(
            create_cmd,
            (
                r"CREATE TABLE DummyName \(\s*"
                    r"test1 varchar\(10\),\s*"
                    r"test2 int,\s*"
                    r"test3 varchar\(30\) NOT NULL\s*"
                r"\)"
            )
        )
