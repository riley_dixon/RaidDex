import json

from copy import deepcopy

from tests.unit.core import CoreUnitTest

from raiddex.core.parser.parser import Parser


POKEMINER_DATA = {
    "templateId": "V0001_POKEMON_BULBASAUR",
    "data": {
        "templateId": "V0001_POKEMON_BULBASAUR",
        "pokemonSettings": {
            "pokemonId": "BULBASAUR",
            "type": "POKEMON_TYPE_GRASS",
            "type2": "POKEMON_TYPE_POISON",
            "stats": {
                "baseStamina": 128,
                "baseAttack": 118,
                "baseDefense": 111
            },
            "quickMoves": [
                "VINE_WHIP_FAST",
                "TACKLE_FAST"
            ]
        }
    }
}

CUSTOM_DATA = {
    "data": {
        "pokemonSettings": {
            "pokemon_alias": "Mr.Bulbasaur",
            "stats": {
                "baseAttack": 9000,  # Bulbasaur found a lot of rare candy.
            },
            "quickMoves": [
                "CUSTOM_MOVE_FAST"
            ],
            "chargedMoves": [
                "CUSTOM_CHARGED_MOVE"
            ],
            "customStats": {
                "shiny": True
            }
        },
    }
}


class TestParser(CoreUnitTest):

    def setUp(self):
        self.parser = Parser()
        self.pm_data = deepcopy(POKEMINER_DATA)
        self.custom_data = deepcopy(CUSTOM_DATA)

    def test_merge_data_not_mentioned(self):
        """
        Test elements not present in CUSTOM_DATA.

        Tests elements at each possible depth level of the merged dictionary.
        """
        self.parser._merge_data(self.pm_data, self.custom_data)
        self.assertEqual(self.pm_data["templateId"], POKEMINER_DATA["templateId"])
        self.assertEqual(
            self.pm_data["data"]["templateId"], POKEMINER_DATA["data"]["templateId"]
        )
        self.assertEqual(
            self.pm_data["data"]["pokemonSettings"]["type"],
            POKEMINER_DATA["data"]["pokemonSettings"]["type"]
        )
        self.assertEqual(
            self.pm_data["data"]["pokemonSettings"]["stats"]["baseDefense"],
            POKEMINER_DATA["data"]["pokemonSettings"]["stats"]["baseDefense"]
        )
    
    def test_merge_data_new_value(self):
        self.parser._merge_data(self.pm_data, self.custom_data)
        self.assertIn("pokemon_alias", self.pm_data["data"]["pokemonSettings"])
        self.assertEqual(
            self.pm_data["data"]["pokemonSettings"]["pokemon_alias"],
            CUSTOM_DATA["data"]["pokemonSettings"]["pokemon_alias"]
        )
        self.assertListEqual(
            self.pm_data["data"]["pokemonSettings"]["chargedMoves"],
            CUSTOM_DATA["data"]["pokemonSettings"]["chargedMoves"]
        )
        self.assertDictEqual(
            self.pm_data["data"]["pokemonSettings"]["customStats"],
            CUSTOM_DATA["data"]["pokemonSettings"]["customStats"]
        )

    def test_merge_data_overwrite_value(self):
        self.parser._merge_data(self.pm_data, self.custom_data)
        self.assertEqual(
            self.pm_data["data"]["pokemonSettings"]["stats"]["baseAttack"],
            CUSTOM_DATA["data"]["pokemonSettings"]["stats"]["baseAttack"]
        )

    def test_merge_data_extend_sequence(self):
        self.parser._merge_data(self.pm_data, self.custom_data)
        self.assertListEqual(
            self.pm_data["data"]["pokemonSettings"]["quickMoves"],
            POKEMINER_DATA["data"]["pokemonSettings"]["quickMoves"] + 
                CUSTOM_DATA["data"]["pokemonSettings"]["quickMoves"]
        )

    def test_merge_data_type_mismatch(self):
        old_value = self.custom_data["data"]["pokemonSettings"]["stats"]["baseAttack"]
        self.custom_data["data"]["pokemonSettings"]["stats"]["baseAttack"] = "9000"

        with self.assertRaises(TypeError):
            self.parser._merge_data(self.pm_data, self.custom_data)
        
        self.custom_data["data"]["pokemonSettings"]["stats"]["baseAttack"] = old_value
        # Now test reflective property
        old_value = self.pm_data["data"]["pokemonSettings"]["stats"]["baseAttack"]
        self.pm_data["data"]["pokemonSettings"]["stats"]["baseAttack"] = "9000"

        with self.assertRaises(TypeError):
            self.parser._merge_data(self.pm_data, self.custom_data)
    
    def test_merge_data_type_mismatch_collection(self):
        old_value = self.custom_data["data"]["pokemonSettings"]["stats"] = "9000"
        self.custom_data["data"]["pokemonSettings"]["stats"] = "9000"

        with self.assertRaises(TypeError):
            self.parser._merge_data(self.pm_data, self.custom_data)
        
        self.custom_data["data"]["pokemonSettings"]["stats"] = old_value
        old_value = self.custom_data["data"]["pokemonSettings"]["quickMoves"]

        with self.assertRaises(TypeError):
            self.parser._merge_data(self.pm_data, self.custom_data)
