from unittest.mock import patch

from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier, MAX_LEVEL

from tests.unit.core.models import ModelUnitTest
from tests.unit.core.models.pokemon import CPM_DATA

class TestCPMultiplier(ModelUnitTest):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        CPMultiplier.parse_from_data(CPM_DATA)

    def setUp(self):
        super().setUp()
    
    def test_half_levels(self):
        """
        Need to tolerate due to some floating point rounding.

        Comparing to data accepted to be true from GamePress.
        """
        cpm_1_5 = CPMultiplier[1.5]
        self.assertAlmostEqual(cpm_1_5, 0.1351374318, places=4)
        cpm_3_5 = CPMultiplier[3.5]
        self.assertAlmostEqual(cpm_3_5, 0.2365726613, places=4)
        cpm_14_5 = CPMultiplier[14.5]
        self.assertAlmostEqual(cpm_14_5, 0.508701765, places=4)
        cpm_25_5 = CPMultiplier[25.5]
        self.assertAlmostEqual(cpm_25_5, 0.6745818959, places=4)
        cpm_36_5 = CPMultiplier[36.5]
        self.assertAlmostEqual(cpm_36_5, 0.7702972656, places=4)
        cpm_47_5 = CPMultiplier[47.5]
        self.assertAlmostEqual(cpm_47_5, 0.82779999, places=4)
        cpm_50_5 = CPMultiplier[50.5]
        self.assertAlmostEqual(cpm_50_5, 0.84279999, places=4)

    def test_invalid_level_not_divisible_by_half(self):
         with self.assertRaises(ValueError):
            CPMultiplier[1.4]

    def test_invalid_level_out_of_range(self):
        with self.assertRaises(IndexError):
            CPMultiplier[0.5]
        
        with self.assertRaises(IndexError):
            CPMultiplier[MAX_LEVEL + 0.5]
    
    def test_get_closest_level(self):
        tested_cpm = 0.74
        closest_level = CPMultiplier.get_closest_level(tested_cpm)
        self.assertEqual(31, closest_level)

        tested_cpm = 0.735
        closest_level = CPMultiplier.get_closest_level(tested_cpm)
        self.assertEqual(30.5, closest_level)
