import json


CPM_DATA = [
    0.094, 0.16639787, 0.21573247, 0.25572005, 0.29024988, 0.3210876, 0.34921268,
    0.3752356, 0.39956728, 0.4225, 0.44310755, 0.4627984, 0.48168495, 0.49985844,
    0.51739395, 0.5343543, 0.5507927, 0.5667545, 0.5822789, 0.5974, 0.6121573,
    0.6265671, 0.64065295, 0.65443563, 0.667934, 0.6811649, 0.69414365, 0.7068842,
    0.7193991, 0.7317, 0.7377695, 0.74378943, 0.74976104, 0.7556855, 0.76156384,
    0.76739717, 0.7731865, 0.77893275, 0.784637, 0.7903, 0.7953, 0.8003, 0.8053,
    0.8103, 0.8153, 0.8203, 0.8253, 0.8303, 0.8353, 0.8403, 0.8453, 0.8503, 0.8553,
    0.8603, 0.8653
]

# Please note, for unit testing purposes this also does contain
# Custom Data. Larger Integration/System tests should use the separate
# asset files.
POKEMINERS_POKEMON = json.loads("""{
    "templateId": "V0025_POKEMON_PIKACHU_NORMAL",
    "data": {
        "templateId": "V0025_POKEMON_PIKACHU_NORMAL",
        "pokemonSettings": {
            "name_alias": "PIKACHU_ALIAS",
            "form_alias": "NORMAL_ALIAS",
            "pokemonId": "PIKACHU",
            "modelScale": 1.48,
            "type": "POKEMON_TYPE_ELECTRIC",
            "camera": {
                "diskRadiusM": 0.555,
                "cylinderRadiusM": 0.37,
                "cylinderHeightM": 0.5,
                "shoulderModeScale": 0.5
            },
            "encounter": {
                "collisionRadiusM": 0.16,
                "collisionHeightM": 0.16,
                "collisionHeadRadiusM": 0.13,
                "movementType": "MOVEMENT_JUMP",
                "movementTimerS": 10.0,
                "jumpTimeS": 1.0,
                "attackTimerS": 29.0,
                "bonusCandyCaptureReward": 2,
                "attackProbability": 0.1,
                "dodgeProbability": 0.15,
                "dodgeDurationS": 1.0,
                "dodgeDistance": 1.0,
                "cameraDistance": 2.775,
                "minPokemonActionFrequencyS": 0.2,
                "maxPokemonActionFrequencyS": 1.6
            },
            "stats": {
                "baseStamina": 111,
                "baseAttack": 112,
                "baseDefense": 96
            },
            "quickMoves": [
                "THUNDER_SHOCK_FAST",
                "QUICK_ATTACK_FAST"
            ],
            "cinematicMoves": [
                "DISCHARGE",
                "THUNDERBOLT",
                "WILD_CHARGE"
            ],
            "animationTime": [
                1.8333,
                0.6667,
                1.6,
                1.5667,
                0.0,
                1.8,
                1.1333,
                1.066667
            ],
            "evolutionIds": [
                "RAICHU"
            ],
            "evolutionPips": 1,
            "pokedexHeightM": 0.4,
            "pokedexWeightKg": 6.0,
            "parentPokemonId": "PICHU",
            "heightStdDev": 0.05,
            "weightStdDev": 0.75,
            "familyId": "FAMILY_PIKACHU",
            "candyToEvolve": 50,
            "kmBuddyDistance": 1.0,
            "buddySize": "BUDDY_SHOULDER",
            "modelHeight": 0.4,
            "evolutionBranch": [
                {
                    "evolution": "RAICHU",
                    "candyCost": 50
                }
            ],
            "modelScaleV2": 1.1,
            "form": "PIKACHU_NORMAL",
            "buddyOffsetMale": [
                0.0,
                0.0,
                0.0
            ],
            "buddyOffsetFemale": [
                0.0,
                0.0,
                0.0
            ],
            "buddyScale": 11.0,
            "thirdMove": {
                "stardustToUnlock": 10000,
                "candyToUnlock": 25
            },
            "isTransferable": true,
            "isDeployable": true,
            "isTradable": true,
            "buddyGroupNumber": 1,
            "eliteQuickMove": [
                "PRESENT_FAST"
            ],
            "eliteCinematicMove": [
                "SURF",
                "THUNDER"
            ]
        }
    }
}""")
