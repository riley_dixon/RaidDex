from unittest import skip
from unittest.mock import MagicMock, patch
from raiddex.core.models.types.types import PokemonType

from tests.unit.core.models import ModelUnitTest
from tests.unit.core.models.pokemon import CPM_DATA, POKEMINERS_POKEMON

from raiddex.core.models.pokemon.pokemon import LivePokemon, Pokemon, MULTI_WORD_POKEMON
from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier


class TestPokemonModel(ModelUnitTest):

    MODULE_PATH = Pokemon.__module__

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if len(CPMultiplier._CP_MULTIPLIERS) == 0:
            CPMultiplier.parse_from_data(CPM_DATA)

    def test_equal(self):
        attrs = {
            "name": "test_name",
            "form": "test_form",
            "id": 0,
            "types": [PokemonType.NORMAL],
            "base_attack": 1,
            "base_defense": 2,
            "base_stamina": 3
        }
        pokemon1 = Pokemon(**attrs)
        pokemon2 = Pokemon(**attrs)

        self.assertEqual(pokemon1, pokemon2)

    def test_not_equal(self):
        """
        Go through each attribute one by one and make a small modification to it.

        Possible enhancement: Create these values dynamically and randomly through reflection.
        """
        attrs = {
            "name": "test_name",
            "form": "test_form",
            "id": 0,
            "types": [PokemonType.NORMAL],
            "base_attack": 1,
            "base_defense": 2,
            "base_stamina": 3
        }
        pokemon1 = Pokemon(**attrs)

        for attr_name in attrs.keys():
            other_attrs = attrs.copy()
            original_value = attrs[attr_name]
            if isinstance(original_value, int):
                new_value = original_value + 1
            elif isinstance(original_value, str):
                new_value = "OTHER" + original_value
            elif attr_name == "types":
                new_value = [PokemonType.NORMAL, PokemonType.DRAGON]
            other_attrs[attr_name] = new_value
            
            pokemon2 = Pokemon(**other_attrs)
            self.assertNotEqual(pokemon1, pokemon2)

    def test_equal_different_types(self):
        attrs = {
            "name": "test_name",
            "form": "test_form",
            "id": 0,
            "types": [PokemonType.NORMAL],
            "base_attack": 1,
            "base_defense": 2,
            "base_stamina": 3
        }
        pokemon1 = Pokemon(**attrs)

        with self.assertRaises(TypeError):
            pokemon1 == None

    def test_extract_pokemon_number(self):
        template_id = "V0025_POKEMON_PIKACHU_NORMAL"

        pokemon_number = Pokemon._extract_pokemon_number(template_id)

        self.assertEqual(25, pokemon_number)

    def test_extract_pokemon_number_missing(self):
        template_id = "V_POKEMON_PIKACHU_NORMAL"

        with self.assertRaises(ValueError):
            Pokemon._extract_pokemon_number(template_id)

    def test_extract_pokemon_name(self):
        template_id = "V0025_POKEMON_PIKACHU_NORMAL"

        pokemon_name = Pokemon._extract_pokemon_name(template_id)

        self.assertEqual("PIKACHU_NORMAL", pokemon_name)

    def test_extract_pokemon_name_missing(self):
        template_id = "V0025_POKEMON_"

        with self.assertRaises(ValueError):
            Pokemon._extract_pokemon_name(template_id)
    
    def test_extract_pokemon_form(self):
        template_id = "V0025_POKEMON_PIKACHU_NORMAL"

        form = Pokemon._extract_pokemon_form(template_id)

        self.assertEqual("NORMAL", form)

    def test_extract_pokemon_form_missing_name(self):
        template_id = "V0427_POKEMON_"

        with self.assertRaises(ValueError):
            Pokemon._extract_pokemon_form(template_id)

    def test_extract_pokemon_form_no_form(self):
        template_id = "V0427_POKEMON_BUNEARY"
        form = Pokemon._extract_pokemon_form(template_id)
        self.assertEqual("NORMAL", form)

    def test_extract_pokemon_form_multi_word_pokemon(self):
        for pokemon in MULTI_WORD_POKEMON:
            with self.subTest(f"Multi_Word Pokemon Name Form Extraction for {pokemon}."):
                template_id = f"V0122_POKEMON_{pokemon}_GALARIAN"
                form = Pokemon._extract_pokemon_form(template_id)
                self.assertEqual("GALARIAN", form)

    def test_extract_pokemon_form_multi_word_pokemon_form_missing(self):
        for pokemon in MULTI_WORD_POKEMON:
            with self.subTest(f"Multi_Word Pokemon Name Form Extraction for {pokemon}."):
                template_id = f"V0122_POKEMON_{pokemon}"
                form = Pokemon._extract_pokemon_form(template_id)
                self.assertEqual("NORMAL", form)

    def test_extract_pokemon_types_singular(self):
        type1 = PokemonType.FIRE

        with patch(self.MODULE_PATH + ".PokemonType.from_str") as mocked_from_str:
            mocked_from_str.side_effect = [type1]
            types = Pokemon._extract_pokemon_types("POKEMON_TYPE_FIRE")

        self.assertEqual(1, len(types))
        self.assertEqual([type1], types)

    def test_extract_pokemon_types_plural(self):
        type1 = PokemonType.FIRE
        type2 = PokemonType.WATER

        with patch(self.MODULE_PATH + ".PokemonType.from_str") as mocked_from_str:
            mocked_from_str.side_effect = [type1, type2]
            types = Pokemon._extract_pokemon_types("POKEMON_TYPE_FIRE", "POKEMON_TYPE_WATER")

        self.assertEqual(2, len(types))
        self.assertEqual([type1, type2], types)

    def test_parse_from_pokeminers(self):
        """
        A bit tricky to properly isolate this due to Enum's special metaclass implementation.
        __dir__() does not include `PokemonType.from_str` due to 2 things:
        1. MetaEnum __dir__ implementation only includes specific methods and declared Enum attributes.
        2. dir(PokemonType) will call the MetaEnum implementation rather than an overridden implementation.
        """
        expected_type = PokemonType.ELECTRIC
        with patch(self.MODULE_PATH + ".PokemonType.from_str") as mocked_from_str:
            mocked_from_str.side_effect = [expected_type]
            pokemon = Pokemon.parse_from_pokeminers(POKEMINERS_POKEMON)

        self.assertEqual("PIKACHU", pokemon.name)
        self.assertEqual("PIKACHU_ALIAS", pokemon.name_alias)
        self.assertEqual("NORMAL", pokemon.form)
        self.assertEqual("NORMAL_ALIAS", pokemon.form_alias)
        self.assertEqual(25, pokemon.id)
        self.assertEqual([expected_type], pokemon.types)
        self.assertEqual(112, pokemon.base_attack)
        self.assertEqual(96, pokemon.base_defense)
        self.assertEqual(111, pokemon.base_stamina)
    
    def test_types_setter_as_pokemon_type(self):
        expected_types = PokemonType.FIRE
        dummy = Pokemon()
        dummy.types = expected_types
        self.assertEqual([expected_types], dummy.types)

    def test_types_setter_as_list_singular(self):
        expected_types = [PokemonType.FIRE]
        dummy = Pokemon()
        dummy.types = expected_types
        self.assertEqual(expected_types, dummy.types)

    def test_types_setter_as_list_plural(self):
        expected_types = [PokemonType.FIRE, PokemonType.WATER]
        dummy = Pokemon()
        dummy.types = expected_types
        self.assertEqual(expected_types, dummy.types)

    def test_types_setter_as_list_too_few_many_pokemon_types(self):
        dummy = Pokemon()

        with self.assertRaises(ValueError):
            dummy.types = []

        with self.assertRaises(ValueError):
            dummy.types = [PokemonType(i) for i in range(0, 3)]

    def test_types_setter_as_list_wrong_type(self):
        dummy = Pokemon()

        with self.assertRaises(TypeError):
            dummy.types = [PokemonType.FIRE, 1]

    def test_types_setter_wrong_type(self):
        dummy = Pokemon()

        with self.assertRaises(TypeError):
            dummy.types = 1

    def test_get_live_pokemon(self):
        pass
    
    def test_live_pokemon_cp(self):
        dialga = Pokemon(
            base_attack=275,
            base_defense=211,
            base_stamina=205,
            types=[PokemonType.DRAGON, PokemonType.STEEL]
        )
        live_dialga = dialga.get_live_pokemon(
            15, 15, 15, 51
        )
        self.assertEqual(4620, live_dialga.cp)
        self.assertAlmostEqual(245.14, live_dialga.attack*live_dialga._cpm, 2)
        self.assertAlmostEqual(191.04, live_dialga.defense*live_dialga._cpm, 2)
        self.assertEqual(185, int(live_dialga.stamina*live_dialga._cpm))
        self.assertAlmostEqual(8663.6, live_dialga.stat_product, 1)

    def test_live_pokemon_mew(self):
        mew = Pokemon(
            base_attack=210,
            base_defense=210,
            base_stamina=225,
            types=PokemonType.PSYCHIC
        )
        live_mew = mew.get_live_pokemon(
            8, 12, 2, 20.5
        )
        self.assertEqual(1790, live_mew.cp)
        self.assertAlmostEqual(131.85, live_mew.attack*live_mew._cpm, 2)
        self.assertAlmostEqual(134.27, live_mew.defense*live_mew._cpm, 2)
        self.assertEqual(137, int(live_mew.stamina*live_mew._cpm))
        self.assertAlmostEqual(2425.4, live_mew.stat_product, 1)

    def test_live_pokemon_ivs_wrong_types(self):
        iv_names = ["attack_iv", "defense_iv", "stamina_iv"]
        live_pokemon = LivePokemon()
        for iv in iv_names:
            with self.assertRaises(TypeError):
                setattr(live_pokemon, iv, "13")

    def test_live_pokemon_ivs_ranges(self):
        iv_names = ["attack_iv", "defense_iv", "stamina_iv"]
        live_pokemon = LivePokemon()
        for iv in iv_names:
            with self.assertRaises(ValueError):
                setattr(live_pokemon, iv, -1)
            with self.assertRaises(ValueError):
                setattr(live_pokemon, iv, 16)

    def test_live_pokemon_ivs(self):
        iv_names = ["attack_iv", "defense_iv", "stamina_iv"]
        live_pokemon = LivePokemon()
        for iv in iv_names:
            setattr(live_pokemon, iv, 13)

    def test_live_pokemon_attack(self):
        live_pokemon = LivePokemon(
            base_attack=150,
            attack_iv=15
        )

        self.assertEqual(165, live_pokemon.attack)

    def test_live_pokemon_scaled_attack(self):
        """
        Mock the CPMultiplier to 0.5, irrespective of level.
        """
        with patch(self.MODULE_PATH + ".CPMultiplier") as mocked_cpm:
            mocked_cpm.__getitem__.return_value = 0.5
            live_pokemon = LivePokemon(
                base_attack=150,
                attack_iv=15
            )
            self.assertEqual(82.5, live_pokemon.scaled_attack)

    def test_live_pokemon_defense(self):
        live_pokemon = LivePokemon(
            base_defense=150,
            defense_iv=15
        )

        self.assertEqual(165, live_pokemon.defense)

    def test_live_pokemon_scaled_defense(self):
        """
        Mock the CPMultiplier to 0.5, irrespective of level.
        """
        with patch(self.MODULE_PATH + ".CPMultiplier") as mocked_cpm:
            mocked_cpm.__getitem__.return_value = 0.5
            live_pokemon = LivePokemon(
                base_defense=150,
                defense_iv=15
            )
            self.assertEqual(82.5, live_pokemon.scaled_defense)

    def test_live_pokemon_stamina(self):
        live_pokemon = LivePokemon(
            base_stamina=150,
            stamina_iv=15
        )

        self.assertEqual(165, live_pokemon.stamina)

    def test_live_pokemon_scaled_stamina(self):
        """
        Mock the CPMultiplier to 0.5, irrespective of level.
        """
        with patch(self.MODULE_PATH + ".CPMultiplier") as mocked_cpm:
            mocked_cpm.__getitem__.return_value = 0.5
            live_pokemon = LivePokemon(
                base_stamina=150,
                stamina_iv=15
            )
            self.assertEqual(82.5, live_pokemon.scaled_stamina)

    def test_pokemon_level(self):
        # Mock CPMultiplier
        test_cpm = {
            4: 0.25,
            6: 0.5
        }
        with patch(self.MODULE_PATH + ".CPMultiplier") as mocked_cpm:
            mocked_cpm.__getitem__.side_effect = test_cpm.__getitem__
            live_pokemon = LivePokemon(level=4)
            self.assertEqual(0.25, live_pokemon._cpm)

            live_pokemon.level = 6
            self.assertEqual(0.5, live_pokemon._cpm)

    def test_stat_product(self):
        with patch(self.MODULE_PATH + ".CPMultiplier") as mocked_cpm:
            mocked_cpm.__getitem__.return_value = 0.5
            live_pokemon = LivePokemon(
                base_attack=4,
                attack_iv=1,
                base_defense=3,
                defense_iv=2,
                base_stamina=2,
                stamina_iv=3
            )
            self.assertEqual(0.0125, live_pokemon.stat_product)
