from unittest.mock import MagicMock, patch
from tests.unit.core import CoreUnitTest

from raiddex.core.tools.pvp_rank import PvPIVStat, PvPIVStats, PvPRankTool, get_iv_hex
from raiddex.core.models.pokemon.cp_multiplier import CPMultiplier
from raiddex.core.models.pokemon.pokemon import Pokemon, PokemonType

from tests.unit.core.models.pokemon import CPM_DATA


MODULE_PATH = PvPRankTool.__module__

class MockedPokemon:
    pass


class MockedLivePokemon:

    def __init__(
        self, pokemon, attack_iv, defense_iv, stamina_iv, level, cp, stat_product
    ):
        self.pokemon = pokemon
        self.attack_iv = attack_iv
        self.defense_iv = defense_iv
        self.stamina_iv = stamina_iv
        self.level = level
        self.cp = cp
        self.stat_product = stat_product

    @classmethod
    def get_default(cls):
        return cls(
            MockedPokemon(), 15, 15, 15, 51, 1234, 1234
        )


class TestPvPRankModule(CoreUnitTest):

    def test_get_iv_hex(self):
        hex_iv = get_iv_hex(11, 4, 15)
        self.assertEqual(0xB4F, hex_iv)

    def test_get_iv_hex_out_of_range(self):
        with self.assertRaises(ValueError):
            get_iv_hex(0, 7, 16)
        with self.assertRaises(ValueError):
            get_iv_hex(4, -1, 13)


class TestPvPIVStat(CoreUnitTest):
    
    def get_default_stativ(self):
        return PvPIVStat(MockedPokemon(), MockedLivePokemon.get_default())

    def test_equal(self):
        stat1 = self.get_default_stativ()
        stat2 = self.get_default_stativ()

        self.assertEqual(stat1, stat2)
        self.assertTrue(stat1 >= stat2)
        self.assertTrue(stat2 <= stat1)

    def test_equal_stat_product_unequal_cp(self):
        stat1 = self.get_default_stativ()
        stat1.optimal_cp = 123
        stat2 = self.get_default_stativ()
        stat2.optimal_cp = 122

        self.assertTrue(stat1 > stat2)
        self.assertTrue(stat1 >= stat2)
        self.assertTrue(stat2 < stat1)
        self.assertTrue(stat2 <= stat1)
        self.assertNotEqual(stat1, stat2)

    def test_iv_to_str(self):
        mocked_pokemon = MockedLivePokemon(
            None, 13, 11, 5, None, None, None
        )
        pvp_iv = PvPIVStat(mocked_pokemon.pokemon, mocked_pokemon)

        iv_str = pvp_iv.iv_to_str()

        self.assertEqual("(13/11/5)", iv_str)


class TestPvPIVStats(CoreUnitTest):
    
    def setUp(self) -> None:
        super().setUp()
        self._setup_default_pvp_ivs()

    def _setup_default_pvp_ivs(self):
        mocked_live_pokemon = MockedLivePokemon.get_default()
        self.pvp_iv1 = PvPIVStat(mocked_live_pokemon.pokemon, mocked_live_pokemon)
        self.pvp_iv1.attack_iv = 1
        self.pvp_iv1.defense_iv = 2
        self.pvp_iv1.stamina_iv = 3
        self.pvp_iv1.score = 4
        self.pvp_iv1.optimal_cp = 5
        self.pvp_iv2 = PvPIVStat(mocked_live_pokemon.pokemon, mocked_live_pokemon)
        self.pvp_iv2.attack_iv = 6
        self.pvp_iv2.defense_iv = 7
        self.pvp_iv2.stamina_iv = 8
        self.pvp_iv2.score = 9
        self.pvp_iv2.optimal_cp = 10
        self.pvp_iv3 = PvPIVStat(mocked_live_pokemon.pokemon, mocked_live_pokemon)
        self.pvp_iv3.attack_iv = 11
        self.pvp_iv3.defense_iv = 12
        self.pvp_iv3.stamina_iv = 13
        self.pvp_iv3.score = 14
        self.pvp_iv3.optimal_cp = 15

    def test_length(self):
        pvp_ivs = PvPIVStats()
        pvp_ivs.ranks.append(self.pvp_iv1)
        pvp_ivs.ranks.append(self.pvp_iv2)
        pvp_ivs.ranks.append(self.pvp_iv3)

        self.assertEqual(3, len(pvp_ivs))

    def test_sort_ranks(self):
        correct_order = [self.pvp_iv3, self.pvp_iv2, self.pvp_iv1]
        pvp_ivs = PvPIVStats()
        pvp_ivs.ranks.append(self.pvp_iv2)
        pvp_ivs.ranks.append(self.pvp_iv3)
        pvp_ivs.ranks.append(self.pvp_iv1)
        pvp_ivs.sort_ranks()
        self.assertListEqual(correct_order, pvp_ivs.ranks)

    def test_append(self):
        pvp_ivs = PvPIVStats()
        self.assertEqual(0, len(pvp_ivs.ranks))
        self.assertEqual(0, len(pvp_ivs.mapped_ranks))

        pvp_ivs.append(self.pvp_iv1)
        self.assertEqual(self.pvp_iv1, pvp_ivs.ranks[0])
        self.assertEqual(self.pvp_iv1, pvp_ivs.mapped_ranks[0x123])

    def test_get_top_rank(self):
        pvp_ivs = PvPIVStats()
        pvp_ivs.ranks.append(self.pvp_iv2)
        pvp_ivs.ranks.append(self.pvp_iv3)
        pvp_ivs.ranks.append(self.pvp_iv1)
        pvp_ivs.sort_ranks()

        top_rank = pvp_ivs.get_top_rank()
        self.assertEqual(self.pvp_iv3, top_rank)

    def test_get_ivs(self):
        pvp_ivs = PvPIVStats()
        pvp_ivs.append(self.pvp_iv2)

        rank = pvp_ivs.get_ivs(6, 7, 8)

        self.assertEqual(self.pvp_iv2, rank)


class TestPvPRankTool(CoreUnitTest):
    
    @classmethod
    def setUpClass(cls, load_data: bool = False):
        super().setUpClass(load_data)
        CPMultiplier.parse_from_data(CPM_DATA)

    def setUp(self) -> None:
        super().setUp()
        self.mocked_live_pokemon = MockedLivePokemon(
            MockedPokemon(),
            attack_iv=3,
            defense_iv=6,
            stamina_iv=9,
            level=22.5,
            cp=1499,
            stat_product=320
        )
        self.pvp_rank = PvPRankTool()
    
    def test_calculate_max_level(self):
        """
        Integration Test with CPMultiplier
        """
        self.mocked_live_pokemon.attack = 340
        self.mocked_live_pokemon.defense = 112
        self.mocked_live_pokemon.stamina =  220

        calculated_max_level = self.pvp_rank._calculate_max_level(
            self.mocked_live_pokemon, 1500, 51
        )

        self.assertEqual(15.5, calculated_max_level)

    def test_calculate_max_level_no_ceiling(self):
        max_level = 83
        
        calculated_max_level = self.pvp_rank._calculate_max_level(
            self.mocked_live_pokemon, -1, max_level
        )

        self.assertEqual(max_level, calculated_max_level)

    def test_calculate_ranks_top(self):
        """
        Integration test using both Pokemon and CPMultiplier
        """
        pokemon = Pokemon(base_attack=200, base_defense=200, base_stamina=200, types=PokemonType.NORMAL)

        ranks = self.pvp_rank.calculate_ranks(pokemon, 1500)
        top_rank = ranks.get_top_rank()
        self.assertEqual(1500, top_rank.optimal_cp)
        self.assertEqual(1, top_rank.attack_iv)
        self.assertEqual(15, top_rank.defense_iv)
        self.assertEqual(14, top_rank.stamina_iv)
        self.assertAlmostEqual(1894.71, top_rank.score, delta=0.01)
        self.assertAlmostEqual(100.0, top_rank.score_percent, delta=0.1)
