from unittest.mock import patch

from tests.unit import UnitTest

from raiddex.utils.op_env import (
    OperatingEnvironment, RAIDDEX_OP_ENV_VAR_NAME, get_operating_environment
)


class TestOpEnv(UnitTest):

    MODULE_PATH = OperatingEnvironment.__module__

    @classmethod
    def setUpClass(cls, load_data: bool = False):
        super().setUpClass(load_data)

        cls.patched_env_vars = patch(
            cls.MODULE_PATH + ".os.environ",
            new=dict()
        )
    
    def setUp(self) -> None:
        super().setUp()
        self.mocked_env_vars = self.patched_env_vars.start()
    
    def tearDown(self) -> None:
        self.patched_env_vars.stop()
        super().tearDown()

    def _set_mocked_op_env(self, op_env: str) -> None:
        self.mocked_env_vars[RAIDDEX_OP_ENV_VAR_NAME] = op_env

    # This is a bit verbose, but lets make sure OpEnv can be determined for all scenarios.

    def test_get_operating_environment_development(self):
        self._set_mocked_op_env("DEVELOPMENT")
        op_env = get_operating_environment()
        self.assertEqual(OperatingEnvironment.DEVELOPMENT, op_env)

    def test_get_operating_environment_testing(self):
        self._set_mocked_op_env("TESTING")
        op_env = get_operating_environment()
        self.assertEqual(OperatingEnvironment.TESTING, op_env)

    def test_get_operating_environment_gitlab(self):
        self._set_mocked_op_env("GITLAB_CI")
        op_env = get_operating_environment()
        self.assertEqual(OperatingEnvironment.GITLAB_CI, op_env)

    def test_get_operating_environment_production(self):
        self._set_mocked_op_env("PRODUCTION")
        op_env = get_operating_environment()
        self.assertEqual(OperatingEnvironment.PRODUCTION, op_env)

    def test_get_operating_environment_unknown_environment(self):
        # Suppress printing to STDOUT for this test.
        with patch(self.MODULE_PATH + ".print"):
            self._set_mocked_op_env("UNKNOWN")
            op_env = get_operating_environment()
            self.assertEqual(OperatingEnvironment.DEVELOPMENT, op_env)

    def test_get_operating_environment_missing_environment(self):
        # Suppress printing to STDOUT for this test.
        with patch(self.MODULE_PATH + ".print"):
            self._set_mocked_op_env("UNKNOWN")
            op_env = get_operating_environment()
            self.assertEqual(OperatingEnvironment.DEVELOPMENT, op_env)
