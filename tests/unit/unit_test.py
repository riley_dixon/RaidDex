from tests import BaseTest

class UnitTest(BaseTest):
    
    @classmethod
    def setUpClass(cls, load_data: bool = False):
        super().setUpClass(load_data)
