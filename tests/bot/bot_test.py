from raiddex.db.connection_config import get_conn_config
from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager
from raiddex.core.parser.parser import Parser

from tests import BaseTest


class BotTest(BaseTest):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        parser = Parser()
        parser.parse_all_data()
        DBPool.create_pools()
        cls.db_manager = DBManager()
        cls.db_manager.initialize_db(parser)

        conn_config = get_conn_config()
        cls.raiddex_db = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()

    @classmethod
    def tearDownClass(cls) -> None:
        DBPool.raiddex_pool.close()
        cls.db_manager._drop_database(cls.raiddex_db)
        cls.db_manager._drop_user(cls.raiddex_user)
        DBPool.close_pools()
        super().tearDownClass()
