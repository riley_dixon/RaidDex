from unittest.mock import MagicMock, patch

from tests.bot.transformers import MockedChoice, TransformerTest

from raiddex.bot.transformers.pokemon import PokemonTransformer

class TestPokemonNameTransformer(TransformerTest):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(PokemonTransformer)

    def setUp(self) -> None:
        super().setUp()
        self.interaction = MagicMock()

    def test_autocomplete_pokemon_names(self):
        with patch("raiddex.bot.autocomplete.pokemon_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("hit")
            
            self.assertEqual(3, len(suggestions))
            names = [choice.name for choice in suggestions]
            self.assertIn("Hitmonchan", names)
            self.assertIn("Hitmonlee", names)
            self.assertIn("Hitmontop", names)

    def test_autocomplete_short_name(self):
         with patch("raiddex.bot.autocomplete.pokemon_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("me")
            
            self.assertEqual(0, len(suggestions))
