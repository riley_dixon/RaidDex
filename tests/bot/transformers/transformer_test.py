import asyncio

from unittest.mock import MagicMock

from discord.app_commands import Transformer

from tests.bot import BotTest

class MockedChoice(MagicMock):

    def __init__(self, name, value):
        super().__init__()
        self.name = name
        self.value = value


class TransformerTest(BotTest):

    @classmethod
    def setUpClass(cls, tested_cls: type[Transformer]):
        """
        tested_cls - The Transformer class under test.
        """
        super().setUpClass()
        cls.tested_cls = tested_cls

    def run_autocomplete(self, current_value):
        """Hide the asyncio call, mock out the `discord.Interaction` object."""
        result = asyncio.run(
            self.tested_cls().autocomplete(self.interaction, current_value)
        )
        return result