from unittest.mock import MagicMock, patch

from tests.bot.transformers import MockedChoice, TransformerTest

from raiddex.bot.transformers.pvp_league import PvPLeagueTransformer

class TestPvPLeagueTransformer(TransformerTest):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(PvPLeagueTransformer)

    def setUp(self) -> None:
        super().setUp()
        self.interaction = MagicMock()

    def test_autocomplete_little(self):
        with patch("raiddex.bot.autocomplete.pvp_league_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("l")

            self.assertEqual(1, len(suggestions))
            suggestion = suggestions[0]
            self.assertEqual("Little", suggestion.name)
            self.assertEqual("LITTLE", suggestion.value)

    def test_autocomplete_great(self):
        with patch("raiddex.bot.autocomplete.pvp_league_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("g")

            self.assertEqual(1, len(suggestions))
            suggestion = suggestions[0]
            self.assertEqual("Great", suggestion.name)
            self.assertEqual("GREAT", suggestion.value)

    def test_autocomplete_ultra(self):
        with patch("raiddex.bot.autocomplete.pvp_league_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("u")

            self.assertEqual(1, len(suggestions))
            suggestion = suggestions[0]
            self.assertEqual("Ultra", suggestion.name)
            self.assertEqual("ULTRA", suggestion.value)

    def test_autocomplete_master(self):
        with patch("raiddex.bot.autocomplete.pvp_league_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("m")

            self.assertEqual(1, len(suggestions))
            suggestion = suggestions[0]
            self.assertEqual("Master", suggestion.name)
            self.assertEqual("MASTER", suggestion.value)

    def test_autocomplete_empty(self):
        with patch("raiddex.bot.autocomplete.pvp_league_name.Choice", new=MockedChoice):
            suggestions = self.run_autocomplete("")

            self.assertEqual(4, len(suggestions))
            names = [suggestion.name for suggestion in suggestions]
            self.assertIn("Little", names)
            self.assertIn("Great", names)
            self.assertIn("Ultra", names)
            self.assertIn("Master", names)
