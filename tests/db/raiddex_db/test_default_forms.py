from unittest.mock import MagicMock
from psycopg import sql
from psycopg.errors import ForeignKeyViolation
from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.models.types.types import PokemonType

from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager
from raiddex.db.connection_config import get_conn_config
from raiddex.db.raiddex_db.tables.default_forms import DefaultFormsTable
from raiddex.db.raiddex_db.tables.pokemons import PokemonsTable

from tests.db import DBTest
from tests.db.mocked_conn_pool import MockedConnectionPool


class TestPokemonsDBTable(DBTest):

    MODULE_PATH = DefaultFormsTable.__module__
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=True)
        DBPool.create_pools()
        DBPool.superuser_pool.open()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.raiddex_db_name = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()
        cls.raiddex_pw = conn_config.get_raiddex_user_password()

        cls.db_manager = DBManager()
        cls.mocked_db_pool = MockedConnectionPool(
            cls.raiddex_user, cls.raiddex_pw, cls.db_address, cls.db_port, cls.raiddex_db_name
        )
        cls.db_manager._create_user(cls.raiddex_user, cls.raiddex_pw)
        cls.db_manager._create_database(cls.raiddex_db_name)
        cls.db_manager._grant_db_access(cls.raiddex_db_name, cls.raiddex_user)

        cls.addClassCleanup(DBPool.close_pools)
        cls.addClassCleanup(cls.db_manager._drop_user, cls.raiddex_user)
        cls.addClassCleanup(cls.db_manager._drop_database, cls.raiddex_db_name)

        cls.mocked_db_pool.open()
        cls.pokemons_table = PokemonsTable(cls.mocked_db_pool)
        cls.default_forms_table = DefaultFormsTable(cls.mocked_db_pool)
        with DBPool.get_su_conn(cls.raiddex_db_name) as su_conn:
            su_conn.execute(cls.pokemons_table.create_table_sql())
            su_conn.execute(cls.default_forms_table.create_table_sql())
            su_conn.execute(sql.SQL("GRANT SELECT, INSERT, UPDATE ON pokemons, default_forms TO raiddex"))
    
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mocked_db_pool.close()
        super().tearDownClass()
    
    def setUp(self) -> None:
        self.mocked_db_pool.conn.transaction()
    
    def tearDown(self) -> None:
        self.mocked_db_pool.conn.rollback_conn()

    def test_columns(self):
        """
        Sanity check that PostgreSQL created our table schema correctly.
        """
        expected_columns = [column.name for column in self.default_forms_table.columns]
        actual_columns = self.default_forms_table.list_columns()
        self.assertColumnsEqual(expected_columns, actual_columns)
    
    def _get_test_pokemon(self) -> Pokemon:
        test_pokemon = Pokemon()
        test_pokemon.name = "TEST_1"
        test_pokemon.name_alias = None
        test_pokemon.form = "NORMAL"
        test_pokemon.form_alias = None
        test_pokemon.id = 0
        test_pokemon.types = [PokemonType.FIGHTING, PokemonType.NORMAL]
        test_pokemon.base_attack = 1
        test_pokemon.base_defense = 2
        test_pokemon.base_stamina = 3

        return test_pokemon
    
    def test_add_default_form(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon)

        self.default_forms_table.add_default_form(test_pokemon.name, test_pokemon.form)

        with self.mocked_db_pool.connection() as db_conn:
            query = sql.SQL("""
                SELECT {table_name}.name, {table_name}.form FROM {table_name}
            """).format(
                table_name=sql.Identifier(self.default_forms_table.name)
            )

            default_form = db_conn.execute(query).fetchall()
        
        self.assertEqual(1, len(default_form))
        default_form = default_form[0]
        self.assertEqual(test_pokemon.name, default_form[0])
        self.assertEqual(test_pokemon.form, default_form[1])

    def test_add_default_form_bad_pokemon_name(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon)

        with self.assertRaises(ForeignKeyViolation):
            # Should we instead use our own kind of Exception?
            self.default_forms_table.add_default_form("BAD_NAME", test_pokemon.form)

    def test_add_default_form_bad_pokemon_form(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon)

        with self.assertRaises(ForeignKeyViolation):
            # Should we instead use our own kind of Exception?
            self.default_forms_table.add_default_form(test_pokemon.name, "BAD_FORM")

    def test_get_default_form(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon)

        with self.mocked_db_pool.connection() as db_conn:
            query = sql.SQL("""
                INSERT INTO default_forms (name, form)
                VALUES (%(name)s, %(form)s)
            """)
            test_params = {
                "name": test_pokemon.name,
                "form": test_pokemon.form
            }
            db_conn.execute(query, test_params)
        default_form = self.default_forms_table.get_default_form(test_pokemon.name)

        self.assertEqual(test_pokemon.form, default_form)

    def test_get_pokemon_with_default_form(self):
        """
        Sanity Test get_pokemon with a Default Form rather than mocking out the default_form.
        """
        test_pokemon = self._get_test_pokemon()
        test_pokemon.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon)

        default_pokemon = self.pokemons_table.get_pokemon(test_pokemon.name, "")
        self.assertIsNone(default_pokemon)

        self.default_forms_table.add_default_form(test_pokemon.name, test_pokemon.form)

        default_pokemon = self.pokemons_table.get_pokemon(test_pokemon.name, "")
        self.assertEqual(test_pokemon.form, default_pokemon.form)

    def test_add_default_forms(self):
        test_pokemon1 = self._get_test_pokemon()
        test_pokemon1.form = "CUSTOM_FORM"
        self.pokemons_table.add_pokemon(test_pokemon1)

        test_pokemon2 = self._get_test_pokemon()
        test_pokemon2.name = "TEST_2"
        test_pokemon2.form = "CUSTOM_FORM_2"
        self.pokemons_table.add_pokemon(test_pokemon2)

        default_forms = {
            test_pokemon1.name: test_pokemon1.form,
            test_pokemon2.name: test_pokemon2.form
        }
        self.default_forms_table.add_default_forms(default_forms)

        stored_default_form_1 = self.default_forms_table.get_default_form(test_pokemon1.name)
        stored_default_form_2 = self.default_forms_table.get_default_form(test_pokemon2.name)

        self.assertEqual(test_pokemon1.form, stored_default_form_1)
        self.assertEqual(test_pokemon2.form, stored_default_form_2)

    def test_add_missing_default_forms(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2.form = "NOT_NORMAL"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        parsed_default_forms = {
            test_pokemon_1.name: test_pokemon_1.form,
            test_pokemon_2.name: test_pokemon_2.form
        }

        self.default_forms_table.add_default_form(test_pokemon_1.name, test_pokemon_1.form)

        current_default_forms = self.default_forms_table._get_all_default_forms()
        self.assertEqual(1, len(current_default_forms))
        self.assertEqual(test_pokemon_1.form, current_default_forms[test_pokemon_1.name])

        missing_default_forms = self.default_forms_table._add_missing_default_forms(
            parsed_default_forms, current_default_forms
        )
        self.assertEqual(test_pokemon_2.form, missing_default_forms[test_pokemon_2.name])

        current_default_forms = self.default_forms_table._get_all_default_forms()
        for name, form in parsed_default_forms.items():
            self.assertEqual(form, current_default_forms[name])

    def test_update_default_form(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NEW_DEFAULT"
        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        self.default_forms_table.add_default_form(test_pokemon_1.name, test_pokemon_1.form)

        self.default_forms_table.update_default_forms({test_pokemon_1.name: test_pokemon_2.form})

        db_default_form  = self.default_forms_table._get_all_default_forms()
        self.assertEqual(test_pokemon_2.form, db_default_form[test_pokemon_1.name])

    def test_update_existing_default_forms(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NOT_NORMAL"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        
        self.default_forms_table.add_default_form(test_pokemon_1.name, test_pokemon_1.form)

        parsed_default_forms = {
            test_pokemon_1.name: test_pokemon_2.form
        }
        current_default_forms = self.default_forms_table._get_all_default_forms()
        updated_default_forms = self.default_forms_table._update_existing_default_forms(
            parsed_default_forms, current_default_forms
        )
        self.assertEqual(test_pokemon_2.form, updated_default_forms[test_pokemon_1.name])

        current_default_forms = self.default_forms_table._get_all_default_forms()
        for name, form in parsed_default_forms.items():
            self.assertEqual(form, current_default_forms[name])

    def test_update_table(self):
        """
        Seed initial data, and then:
        1) Add a missing default form entry.
        2) Update a default form to something else.
        """
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NOT_NORMAL"
        all_pokemon = [test_pokemon_1, test_pokemon_2]
        test_pokemon_3 = self._get_test_pokemon()
        test_pokemon_3.name = "OTHER_POKEMON"
        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        self.pokemons_table.add_pokemon(test_pokemon_3)
        self.default_forms_table.add_default_form(test_pokemon_1.name, test_pokemon_1.form)

        parsed_default_forms = {
            test_pokemon_1.name: test_pokemon_2.form,
            test_pokemon_3.name: test_pokemon_3.form
        }
        
        current_default_forms = self.default_forms_table._get_all_default_forms()
        
        mocked_parser = MagicMock()
        mocked_parser.custom_default_forms = parsed_default_forms
        self.default_forms_table.update_table(mocked_parser)

        current_default_forms = self.default_forms_table._get_all_default_forms()
        for name, form in parsed_default_forms.items():
            self.assertEqual(form, current_default_forms[name])
