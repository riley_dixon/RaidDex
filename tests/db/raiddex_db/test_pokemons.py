from unittest.mock import patch, MagicMock

from psycopg import sql

from raiddex.core.models.pokemon.pokemon import Pokemon
from raiddex.core.models.types.types import PokemonType
from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager
from raiddex.db.connection_config import get_conn_config
from raiddex.db.raiddex_db.tables.pokemons import PokemonsTable

from tests.db import DBTest
from tests.db.mocked_conn_pool import MockedConnectionPool


class TestPokemonsDBTable(DBTest):

    MODULE_PATH = PokemonsTable.__module__
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=True)
        DBPool.create_pools()
        DBPool.superuser_pool.open()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.raiddex_db_name = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()
        cls.raiddex_pw = conn_config.get_raiddex_user_password()

        cls.db_manager = DBManager()
        cls.mocked_db_pool = MockedConnectionPool(
            cls.raiddex_user, cls.raiddex_pw, cls.db_address, cls.db_port, cls.raiddex_db_name
        )
        cls.db_manager._create_user(cls.raiddex_user, cls.raiddex_pw)
        cls.db_manager._create_database(cls.raiddex_db_name)
        cls.db_manager._grant_db_access(cls.raiddex_db_name, cls.raiddex_user)

        cls.addClassCleanup(DBPool.close_pools)
        cls.addClassCleanup(cls.db_manager._drop_user, cls.raiddex_user)
        cls.addClassCleanup(cls.db_manager._drop_database, cls.raiddex_db_name)

        cls.mocked_db_pool.open()
        cls.pokemons_table = PokemonsTable(cls.mocked_db_pool)
        create_table_cmd = cls.pokemons_table.create_table_sql()
        with DBPool.get_su_conn(cls.raiddex_db_name) as su_conn:
            su_conn.execute(create_table_cmd)
            su_conn.execute(sql.SQL("GRANT SELECT, INSERT, UPDATE ON pokemons TO raiddex"))
    
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mocked_db_pool.close()
        super().tearDownClass()
    
    def setUp(self) -> None:
        self.mocked_db_pool.conn.transaction()
    
    def tearDown(self) -> None:
        self.mocked_db_pool.conn.rollback_conn()

    def _get_test_pokemon(self) -> Pokemon:
        test_pokemon = Pokemon()
        test_pokemon.name = "TEST_1"
        test_pokemon.name_alias = None
        test_pokemon.form = "NORMAL"
        test_pokemon.form_alias = None
        test_pokemon.id = 0
        test_pokemon.types = [PokemonType.FIGHTING, PokemonType.NORMAL]
        test_pokemon.base_attack = 1
        test_pokemon.base_defense = 2
        test_pokemon.base_stamina = 3

        return test_pokemon
    
    def test_columns(self):
        """
        Sanity check that PostgreSQL created our table schema correctly.
        """
        expected_columns = [column.name for column in self.pokemons_table.columns]
        actual_columns = self.pokemons_table.list_columns()
        self.assertColumnsEqual(expected_columns, actual_columns)

    def test_add_pokemon(self):
        test_pokemon = self._get_test_pokemon()

        self.pokemons_table.add_pokemon(test_pokemon)

        with self.pokemons_table.db_pool.connection() as conn:
            query = sql.SQL("""
                SELECT pokemons.name, pokemons.name_alias, pokemons.form, pokemons.form_alias, pokemons.id, pokemons.type1, pokemons.type2, pokemons.base_attack, pokemons.base_defense, pokemons.base_stamina
                FROM pokemons
                WHERE name = {pokemon_name} AND
                    form = {pokemon_form}
            """).format(
                pokemon_name=sql.Literal(test_pokemon.name), pokemon_form=sql.Literal(test_pokemon.form)
            )
            result = conn.execute(query).fetchone()
        
        self.assertEqual(test_pokemon.name, result[0])
        self.assertEqual(test_pokemon.name_alias, result[1])
        self.assertEqual(test_pokemon.form, result[2])
        self.assertEqual(test_pokemon.form_alias, result[3])
        self.assertEqual(test_pokemon.id, result[4])
        self.assertEqual(test_pokemon.types[0].name, result[5])
        self.assertEqual(test_pokemon.types[1].name, result[6])
        self.assertEqual(test_pokemon.base_attack, result[7])
        self.assertEqual(test_pokemon.base_defense, result[8])
        self.assertEqual(test_pokemon.base_stamina, result[9])

    def test_get_pokemon(self):
        test_attrs = {
            "name": "TEST_1",
            "name_alias": "Alias.Test1",
            "form": "NORMAL",
            "form_alias": "Alias.Normal",
            "id": 1,
            "type1": "WATER",
            "type2": "FIRE",
            "base_attack": 1,
            "base_defense": 2,
            "base_stamina": 3
        }

        with self.pokemons_table.db_pool.connection() as conn:
            query = sql.SQL("""
                INSERT INTO pokemons (name, name_alias, form, form_alias, id, type1, type2, base_attack, base_defense, base_stamina)
                VALUES (%(name)s, %(name_alias)s, %(form)s, %(form_alias)s, %(id)s, %(type1)s, %(type2)s, %(base_attack)s, %(base_defense)s, %(base_stamina)s)
            """)
            conn.execute(query, test_attrs)
        
        with patch(self.MODULE_PATH + ".DefaultFormsTable") as mocked_default_form:
            mocked_default_form().get_default_form.return_value = None
            stored_pokemon = self.pokemons_table.get_pokemon(test_attrs["name"], test_attrs["form"])

        self.assertEqual(test_attrs["name"], stored_pokemon.name)
        self.assertEqual(test_attrs["form"], stored_pokemon.form)
        self.assertEqual(test_attrs["id"], stored_pokemon.id)
        self.assertEqual(PokemonType[test_attrs["type1"]], stored_pokemon.types[0])
        self.assertEqual(PokemonType[test_attrs["type2"]], stored_pokemon.types[1])
        self.assertEqual(2, len(stored_pokemon.types))
        self.assertEqual(test_attrs["base_attack"], stored_pokemon.base_attack)
        self.assertEqual(test_attrs["base_defense"], stored_pokemon.base_defense)
        self.assertEqual(test_attrs["base_stamina"], stored_pokemon.base_stamina)

    def test_get_pokemon_bad_name(self):
        with patch(self.MODULE_PATH + ".DefaultFormsTable") as mocked_default_form:
            mocked_default_form().get_default_form.return_value = None
            pokemon = self.pokemons_table.get_pokemon("PIKACHU", "NORMAL")
        self.assertIsNone(pokemon)

    def test_get_pokemon_on_name_alias(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.name = "MR_MIME"
        test_pokemon.name_alias = "Mr. Mime"
        self.pokemons_table.add_pokemon(test_pokemon)

        with patch(self.MODULE_PATH + ".DefaultFormsTable") as mocked_default_form:
            mocked_default_form().get_default_form.return_value = None
            pokemon = self.pokemons_table.get_pokemon(test_pokemon.name_alias, test_pokemon.form)
        
        self.assertEqual("MR_MIME", pokemon.name)

    def test_get_pokemon_on_form_alias(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.name = "Lugia"
        test_pokemon.form = "S"
        test_pokemon.form_alias = "Apex"
        self.pokemons_table.add_pokemon(test_pokemon)

        pokemon = self.pokemons_table.get_pokemon(test_pokemon.name, test_pokemon.form_alias)
        
        self.assertEqual("Apex", pokemon.form_alias)

    def test_get_pokemon_on_name_and_form_alias(self):
        test_pokemon = self._get_test_pokemon()
        test_pokemon.name = "HO_OH"
        test_pokemon.name_alias = "Ho-oh"
        test_pokemon.form = "S"
        test_pokemon.form_alias = "Apex"
        self.pokemons_table.add_pokemon(test_pokemon)

        pokemon = self.pokemons_table.get_pokemon(test_pokemon.name_alias, test_pokemon.form_alias)
        
        self.assertEqual("Ho-oh", pokemon.name_alias)
        self.assertEqual("Apex", pokemon.form_alias)

    def test_search_pokemon_name(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        names = self.pokemons_table.search_pokemon_name(pokemon_name="TEST_1", num_results=10)
        self.assertEqual(1, len(names))
        self.assertEqual(test_pokemon_1.name, names[0].name)
    
    def test_search_pokemon_name_many_matches(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1_special = self._get_test_pokemon()
        test_pokemon_1_special.form = "SPECIAL"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_1_special)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        names = self.pokemons_table.search_pokemon_name(pokemon_name="Test", num_results=10)
        self.assertEqual(2, len(names))
        self.assertEqual(test_pokemon_1.name, names[0].name)
        self.assertEqual(test_pokemon_2.name, names[1].name)

    def test_search_pokemon_name_bad_name(self):
        test_pokemon_1 = self._get_test_pokemon()
        self.pokemons_table.add_pokemon(test_pokemon_1)

        results = self.pokemons_table.search_pokemon_name(pokemon_name="Billy")
        self.assertEqual([], results)

    def test_search_pokemon_by_alias(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1.name_alias = "alias1"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2.name_alias = "notanalias"
        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        results = self.pokemons_table.search_pokemon_name(pokemon_name="alias")
        self.assertEqual(1, len(results))
        self.assertEqual("alias1", results[0].alias)

    def test_search_pokemon_by_alias_with_matching_name(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1.name = "MR_MIME"
        test_pokemon_1.name_alias = "Mr. Mime"
        self.pokemons_table.add_pokemon(test_pokemon_1)

        results = self.pokemons_table.search_pokemon_name("MR")

        self.assertEqual(1, len(results))
        self.assertEqual("Mr. Mime", results[0].alias)

    def test_search_pokemon_form(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2.form = "SPECIAL"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        forms = self.pokemons_table.search_pokemon_form(pokemon_form="NORMAL", num_results=10)
        self.assertEqual(1, len(forms))
        self.assertEqual(test_pokemon_1.form, forms[0].form)

    def test_search_pokemon_forms_many_matches(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1_special = self._get_test_pokemon()
        test_pokemon_1_special.form = "NORMAL_COSTUME"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2_shadow = self._get_test_pokemon()
        test_pokemon_2_shadow.name = "TEST_2"
        test_pokemon_2_shadow.form = "NORMAL_SHADOW"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_1_special)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        self.pokemons_table.add_pokemon(test_pokemon_2_shadow)

        forms = self.pokemons_table.search_pokemon_form(pokemon_form="NORMAL", num_results=10)
        self.assertEqual(3, len(forms))
        self.assertEqual(test_pokemon_1.form, forms[0].form)
        self.assertEqual(test_pokemon_1_special.form, forms[1].form)
        self.assertEqual(test_pokemon_2_shadow.form, forms[2].form)

    def test_search_pokemon_form_with_name(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1_special = self._get_test_pokemon()
        test_pokemon_1_special.form = "NORMAL_COSTUME"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2_shadow = self._get_test_pokemon()
        test_pokemon_2_shadow.name = "TEST_2"
        test_pokemon_2_shadow.form = "NORMAL_SHADOW"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_1_special)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        self.pokemons_table.add_pokemon(test_pokemon_2_shadow)

        forms = self.pokemons_table.search_pokemon_form(
            pokemon_form="NORMAL", pokemon_name="TEST_2", num_results=10
        )
        self.assertEqual(2, len(forms))
        self.assertEqual(test_pokemon_2.form, forms[0].form)
        self.assertEqual(test_pokemon_2_shadow.form, forms[1].form)

    def test_search_pokemon_form_get_all_of_matching_species(self):
        """
        Get all of the forms of all pokemon that match a "name".
        """
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1_special = self._get_test_pokemon()
        test_pokemon_1_special.form = "NORMAL_COSTUME"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "TEST_2"
        test_pokemon_2_shadow = self._get_test_pokemon()
        test_pokemon_2_shadow.name = "TEST_2"
        test_pokemon_2_shadow.form = "NORMAL_SHADOW"
        test_pokemon_3 = self._get_test_pokemon()
        test_pokemon_3.name = "OTHER_3"
        test_pokemon_3.form = "NORMAL_OTHER"

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_1_special)
        self.pokemons_table.add_pokemon(test_pokemon_2)
        self.pokemons_table.add_pokemon(test_pokemon_2_shadow)

        forms = self.pokemons_table.search_pokemon_form(
            pokemon_form="", pokemon_name="TEST", num_results=10
        )
        forms = [form.form for form in forms]
        self.assertEqual(3, len(forms))
        self.assertIn(test_pokemon_1.form, forms)
        self.assertIn(test_pokemon_1_special.form, forms)
        self.assertIn(test_pokemon_2_shadow.form, forms)

    def test_search_pokemon_bad_form(self):
        test_pokemon_1 = self._get_test_pokemon()
        self.pokemons_table.add_pokemon(test_pokemon_1)

        results = self.pokemons_table.search_pokemon_form(pokemon_form="SPECIAL")
        self.assertEqual([], results)

    def test_search_pokemon_form_with_name_alias(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1.name = "Test1"
        test_pokemon_1.name_alias = "Alias1"
        test_pokemon_1.form = "MyForm"
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.name = "Test2"
        test_pokemon_2.name_alias = "NotAlias1"
        test_pokemon_2.form = "My2ndForm"
        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        results = self.pokemons_table.search_pokemon_form(
            pokemon_form="", pokemon_name="alias1"
        )

        self.assertEqual(1, len(results))
        self.assertEqual("MyForm", results[0].form)
    
    def test_search_pokemon_form_with_form_alias(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1.name = "Lugia"
        test_pokemon_1.form = "S"
        test_pokemon_1.form_alias = "Apex"
        self.pokemons_table.add_pokemon(test_pokemon_1)

        results = self.pokemons_table.search_pokemon_form(
            pokemon_form="apex", pokemon_name="lugia"
        )
        self.assertEqual(1, len(results))
        self.assertEqual("S", results[0].form)
        self.assertEqual("Apex", results[0].alias)
    
    def test_search_pokemon_form_with_form_and_name_alias(self):
        """
        This actually exists in our real data.
        """
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_1.name = "HO_OH"
        test_pokemon_1.name_alias = "Ho-oh"
        test_pokemon_1.form = "S"
        test_pokemon_1.form_alias = "Apex"
        self.pokemons_table.add_pokemon(test_pokemon_1)

        results = self.pokemons_table.search_pokemon_form(
            pokemon_form="apex", pokemon_name="ho-oh"
        )
        self.assertEqual(1, len(results))
        self.assertEqual("S", results[0].form)
        self.assertEqual("Apex", results[0].alias)

    def test_add_missing_pokemon(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NOT_NORMAL"
        all_pokemon = [test_pokemon_1, test_pokemon_2]

        self.pokemons_table.add_pokemon(test_pokemon_1)

        current_pokemon = self.pokemons_table._get_all_pokemon()
        self.assertEqual(1, len(current_pokemon))
        self.assertIn(test_pokemon_1, current_pokemon)
        self.assertNotIn(test_pokemon_2, current_pokemon)

        parsed_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in all_pokemon
        }
        current_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in current_pokemon
        }
        missing_pokemon = self.pokemons_table._add_missing_pokemon(
            parsed_pokemon, current_pokemon
        )

        self.assertIn(test_pokemon_2, missing_pokemon)
        current_pokemon = self.pokemons_table._get_all_pokemon()
        for pokemon in all_pokemon:
            self.assertIn(pokemon, current_pokemon)

    def test_update_pokemon(self):
        test_pokemon_1 = self._get_test_pokemon()
        self.pokemons_table.add_pokemon(test_pokemon_1)

        test_pokemon_1.base_attack = 876
        test_pokemon_1.base_defense = 34
        test_pokemon_1.base_stamina = 257
        test_pokemon_1.types = [PokemonType.FIRE, PokemonType.FLYING]
        self.pokemons_table.update_pokemons([test_pokemon_1])

        db_pokemon = self.pokemons_table._get_all_pokemon()[0]
        self.assertEqual(test_pokemon_1, db_pokemon)

    def test_update_existing_pokemon(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NOT_NORMAL"
        all_pokemon = [test_pokemon_1, test_pokemon_2]

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_2)

        test_pokemon_2.base_attack += 50
        test_pokemon_2.types = [PokemonType.FAIRY, PokemonType.FIGHTING]

        parsed_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in all_pokemon
        }
        current_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in self.pokemons_table._get_all_pokemon()
        }
        updated_pokemon = self.pokemons_table._update_existing_pokemon(
            parsed_pokemon, current_pokemon
        )

        self.assertIn(test_pokemon_2, updated_pokemon)
        current_pokemon = self.pokemons_table._get_all_pokemon()
        for pokemon in all_pokemon:
            self.assertIn(pokemon, current_pokemon)

    def test_update_table(self):
        test_pokemon_1 = self._get_test_pokemon()
        test_pokemon_2 = self._get_test_pokemon()
        test_pokemon_2.form = "NOT_NORMAL"
        all_pokemon = [test_pokemon_1, test_pokemon_2]
        test_pokemon_3 = self._get_test_pokemon()
        test_pokemon_3.name = "OTHER_POKEMON"
        all_pokemon = [test_pokemon_1, test_pokemon_2, test_pokemon_3]

        self.pokemons_table.add_pokemon(test_pokemon_1)
        self.pokemons_table.add_pokemon(test_pokemon_3)

        test_pokemon_3.base_defense += 1

        parsed_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in all_pokemon
        }
        current_pokemon = {
            (pokemon.name, pokemon.form): pokemon
            for pokemon in self.pokemons_table._get_all_pokemon()
        }
        mocked_parser = MagicMock()
        mocked_parser.pokemons.values.return_value = all_pokemon
        self.pokemons_table.update_table(mocked_parser)

        current_pokemon = self.pokemons_table._get_all_pokemon()
        for pokemon in all_pokemon:
            self.assertIn(pokemon, current_pokemon)
