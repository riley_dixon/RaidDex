from unittest.mock import patch
from psycopg import sql

from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column
from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager
from raiddex.db.connection_config import get_conn_config
from raiddex.db.raiddex_db.raiddex_db import RaidDexDB

from tests.db import DBTest, MockedParser
from tests.db.mocked_conn_pool import MockedConnectionPool


class DummyTable(AbstractTable):
    name = "dummy_table"
    columns = [
        Column("col1", "varchar(20)")
    ]
    table_attributes = ""

class TestRaidDexDB(DBTest):
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=True)
        DBPool.create_pools()
        DBPool.superuser_pool.open()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.raiddex_db_name = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()
        cls.raiddex_pw = conn_config.get_raiddex_user_password()

        cls.db_manager = DBManager()
        cls.mocked_db_pool = MockedConnectionPool(
            cls.raiddex_user, cls.raiddex_pw, cls.db_address, cls.db_port, cls.raiddex_db_name
        )
        cls.db_manager._create_user(cls.raiddex_user, cls.raiddex_pw)
        cls.db_manager._create_database(cls.raiddex_db_name)
        cls.db_manager._grant_db_access(cls.raiddex_db_name, cls.raiddex_user)

        cls.addClassCleanup(DBPool.close_pools)
        cls.addClassCleanup(cls.db_manager._drop_user, cls.raiddex_user)
        cls.addClassCleanup(cls.db_manager._drop_database, cls.raiddex_db_name)

        cls.mocked_db_pool.open()
        cls.raiddex_db = RaidDexDB(cls.mocked_db_pool)
    
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mocked_db_pool.close()
        super().tearDownClass()
    
    def setUp(self) -> None:
        self.raiddex_db = RaidDexDB(db_pool=self.mocked_db_pool)
        self.mocked_db_pool.conn.transaction()
    
    def tearDown(self) -> None:
        self.mocked_db_pool.conn.rollback_conn()

    def _drop_table(self, table: AbstractTable):
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            drop_cmd = sql.SQL("DROP TABLE {table_name}").format(
                table_name=sql.Identifier(table.name)
            )
            su_conn.execute(drop_cmd)

    def _drop_column(self, table: AbstractTable, column: Column):
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            drop_cmd = sql.SQL(
                """
                ALTER TABLE {table_name} DROP COLUMN {column_name}
                """
                ).format(
                    table_name=sql.Identifier(table.name),
                    column_name=sql.Identifier(column.name)
            )
            su_conn.execute(drop_cmd)

    def test_list_tables(self):
        table1 = DummyTable(self.raiddex_db.raiddex_pool)
        table2 = DummyTable(self.raiddex_db.raiddex_pool)
        table2.name = "dummy_table2"
        table3 = DummyTable(self.raiddex_db.raiddex_pool)
        table3.name = "dummy_table3"
        test_tables = [table1, table2, table3]

        for table in test_tables:
            self.addCleanup(self._drop_table, table)
            create_cmd = table.create_table_sql()
            with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
                su_conn.execute(create_cmd)
                su_conn.execute(
                    sql.SQL("GRANT SELECT ON {table_name} TO {user}").format(
                        table_name=sql.Identifier(table.name), user=sql.Identifier(self.raiddex_user)
                    )
                )
        
        actual_tables = self.raiddex_db.list_tables()
        self.assertEqual(3, len(actual_tables))
        for table in test_tables:
            self.assertIn(table.name, actual_tables)

    def test_initialize_table(self):
        dummy_table = DummyTable(self.raiddex_db.raiddex_pool)

        before_tables = self.raiddex_db.list_tables()
        self.assertNotIn(dummy_table.name, before_tables)

        self.raiddex_db._initialize_table(dummy_table)
        self.addCleanup(self._drop_table, dummy_table)

        after_tables = self.raiddex_db.list_tables()
        self.assertIn(dummy_table.name, after_tables)

    def test_initialize_tables(self):
        """
        Tests that tables in the RaidDex database can be initialized with mocked data.

        See sanity tests for an identical test that uses real data instead.
        """
        expected_tables = RaidDexDB.managed_tables
        mocked_parser = MockedParser()
        for table in expected_tables:
            self.addCleanup(self._drop_table, table)

        self.raiddex_db.initialize_tables(mocked_parser)

        actual_tables = self.raiddex_db.list_tables()
        for table in expected_tables:
            expected_table_name = table.name
            self.assertIn(expected_table_name, actual_tables)

    def test_get_missing_columns(self):
        table = DummyTable(self.mocked_db_pool)
        self.addCleanup(self._drop_table, table)
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            create_cmd = table.create_table_sql()
            su_conn.execute(create_cmd)
            su_conn.execute(
                sql.SQL("GRANT SELECT ON {table_name} TO {user}").format(
                    table_name=sql.Identifier(table.name), user=sql.Identifier(self.raiddex_user)
                )
            )

        new_column1 = Column("new_col", "integer", "")
        table.columns = table.columns + [new_column1]

        missing_columns = self.raiddex_db._get_missing_columns(table)
        self.assertIn(new_column1, missing_columns)
    
    def test_update_table_schema(self):
        table = DummyTable(self.mocked_db_pool)
        self.addCleanup(self._drop_table, table)
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            create_cmd = table.create_table_sql()
            su_conn.execute(create_cmd)
            su_conn.execute(
                sql.SQL("GRANT SELECT ON {table_name} TO {user}").format(
                    table_name=sql.Identifier(table.name), user=sql.Identifier(self.raiddex_user)
                )
            )

        new_column1 = Column("new_col", "integer", "")
        self.addCleanup(self._drop_column, table, new_column1)
        self.raiddex_db._update_table_schema(table, [new_column1])

        columns = table.list_columns()
        self.assertIn(new_column1.name, columns)

    def test_sync_table_schema(self):
        # Honestly, db tests need a rewrite to move helper modules into DBTest.
        # This really is needed to also cleanup the setup/teardown modules.
        table = DummyTable(self.mocked_db_pool)
        self.addCleanup(self._drop_table, table)
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            create_cmd = table.create_table_sql()
            su_conn.execute(create_cmd)
            su_conn.execute(
                sql.SQL("GRANT SELECT ON {table_name} TO {user}").format(
                    table_name=sql.Identifier(table.name), user=sql.Identifier(self.raiddex_user)
                )
            )

        # Hack to not overwrite the class attribute
        new_column1 = Column("new_col", "integer", "")
        new_column2 = Column("new_col2", "text", "")
        table.columns = table.columns + [new_column1, new_column2]
        self.addCleanup(self._drop_column, table, new_column1)
        self.addCleanup(self._drop_column, table, new_column2)

        self.raiddex_db.sync_table_schema(table)

        current_columns = table.list_columns()
        self.assertIn(new_column1.name, current_columns)
        self.assertIn(new_column2.name, current_columns)
        self.assertEqual(len(DummyTable.columns), 1)

    def test_sync_table_schema_no_update_needed(self):
        # Honestly, db tests need a rewrite to move helper modules into DBTest.
        # This really is needed to also cleanup the setup/teardown modules.
        table = DummyTable(self.mocked_db_pool)
        self.addCleanup(self._drop_table, table)
        with DBPool.get_su_conn(self.raiddex_db_name) as su_conn:
            create_cmd = table.create_table_sql()
            su_conn.execute(create_cmd)
            su_conn.execute(
                sql.SQL("GRANT SELECT ON {table_name} TO {user}").format(
                    table_name=sql.Identifier(table.name), user=sql.Identifier(self.raiddex_user)
                )
            )

        with patch.object(self.raiddex_db, "_update_table_schema") as mocked_update_table:
            self.raiddex_db.sync_table_schema(table)
            mocked_update_table.assert_not_called()
