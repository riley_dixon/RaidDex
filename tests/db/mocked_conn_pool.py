from psycopg import Connection

class MockedConnectionPool:
    """
    A Mocked ConnectionPool that returns a single connection.

    This is to imitate the ConnectionPool interface, while secretly using
    a single Connection object to allow for running each test within a single
    Transaction rather than creating and destroying the DB and User
    (which are expensive operations) to ensure DB consistency between tests.
    """

    def __init__(
        self,
        db_user: str,
        db_password: str,
        db_address: str,
        db_port: int,
        db_name: str
    ) -> None:
        user = db_user
        if db_password is not None:
            user = f"{user}:{db_password}"
        address = db_address
        if db_port is not None:
            address = f"{address}:{db_port}"
        
        self.conn = None
        self.user = user
        self.address = address
        self.db_name = db_name

    def open(self) -> None:
        self.conn = Connection.connect(
            f"postgresql://{self.user}@{self.address}/{self.db_name}"
        )
        def do_nothing():
            """
            Connection context managers will want to close/commit/rollback the connection
            after use. Don't let it for the sake of maintaining our transaction block for testing.
            """
            pass
        self.conn.close_conn = self.conn.close
        self.conn.close = do_nothing
        self.conn.commit_conn = self.conn.commit
        self.conn.commit = do_nothing
        self.conn.rollback_conn = self.conn.rollback
        self.conn.rollback = do_nothing

    def connection(self) -> Connection:
        return self.conn

    def close(self) -> None:
        self.conn.close_conn()
