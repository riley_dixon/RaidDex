# Root for Unit Tests
import logging

from unittest.mock import patch

from psycopg import sql, Connection, OperationalError
from psycopg.pq import ConnStatus
from psycopg_pool import ConnectionPool, PoolTimeout

from tests.db import DBTest, MockedParser

from raiddex.db.connection_config import get_conn_config
from raiddex.db.db_manager import DBManager
from raiddex.db.db_pool import DBPool


class TestDB(DBTest):
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=False)
        DBPool.create_pools()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.raiddex_db = conn_config.get_raiddex_db()
        cls.raiddex_user = conn_config.get_raiddex_user_name()
        cls.raiddex_pw = conn_config.get_raiddex_user_password()
        cls.telemetry_db = conn_config.get_telemetry_db()
        cls.telemetry_user = conn_config.get_telemetry_user_name()
        cls.telemetry_pw = conn_config.get_telemetry_user_password()
    
    @classmethod
    def tearDownClass(cls) -> None:
        DBPool.close_pools()
        super().tearDownClass()

    def setUp(self) -> None:
        super().setUp()
        self.db_manager = DBManager()
        self.test_user_name = "test-user"
        self.test_db_name = "test-db"
        self.mocked_parser = MockedParser()
        
        DBPool.superuser_pool.open()
    
    def tearDown(self) -> None:
        super().tearDown()
        DBPool.raiddex_pool.close()
        DBPool.telemetry_pool.close()

    def _drop_database(self, db_name: str) -> None:
        with DBPool.superuser_pool.connection() as su_conn:
            su_conn.execute(
                sql.SQL("DROP DATABASE IF EXISTS {db_name}").format(
                    db_name=sql.Identifier(db_name)
                )
            )

    def _drop_role(self, role_name: str) -> None:
        with DBPool.superuser_pool.connection() as su_conn:
            su_conn.execute(
                sql.SQL("DROP ROLE IF EXISTS {user_name}").format(
                    user_name=sql.Identifier(role_name)
                )
            )

    def test_db_connection(self):
        """
        Connect to default "postgres" DB.
        """
        with DBPool.superuser_pool.connection(timeout=0.2) as su_conn:
            conn_info = su_conn.info
            self.assertEqual(conn_info.status, ConnStatus.OK)

    def _test_bad_pool(self, bad_pool: ConnectionPool):
        # Tests using this function will be noisy for expected failures.
        # Disable the internal pool logger for these tests.
        logger = logging.getLogger("psycopg.pool")
        logger.disabled = True
        self.addCleanup(setattr, logger, "disabled", False)
        
        bad_pool.open()
        self.addCleanup(bad_pool.close, 1)
        
        with self.assertRaises(PoolTimeout):
            with bad_pool.connection(timeout=0.2):
                pass

    def test_bad_username_connection(self):
        bad_user_pool = DBPool.create_pool(
            "fake_user",
            self.su_pw,
            self.db_address,
            self.db_port,
            self.default_db,
            "test-fake-user"
        )
        self._test_bad_pool(bad_user_pool)
    
    def test_bad_password_connection(self):
        bad_user_pool = DBPool.create_pool(
            self.su_name,
            "fake_pw",
            self.db_address,
            self.db_port,
            self.default_db,
            "test-fake-pw"
        )
        self._test_bad_pool(bad_user_pool)

    def test_bad_address_connection(self):
        bad_hostspec_pool = DBPool.create_pool(
            self.su_name,
            self.su_pw,
            "172.0.0.1",
            self.db_port,
            self.default_db,
            "test-fake-address",
        )
        self._test_bad_pool(bad_hostspec_pool)
    
    def test_bad_port_connection(self):
        bad_hostspec_pool = DBPool.create_pool(
            self.su_name,
            self.su_pw,
            self.db_address,
            80,
            self.default_db,
            "test-fake-port",
        )
        self._test_bad_pool(bad_hostspec_pool)

    def test_bad_dbname_connection(self):
        bad_dbname_pool = DBPool.create_pool(
            self.su_name,
            self.su_pw,
            self.db_address,
            self.db_port,
            "fake_db",
            "test-fake-db"
        )
        self._test_bad_pool(bad_dbname_pool)

    def test_create_and_drop_user(self):
        self.db_manager._create_user(self.test_user_name)
        self.assertDBRoleExists(self.test_user_name)

        self.db_manager._drop_user(self.test_user_name)
        self.assertDBRoleNotExists(self.test_user_name)

    def test_create_and_drop_db(self):
        self.db_manager._create_database(self.test_db_name)
        self.assertDBExists(self.test_db_name)

        self.db_manager._drop_database(self.test_db_name)
        self.assertDBRoleNotExists(self.test_db_name)

    def test_initialize_db(self):
        """
        Tests only at the Database level.
        
        Lower granularity tests within the table should be done elsewhere.
        """
        self.addCleanup(self._drop_role, self.raiddex_user)
        self.addCleanup(self._drop_role, self.telemetry_user)
        self.addCleanup(self._drop_database, self.raiddex_db)
        self.addCleanup(self._drop_database, self.telemetry_db)
        self.db_manager.initialize_db(self.mocked_parser)

        self.assertDBRoleExists(self.raiddex_user)
        self.assertDBRoleExists(self.telemetry_user)
        self.assertDBExists(self.raiddex_db)
        self.assertDBExists(self.telemetry_db)

        # Test that our specific user can access the DB.
        raiddex_user = self.raiddex_user
        if self.raiddex_pw is not None or self.raiddex_pw != "":
            raiddex_user = f"{raiddex_user}:{self.raiddex_pw}"
        with Connection.connect(
            f"postgresql://{raiddex_user}@{self.db_address}:{self.db_port}/{self.raiddex_db}"
        ) as conn:
            conn_info = conn.info
            self.assertEqual(conn_info.status, ConnStatus.OK)
        
        telemetry_user = self.telemetry_user
        if self.telemetry_pw is not None or self.telemetry_pw != "":
            telemetry_user = f"{telemetry_user}:{self.telemetry_pw}"
        with Connection.connect(
            f"postgresql://{telemetry_user}@{self.db_address}:{self.db_port}/{self.telemetry_db}"
        ) as conn:
            conn_info = conn.info
            self.assertEqual(conn_info.status, ConnStatus.OK)

        # Test that an unrelated, unprivileged account cannot connect.
        self.db_manager._create_user(self.test_user_name)
        self.addCleanup(self._drop_role, self.test_user_name)
        # test user cleaned up in tearDown
        with self.assertRaises(OperationalError):
            with Connection.connect(
                f"postgresql://{self.test_user_name}@{self.db_address}:{self.db_port}/{self.raiddex_db}"
            ) as conn:
                pass
        with self.assertRaises(OperationalError):
            with Connection.connect(
                f"postgresql://{self.test_user_name}@{self.db_address}:{self.db_port}/{self.telemetry_db}"
            ) as conn:
                pass

    def test_list_databases(self):
        db_list = self.db_manager._list_databases()

        self.assertIn("postgres", db_list)
        self.assertIn("template0", db_list)
        self.assertIn("template1", db_list)
        self.assertEqual(len(db_list), 3)

    def test_list_users(self):
        user_list = self.db_manager._list_users()

        self.assertIn("rd_admin", user_list)
        self.assertEqual(len(user_list), 1)

    def test_create_user_already_exists(self):
        self.addCleanup(self._drop_role, self.raiddex_user)
        self.addCleanup(self._drop_role, self.telemetry_user)
        
        self.db_manager._create_user(self.raiddex_user)
        self.db_manager._create_user(self.telemetry_user)
        with patch.object(self.db_manager, "_create_user") as mocked_create_user:
            self.db_manager.create_users()
            mocked_create_user.assert_not_called()

    def test_create_database_already_exists(self):
        self.addCleanup(self._drop_database, self.raiddex_db)
        self.addCleanup(self._drop_database, self.telemetry_db)
        
        self.db_manager._create_database(self.raiddex_db)
        self.db_manager._create_database(self.telemetry_db)
        with patch.object(self.db_manager, "_create_database") as mocked_create_db:
            self.db_manager.create_databases()
            mocked_create_db.assert_not_called()
