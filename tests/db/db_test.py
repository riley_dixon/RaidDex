from unittest.mock import MagicMock
from psycopg import sql
from raiddex.db.db_pool import DBPool

from tests import BaseTest


class MockedParser(MagicMock):
    """
    Mocked `Parser` object for these tests.

    Result will be Tables will not get populated as this could be a fairly
    expensive operation.
    
    Save inserts for system tests.
    """
    def __init__(self):
        super().__init__()
        self.pokemons = {}
        self.custom_default_forms = {}


class DBTest(BaseTest):

    @classmethod
    def setUpClass(cls, load_data: bool = True):
        super().setUpClass(load_data)

    def assertDBRoleExists(self, user_name):
        """
        Assert that the ROLE user_name exists in Postgres.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"usename\" FROM pg_user")
            user_names = [user[0] for user in result.fetchall()]
            self.assertIn(user_name, user_names)
    
    def assertDBRoleNotExists(self, user_name):
        """
        Assert that the ROLE user_name does not exist in Postgres.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"usename\" FROM pg_user")
            user_names = [user[0] for user in result.fetchall()]
            self.assertNotIn(user_name, user_names)

    def assertDBExists(self, db_name):
        """
        Assert that the DATABASE db_name exists in Postgres.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"datname\" FROM pg_database")
            db_names = [db[0] for db in result.fetchall()]
            self.assertIn(db_name, db_names)
    
    def assertDBNotExists(self, db_name):
        """
        Assert that the DATABASE db_name does not exist in Postgres.
        """
        with DBPool.superuser_pool.connection() as su_conn:
            result = su_conn.execute("SELECT \"datname\" FROM pg_database")
            db_names = [db[0] for db in result.fetchall()]
            self.assertNotIn(db_name, db_names)

    def assertColumnsEqual(self, expected_columns: list[str], actual_columns: list[str]) -> None:
        remaining_columns = actual_columns.copy()
        missing_columns = []

        for column in actual_columns:
            if column in expected_columns:
                remaining_columns.remove(column)
            else:
                missing_columns.append(column)
        
        self.assertListEqual([], missing_columns, f"Missing columns from table: {missing_columns}")
        self.assertListEqual([], remaining_columns, f"Unexpected columns present in table: {remaining_columns}")
    
    def get_table_columns(self, db_name, table_name) -> list[str]:
        query = sql.SQL("""
            SELECT {column_name}
            FROM information_schema.columns
            WHERE table_name = {table_name}
            """).format(
                column_name=sql.Identifier("column_name"), table_name=sql.Literal(table_name)
        )

        with DBPool.get_su_conn(db_name) as su_conn:
            result = su_conn.execute(query)
            columns = [column[0] for column in result.fetchall()]
        
        return columns