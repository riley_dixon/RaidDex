import asyncio

from typing import NamedTuple
from unittest.mock import MagicMock
from psycopg import sql
from psycopg.rows import namedtuple_row

from raiddex.db.abstract_table import AbstractTable
from raiddex.db.base_column import Column
from raiddex.db.db_pool import DBPool
from raiddex.db.db_manager import DBManager
from raiddex.db.connection_config import get_conn_config
from raiddex.db.telemetry.telemetry_db import TelemetryDB

from tests.db import DBTest, MockedParser
from tests.db.mocked_conn_pool import MockedConnectionPool


class DummyTable(AbstractTable):
    name = "dummy_table"
    columns = [
        Column("interaction_id", "bigint", "PRIMARY KEY"),
        Column("guild_id", "bigint"),
        Column("channel_id", "bigint"),
        Column("user_id", "bigint"),
        Column("status", "varchar(10)"),
        Column("col1", "varchar(20)")
    ]
    table_attributes = ""


class TestTelemetryDB(DBTest):
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=True)
        DBPool.create_pools()
        DBPool.superuser_pool.open()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()
        cls.telemetry_db_name = conn_config.get_telemetry_db()
        cls.telemetry_user = conn_config.get_telemetry_user_name()
        cls.telemetry_pw = conn_config.get_telemetry_user_password()

        cls.db_manager = DBManager()
        cls.mocked_db_pool = MockedConnectionPool(
            cls.telemetry_user, cls.telemetry_pw, cls.db_address, cls.db_port, cls.telemetry_db_name
        )
        cls.db_manager._create_user(cls.telemetry_user, cls.telemetry_pw)
        cls.db_manager._create_database(cls.telemetry_db_name)
        cls.db_manager._grant_db_access(cls.telemetry_db_name, cls.telemetry_user)

        cls.addClassCleanup(DBPool.close_pools)
        cls.addClassCleanup(cls.db_manager._drop_user, cls.telemetry_user)
        cls.addClassCleanup(cls.db_manager._drop_database, cls.telemetry_db_name)

        cls.mocked_db_pool.open()
        cls.telemetry_db = TelemetryDB(cls.mocked_db_pool)
    
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mocked_db_pool.close()
        super().tearDownClass()
    
    def setUp(self) -> None:
        self.telemetry_db = TelemetryDB(db_pool=self.mocked_db_pool)
        self.mocked_db_pool.conn.transaction()
    
    def tearDown(self) -> None:
        self.mocked_db_pool.conn.rollback_conn()

    def _drop_table(self, table: AbstractTable) -> None:
        with DBPool.get_su_conn(self.telemetry_db_name) as su_conn:
            drop_cmd = sql.SQL("DROP TABLE {table_name}").format(
                table_name=sql.Identifier(table.name)
            )
            su_conn.execute(drop_cmd)
    
    def _create_table(self, table: AbstractTable, user: str) -> None:
        """
        Create a table and grant access to user.

        Table will be dropped at the end of the test.
        """
        create_table = table.create_table_sql()
        with DBPool.get_su_conn(self.telemetry_db_name) as su_conn:
            self.addCleanup(self._drop_table, table)
            su_conn.execute(create_table)
            su_conn.execute(
                sql.SQL("GRANT SELECT, INSERT ON {table_name} TO {user}").format(
                    table_name=sql.Identifier(table.name), user=sql.Identifier(user)
                )
            )
    
    def _get_dummy_interaction(self, table: AbstractTable) -> MagicMock:
        mocked_interaction = MagicMock()
        mocked_interaction.data = {
            "options": [
                {
                    "name": "col1",
                    "value": "test_col1"
                }
            ]
        }
        mocked_interaction.command.name = table.name
        mocked_interaction.id = 1
        mocked_interaction.guild_id = 2
        mocked_interaction.channel_id = 3
        mocked_interaction.user.id = 4

        return mocked_interaction

    def _get_telemetry_row(self, table: AbstractTable) -> NamedTuple:
        with self.mocked_db_pool.connection() as conn:
            query = sql.SQL(
                "SELECT * FROM {table_name}"
            ).format(table_name=sql.Identifier(table.name))
            cursor = conn.cursor(row_factory=namedtuple_row)
            results = cursor.execute(query)
            row = results.fetchone()
        return row

    def test_insert_app_invocation(self):
        dummy_table = DummyTable(self.mocked_db_pool)
        self._create_table(dummy_table, self.telemetry_user)
        mocked_interaction = self._get_dummy_interaction(dummy_table)
        test_status = "COMPLETE"

        self.telemetry_db.insert_app_command_invocation(mocked_interaction, test_status)
        row = self._get_telemetry_row(dummy_table)
        
        self.assertEqual(mocked_interaction.id, row.interaction_id)
        self.assertEqual(mocked_interaction.guild_id, row.guild_id)
        self.assertEqual(mocked_interaction.channel_id, row.channel_id)
        self.assertEqual(mocked_interaction.user.id, row.user_id)
        self.assertEqual(test_status, row.status)
        for option in mocked_interaction.data["options"]:
            name = option["name"]
            value = option["value"]
            self.assertEqual(value, getattr(row, name))

    def test_app_command_complete(self):
        dummy_table = DummyTable(self.mocked_db_pool)
        self._create_table(dummy_table, self.telemetry_user)
        mocked_interaction = self._get_dummy_interaction(dummy_table)

        asyncio.run(self.telemetry_db.app_command_complete(mocked_interaction))
        row = self._get_telemetry_row(dummy_table)
        
        self.assertEqual(mocked_interaction.id, row.interaction_id)
        self.assertEqual(mocked_interaction.guild_id, row.guild_id)
        self.assertEqual(mocked_interaction.channel_id, row.channel_id)
        self.assertEqual(mocked_interaction.user.id, row.user_id)
        self.assertEqual("COMPLETED", row.status)
        for option in mocked_interaction.data["options"]:
            name = option["name"]
            value = option["value"]
            self.assertEqual(value, getattr(row, name))

    def test_app_command_error(self):
        dummy_table = DummyTable(self.mocked_db_pool)
        self._create_table(dummy_table, self.telemetry_user)
        mocked_interaction = self._get_dummy_interaction(dummy_table)

        asyncio.run(self.telemetry_db.app_command_error(mocked_interaction))
        row = self._get_telemetry_row(dummy_table)
        
        self.assertEqual(mocked_interaction.id, row.interaction_id)
        self.assertEqual(mocked_interaction.guild_id, row.guild_id)
        self.assertEqual(mocked_interaction.channel_id, row.channel_id)
        self.assertEqual(mocked_interaction.user.id, row.user_id)
        self.assertEqual("FAILED", row.status)
        for option in mocked_interaction.data["options"]:
            name = option["name"]
            value = option["value"]
            self.assertEqual(value, getattr(row, name))

    def test_app_command_user_error(self):
        dummy_table = DummyTable(self.mocked_db_pool)
        self._create_table(dummy_table, self.telemetry_user)
        mocked_interaction = self._get_dummy_interaction(dummy_table)

        asyncio.run(self.telemetry_db.app_command_user_error(mocked_interaction))
        row = self._get_telemetry_row(dummy_table)
        
        self.assertEqual(mocked_interaction.id, row.interaction_id)
        self.assertEqual(mocked_interaction.guild_id, row.guild_id)
        self.assertEqual(mocked_interaction.channel_id, row.channel_id)
        self.assertEqual(mocked_interaction.user.id, row.user_id)
        self.assertEqual("USER_ERROR", row.status)
        for option in mocked_interaction.data["options"]:
            name = option["name"]
            value = option["value"]
            self.assertEqual(value, getattr(row, name))

    def test_initialize_tables(self):
        """
        Tests that tables in the Telemetry database can be initialized.
        """
        expected_tables = TelemetryDB.managed_tables.values()
        mocked_parser = MockedParser()
        for table in expected_tables:
            self.addCleanup(self._drop_table, table)

        self.telemetry_db.initialize_tables()

        actual_tables = self.telemetry_db.list_tables()
        for table in expected_tables:
            expected_table_name = table.name
            self.assertIn(expected_table_name, actual_tables)
