from psycopg import sql

from tests.db import DBTest
from tests.db.mocked_conn_pool import MockedConnectionPool

from raiddex.db.db_pool import DBPool
from raiddex.db.connection_config import get_conn_config
from raiddex.db.abstract_table import AbstractTable



class DummyTable(AbstractTable):
    """
    A mocked out AbstractTable that implements the required abstract methods.
    """
    name = None
    columns = None
    table_attributes = None


class TestAbstractTable(DBTest):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(load_data=True)
        DBPool.create_pools()
        DBPool.superuser_pool.open()
        conn_config = get_conn_config()
        cls.db_address = conn_config.get_db_address()
        cls.db_port = conn_config.get_db_port()
        cls.default_db = conn_config.get_default_db()
        cls.su_name = conn_config.get_su_name()
        cls.su_pw = conn_config.get_su_password()

        cls.mocked_db_pool = MockedConnectionPool(
            cls.su_name, cls.su_pw, cls.db_address, cls.db_port, cls.default_db
        )
        cls.addClassCleanup(DBPool.close_pools)
        cls.mocked_db_pool.open()
    
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mocked_db_pool.close()
        super().tearDownClass()
    
    def setUp(self) -> None:
        self.dummy_table = DummyTable(self.mocked_db_pool)
        self.mocked_db_pool.conn.transaction()
    
    def tearDown(self) -> None:
        self.mocked_db_pool.conn.rollback_conn()

    def _drop_table(self, table_name):
        with DBPool.get_su_conn(self.default_db) as su_conn:
            drop_cmd = sql.SQL("DROP TABLE {table_name}").format(
                table_name=sql.Identifier(table_name)
            )
            su_conn.execute(drop_cmd)

    def test_list_columns(self):
        self.dummy_table.name = "col_test"
        self.addCleanup(self._drop_table, self.dummy_table.name)
        cmd = sql.SQL("""
            CREATE TABLE {table_name} (
                col1 varchar(2),
                col2 varchar(2),
                col3 varchar(2)
            )
        """).format(
            table_name=sql.Identifier(self.dummy_table.name)
        )
        with DBPool.get_su_conn(self.default_db) as su_conn:
            su_conn.execute(cmd)

        columns = self.dummy_table.list_columns()

        self.assertEqual(3, len(columns))
        self.assertIn("col1", columns)
        self.assertIn("col2", columns)
        self.assertIn("col3", columns)
