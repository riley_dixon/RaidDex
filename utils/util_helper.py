# Random scripts for easier analysis. May or may not work or is tested.
import json
import pathlib
import pprint
import re
import sys
from types import NoneType

BASE_PATH = pathlib.Path(__file__).parent.joinpath("..").resolve()
PM_GAME_DATA_PATH = BASE_PATH / "./assets/game_data/latest/latest.json"
sys.path.append(str(BASE_PATH))

from raiddex.core.models.forms.forms import TEMPLATE_ID_FILTER as FORM_FILTER
from raiddex.core.models.moves.pve_move import TEMPLATE_ID_FILTER as PVE_MV_ID_FILTER
from raiddex.core.models.moves.pvp_move import TEMPLATE_ID_FILTER as PVP_MV_ID_FILTER
from raiddex.core.models.pokemon.pokemon import TEMPLATE_ID_FILTER as PK_ID_FILTER

with open(PM_GAME_DATA_PATH, "r") as pm_data_file:
    pm_data = json.load(pm_data_file)

def get_all_forms():
    forms = []
    for node in pm_data:
        if re.match(FORM_FILTER, node["templateId"]) is None:
            continue
        forms.append(node)
    return forms

def get_all_pve_moves():
    moves = []
    for node in pm_data:
        if re.match(PVE_MV_ID_FILTER, node["templateId"]) is None:
            continue
        moves.append(node)
    return moves

def get_all_pvp_moves():
    moves = []
    for node in pm_data:
        if re.match(PVP_MV_ID_FILTER, node["templateId"]) is None:
            continue
        moves.append(node)
    return moves

def get_all_pokemon():
    pokemons = []
    for node in pm_data:
        if (
            re.match(PK_ID_FILTER, node["templateId"]) is None
            or (re.search(r"HOME_(FORM_)?REVERSION", node["templateId"])
                is not None)
        ):
            continue
        pokemons.append(node)
    return pokemons

def get_object_schema(all_items):
    """
    Return the schema of a collection of objects.
    This considers all objects to be able to make a list of all possible
    keys that may be encountered.

    Its not pretty.... but it works.
    Probably could have found a better solution in a library somewhere lol
    """
    def get_subobject_schema(schema_current_view, item_sub_dict):
        for k, v in item_sub_dict.items():
            if isinstance(v, dict):
                if k not in schema_current_view:
                    schema_current_view[k] = {}
                elif not isinstance(schema_current_view[k], dict):
                    raise TypeError("weeeee")
                get_subobject_schema(schema_current_view[k], v)
            elif isinstance(v, list):
                if k not in schema_current_view:
                    schema_current_view[k] = []
                if len(v) == 0:
                    if schema_current_view[k] == []:
                        schema_current_view[k] = [type(None)]
                    elif schema_current_view[k][0] != type(None):
                        raise TypeError("I hate life")
                else:
                    for x in v:
                        if isinstance(x, dict):
                            if schema_current_view[k] == []:
                                schema_current_view[k] = [{}]
                            if isinstance(schema_current_view[k][0], dict):
                                get_subobject_schema(schema_current_view[k][0], x)
                            else:
                                raise TypeError("I really hate life.")
                        else:
                            if schema_current_view[k] == []:
                                schema_current_view[k] = [type(x)]
                            elif schema_current_view[k][0] != type(x):
                                raise TypeError("Something something life.")
            else:
                if k in schema_current_view and schema_current_view[k] != type(v):
                    raise TypeError("Ahhhh %s != %s" % (type(schema_current_view[k]), type(v)))
                schema_current_view[k] = type(v)

    schema = {}
    for item in all_items:
        get_subobject_schema(schema, item)
    
    return schema
