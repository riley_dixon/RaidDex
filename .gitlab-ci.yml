stages:
  - build_base
  - build
  - quick_tests
  - long_tests
  - deploy_internal

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

variables:
  DOCKER_IMAGE:
    description: "Version of Docker when building new images within Docker."
    value: docker:24.0
  DIND_IMAGE:
    description: "DIND Service Image"
    value: ${DOCKER_IMAGE}-dind
  RAIDDEX_CI_IMAGE:
    description: "Path to our Docker image we use for CI coverage."
    value: ${CI_REGISTRY_IMAGE}/raiddex_ci_env
  RAIDDEX_OP_ENV:
    description: Configure RaidDex behavior to expect to be running in a CI environment.
    value: GITLAB_CI
  LATEST_RAIDDEX_CI_IMAGE:
    value: ${RAIDDEX_CI_IMAGE}:latest
  CURRENT_RAIDDEX_CI_IMAGE:
    value: ${RAIDDEX_CI_IMAGE}:${CI_COMMIT_REF_SLUG}
  GITLAB_PACKAGE_REGISTRY_URL:
    value: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*.whl
  POSTGRES_DB:
    description: Name to be given to the default database in PostgreSQL (for CI jobs).
    value: ${SECRET_POSTGRES_DB}
  POSTGRES_USER:
    description: Name of the initial superuser account for PostgreSQL (for CI jobs).
    value: ${SECRET_POSTGRES_USER}
  POSTGRES_PASSWORD:
    description: Password for the initial superuser account for PostgreSQL (for CI jobs).
    value: ${SECRET_POSTGRES_PASSWORD}
  POSTGRES_RAIDDEX_DB:
    description: Name for the database containing Pokemon GO data (for CI jobs).
    value: ${SECRET_RAIDDEX_DB}
  POSTGRES_RAIDDEX_USER:
    description: Name for the user account delegated for querying Pokemon GO data (for CI jobs).
    value: ${SECRET_RAIDDEX_USER}
  POSTGRES_RAIDDEX_PASSWORD:
    description: Password for the user account delegated for querying Pokemon GO data (for CI jobs).
    value: ${SECRET_RAIDDEX_PASSWORD}
  POSTGRES_TELEMETRY_DB:
    description: Name for the database containing telemetry data (for CI jobs).
    value: ${SECRET_TELEMETRY_DB}
  POSTGRES_TELEMETRY_USER:
    description: Name for the user account delegated for handling telemetry data (for CI jobs).
    value: ${SECRET_TELEMETRY_USER}
  POSTGRES_TELEMETRY_PASSWORD:
    description: Password for the user account delegated for handling telemetry data (for CI jobs).
    value: ${SECRET_TELEMETRY_PASSWORD}

build_ci_image:
  image: ${DOCKER_IMAGE}
  services:
    - ${DIND_IMAGE}
  stage: build_base
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker buildx build
      -f docker/DOCKERFILE.ci_image
      --build-arg BUILDKIT_INLINE_CACHE=1
      --cache-from=type=registry,ref=${CURRENT_RAIDDEX_CI_IMAGE}
      --cache-from=type=registry,ref=${LATEST_RAIDDEX_CI_IMAGE}
      -t ${CURRENT_RAIDDEX_CI_IMAGE} .
    - docker push ${CURRENT_RAIDDEX_CI_IMAGE}

build_venv:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: build
  script:
    - python3.10 -m venv --upgrade-deps ./venv
    - source ./venv/bin/activate
    - pip install .
  artifacts:
    name: raiddex-python-venv
    expire_in: 2 Hours
    paths:
      - venv

# TODO: Eventually get unit tests to run prior to needing to build
# the ci image & other deps.
unit_tests:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: quick_tests
  before_script:
    - source ./venv/bin/activate
  script:
    - python3 -m unittest discover tests.unit

db_tests:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: quick_tests
  services:
    - postgres:15.5
  before_script:
    - source ./venv/bin/activate
  script:
    - python3 -m unittest discover tests.db

sanity_tests:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: quick_tests
  services:
    - postgres:15.5
  before_script:
    - source ./venv/bin/activate
  script:
    - python3 -m unittest discover tests.sanity

bot_tests:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: quick_tests
  services:
    - postgres:15.5
  before_script:
    - source ./venv/bin/activate
  script:
    - python3 -m unittest discover tests.bot

db_upgrade_test:
  image: ${CURRENT_RAIDDEX_CI_IMAGE}
  stage: long_tests
  services:
    - postgres:15.5
  before_script:
    - source ./venv/bin/activate
  script:
    - pip uninstall -y raiddex
    - git checkout origin/${CI_DEFAULT_BRANCH}
    - pip install .
    - python3 -m tests.sanity.run_db_initialization
    - pip uninstall -y raiddex
    - git checkout ${CI_COMMIT_SHA}
    - pip install .
    - python3 -m tests.sanity.run_db_initialization
  variables:
    RAIDDEX_STDERR_LOG_LEVEL_OVERRIDE: INFO

push_ci_image:
  image: ${DOCKER_IMAGE}
  stage: deploy_internal
  services:
    - ${DIND_IMAGE}
  script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker buildx build
      -f docker/DOCKERFILE.ci_image
      --build-arg BUILDKIT_INLINE_CACHE=1
      --cache-from=type=registry,ref=${LATEST_RAIDDEX_CI_IMAGE}
      -t ${LATEST_RAIDDEX_CI_IMAGE} .
    - docker push ${LATEST_RAIDDEX_CI_IMAGE}
  rules:
    # Run only when we Merge a branch into "master".
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"

push_raiddex_wheel:
  image: ${LATEST_RAIDDEX_CI_IMAGE}
  stage: deploy_internal
  before_script:
    - source ./venv/bin/activate
    - pip install build twine
  script:
    - python3.10 -m build
    - python3.10 -m twine upload
      --repository-url ${GITLAB_PACKAGE_REGISTRY_URL}
  variables:
    TWINE_PASSWORD: ${CI_JOB_TOKEN}
    TWINE_USERNAME: gitlab-ci-token # Do not change this username for GitLab
  needs:
    - push_ci_image
    - job: build_venv
      artifacts: true
  rules:
    # Run only when we Merge a branch into "master".
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
      when: manual
# push to production