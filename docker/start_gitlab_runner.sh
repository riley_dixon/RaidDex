GITLAB_CONFIG_COLUME_NAME="local-gitlab-runner-config"
GITLAB_RUNNER_VERSION="ubuntu-v16.6.0"
GITLAB_RUNNER_IMAGE="gitlab/gitlab-runner:${GITLAB_RUNNER_VERSION}"
RUNNER_NAME="local-gitlab-runner"


docker run \
    -d \
    --name ${RUNNER_NAME} \
    --restart always \
    --env TZ=UTC \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ${GITLAB_CONFIG_COLUME_NAME}:/etc/gitlab-runner \
    ${GITLAB_RUNNER_IMAGE}
