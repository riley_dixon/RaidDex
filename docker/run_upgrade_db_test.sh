#!/bin/sh
# This assumes we are running in an Alpine container.
# This script also assumes we are connecting to a clean, disposable,
# postgresql container. It is up to the caller to ensure that the
# containers are properly managed.

set -e

echo "Installing packages..."
apk add \
    git \
    gcc \
    musl-dev \
    python3-dev \
    py3-pip \
    py3-virtualenv \
    libpq-dev \
    postgresql15-client \
    iptables > /dev/null
cp -r /tmp/raiddex /home/raiddex
cd /home/raiddex

# Store any working changes prior to changing to master branch
echo "Setting code base to state of "master"."
CURRENT_GIT_REF=`git rev-parse HEAD`
CURRENT_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
git stash > /dev/null
git checkout master > /dev/null

# Setup venv
echo "Setting up virtualenv..."
rm -rf /home/raiddex/venv/*
python3 -m venv --upgrade-deps ./venv
source /home/raiddex/venv/bin/activate

pip install -e /home/raiddex > /dev/null

# Run Unit Tests prior to running expensive DB Tests.
# This is running tests from master, so they **SHOULD** pass.
echo "Running Unit Tests to verify test env setup correctly..."
mkdir -p /var/log/raiddex
python3 -m unittest discover tests.unit

# I don't want to add yet another "OperatingEnvironment" for this case.
# We really need to implement a better way to configure DB network
# configuration now that we are getting more sophisticated.
# This below snippet allows us to hack-localhost to route to
# the Postgres container running on our host.
#   CAUTION: This hack has security implications. However, for a temporary
#            test container that is not reaching out to the internet (we only
#            redirect packets originating from the container to the host network),
#            we should be fine.
#            The sysctl variable `net.ipv4.conf.all.route_localnet` needs to be set
#            to non-zero.
DOCKER_HOST_IP=`grep -e host.docker.internal /etc/hosts | awk '{ print $1}'`
iptables -t nat -A OUTPUT -d localhost -p TCP --dport 5434:5435 -j DNAT --to-destination ${DOCKER_HOST_IP}
iptables -t nat -A POSTROUTING -d ${DOCKER_HOST_IP} -p TCP --dport 5434:5435 -j MASQUERADE

# Initialize the DB with our existing data.
echo "Initializing DB on branch 'master'."
python3 -m tests.sanity.run_db_initialization

# Move back to the latest ref being tested.
echo "Setting code base to state of '${CURRENT_GIT_BRANCH}'."
git checkout ${CURRENT_GIT_BRANCH} > /dev/null
git stash pop

# Try updating the DB.
echo "Upgrading DB from branch 'master' to ref '${CURRENT_GIT_REF}'."
python3 -m tests.sanity.run_db_initialization

echo "DB Upgrade Path was SUCCESSFUL."
